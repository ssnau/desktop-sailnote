angular.module("message.service", ['ui.bootstrap'])
.service('MessageBox', ['$dialog', function($dialog){
        var defaultConfig = {
            backdrop: true,
            keyboard: true,
            backdropClick: true,
            dialogClass:"modal auto-modal",
            template: "<div class=\"modal-header auto-modal\">\n" +
                "	<h5>{{ title }}</h5>\n" +
                "</div>\n" +
                "<div class=\"modal-body\">\n" +
                "	<p>{{ message }}</p>\n" +
                "</div>\n" +
                "<div class=\"modal-footer\">\n" +
                "	<button ng-repeat=\"btn in buttons\" ng-click=\"close(btn.result)\" class=btn ng-class=\"btn.cssClass\">{{ btn.label }}</button>\n" +
                "</div>\n" +
                "",
            controller: ['$scope', 'dialog', 'model', function($scope, dialog, model){
                $scope.title = model.title;
                $scope.message = model.message;
                $scope.buttons = model.buttons;
                $scope.close = function(res){
                    model.close(res);
                    dialog.close(res);
                };
            }],
            buttons: [
                {
                    label: '确定'
                }
            ]
        };
        return function(config) {
            var finalConfig = angular.extend({}, defaultConfig, config);
            finalConfig.resolve =  {
                model: function() {
                    return {
                        title: finalConfig.title,
                        message: finalConfig.message,
                        buttons: finalConfig.buttons,
                        close: finalConfig.close || angular.noop
                    };
                }
            };


            return $dialog.dialog(finalConfig);
        }

    }]);