angular.module("ssnau.service", [])
.factory("ThemeManager", function(){
    var prototype = {
        fetch: function (name) {
            if (this.loaded.indexOf(name) != -1) return;

            var themes = this.themes;
            themes.forEach(function(theme) {
                if (theme.name === name) {
                    _fetch(theme.path + "/" + name);
                }
            });
        }
    };

    var constructor =  function ThemeManager() {
        angular.extend(this, {
            base: "/",  /* base url when fetching a theme */
            loaded: [], /* array of theme name */
            loading: [], /* array of theme name */
            current: null
        });
    };

    angular.extend(constructor.prototype, prototype);

    return constructor;
});