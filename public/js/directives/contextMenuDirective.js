angular.module("ui.ssnau.contextmenu", [])
.directive("hasContextmenu", ["$parse", function($parse){
        var contextMenus = [];

        var contextables = [];


        function closeAll() {
            contextMenus.forEach(function (ndMenu) {
                ndMenu.hide();
            });
        }

        var hasBind = false;
        function bindWindowEvent() {
            if (hasBind) return;
            $(document).on("click", function () {
                closeAll();
            });
            hasBind = true;
        }

        function positionMenu(e, ndMenu){
            var pos = {left: e.pageX, top: e.pageY},
                dem = {width: 5, height: 5},
                limit = {width: $(window).width(), height: $(window).height()};

            var left, top, right, bottom, useBottom, useRight;

            left = pos.left;
            top = pos.top + dem.height;

            var mHeight = ndMenu.height(),
                mWidth  = ndMenu.width(),
                beyondBottom = mHeight + top - limit.height,
                beyondRight  = mWidth + left - limit.width;

            //1. 超下界
            if (beyondBottom > 0) {
                //如果超上界(或不超)超的比下界小，则采用上界
                if (mHeight - top < beyondBottom) {
                    bottom = limit.height - pos.top;
                    useBottom = true;
                }
            }

            //2. 超右界
            if (beyondRight > 0) {
                right = 0;
                useRight = true;
            }

            //set to auto first
            ndMenu.css("top", "auto");
            ndMenu.css("bottom", "auto");
            ndMenu.css("left", "auto");
            ndMenu.css("right", "auto");

            if (useBottom) {
                ndMenu.css("bottom", bottom + "px");
            } else {
                ndMenu.css("top", top + "px");
            }

            if (useRight) {
                ndMenu.css("right", right + "px");
            } else {
                ndMenu.css("left", left + "px");
            }

        }

        return {
            link: function (scope, elem) {
                var ndMenu = $(elem).next();

                if (!ndMenu.hasClass("context-menu")) {
                    console.error("cannot found context menu for has-context element")
                }

                bindWindowEvent();

                ndMenu.css("position", "fixed");
                contextMenus.push(ndMenu);
                contextables.push(elem);

                elem.on('contextmenu', function (e) {
                    e.stopPropagation();
                    e.preventDefault();

                    console.log(e)
                    closeAll();

                    ndMenu.show();

                    positionMenu(e, ndMenu);
                });
            }
        }
}]);