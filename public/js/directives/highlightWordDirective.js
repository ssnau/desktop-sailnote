/**
 * Created with JetBrains WebStorm.
 * User: Administrator
 * Date: 5/11/13
 * Time: 12:54 AM
 * To change this template use File | Settings | File Templates.
 */
angular.module("renderer.ssnau.highlightWord", [])

.directive("highlightWord", function(){
	return {
		restrict: "A",
		link: function(scope, element, attrs){
			var word = '', content, count = 0;
			scope.$watch(attrs["highlightWord"], function(val){
				word = val;
				//如果有listen属性，则调用render方法
				if (attrs['listen']) {
					render();
				} else {
					//仅第一次从当中取，因为只有第一次是干净的
					content = ((count++) === 0) ? element.html() : content;
					if (!val) return;
					var c = content.replace(new RegExp(val, "g"), function(match){
						return "<span class='highlighted'>" + match + "</span>"
					});
					element.html(c);
				}
			});

			scope.$watch(attrs['listen'], function(val){
				content = val;
				render();
			});

			function render(){
				if (content) {
					var c = word ? content.replace(new RegExp(word, "g"), function(match){
						return "<span class='highlighted'>" + match + "</span>"
					}) : content;
					element.html(c);
				}
			}

		}
	}
});

/**
 用法：
 <div highlight-word="keyword" listen="doc">
  {{doc}}
 </div>

 说明：
 1. 每当keyword变化，会重新根据已有内容进行高亮。
 2. 如果有listen属性，则会监听listen对应的值，用该值来render内容及高亮之。
 3. 如果没有listen属性，则直接取其innerHTML的内容进行高亮。
 */