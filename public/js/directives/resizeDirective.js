angular.module("ui.ssnau.resize", [])
.directive("fullHeight", function () {
    return {
        link: function (scope, elem) {
            var ndParent = elem.parent();
            ndParent.on("resize", function () {
                elem.css("width", ndParent.width() + "px");
            });
        }
    }
})
.directive("fullWindow", function () {
    return {
        link: function (scope, elem) {
            $(window).on("resize", function () {
                elem.css("width", $(window).width() + "px");
                elem.css("height", $(window).height() + "px");
            });
            elem.css("marginLeft", 0);
            elem.css("marginTop", 0);
            elem.css("marginRight", 0);
            elem.css("marginBottom", 0);
            elem.css("width", $(window).width() + "px");
            elem.css("height", $(window).height() + "px");
        }
    }
});