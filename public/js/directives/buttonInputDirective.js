/**
 * Created with JetBrains WebStorm.
 * User: Administrator
 * Date: 5/13/13
 * Time: 11:15 PM
 * To change this template use File | Settings | File Templates.
 */
angular.module("ui.ssnau.buttonInput", [])

.directive("buttonInput", function(){
	return {
		restrict: "E",
		replace: true,
		scope: {
			prependList: "=prependList",
			appendList: "=appendList",
			prependModel: "=prepend",
			appendModel: "=append",
			inputModel: "=input"
		},
		template: '<div class="{{className}}">' +
								'<div class="btn-group">' +
									'<button data-toggle="dropdown" class="btn dropdown-toggle" style="display:{{displayPrepend}}">{{prependModel}}&nbsp;<span class="caret"></span></button>' +
									'<ul class="dropdown-menu">' +
										'<li ng-repeat="m in prependList"><a data-index="{{$index}}" type="prepend">{{m}}</a></li>'+
									'</ul>' +
								'</div>' +
								'<input type="text" ng-model="inputModel">' +
								'<div class="btn-group" style="display:{{displayAppend}}">' +
									'<button data-toggle="dropdown" class="btn dropdown-toggle">{{appendModel}}<span class="caret"></span></button>' +
									'<ul class="dropdown-menu">' +
										'<li ng-repeat="m in appendList"><a data-index="{{$index}}" type="append">{{m}}</a></li>' +
									'</ul>' +
								'</div>' +
							'</div>',
		link: function(scope, element, attrs) {
			scope.displayAppend = attrs.append ? '' : 'none';
			scope.displayPrepend = attrs.prepend ? '' : 'none';
			var className = '';
			className += attrs.append ? "input-append" : "";
			className += attrs.prepend ? "input-prepend" : "";
			scope.className = className;
			element.on("click", "a", function(){
				var index = $(this).attr("data-index"),
						type = $(this).attr("type");
				scope.$apply(function(){
						(type == "prepend") ?
						(scope.prependModel = scope.prependList[index]):
							(scope.appendModel = scope.appendList[index]);

				});
			});
		}
	}
});

/*用例
<button-input prepend="searchType" input="keyword"></button-input>
其中searchType和keyword均为当前scope中的变量,searchType= ["content", "tag"]
*/