angular.module("ui.ssnau.keyboard", [])
/**
 * 用法：
 * ui-keydown="{keyCode: functionName}" 或
 * ui-keydown="{keyCode: expression}"
 * 如果keyCode为`*`，则表示任何键的按下都会触发事件。
 * 但对于已经设定了事件的keyCode则无效。
 * 如{
 *   "*": run,
 *   "13": fly
 * }
 * 当用户按下`Enter`时，只会触发fly，而不是run+fly.
 * 而当用户按下其它键时，都会触发run.
 *
 * <input type='text' ui-keydown="{13: submit}" />
 * $scope.submit = function(){
 *  console.log("submiting");
 * }
 * <input type='text' ui-keydown="{13: 'save(note)'}" />
 * $scope.save = function(note){
 *   note.$save();
 * }
 */
    .directive("uiKeydown", ["$parse", function ($parse) {
        return function (scope, elem, attr) {
            var config = scope.$eval(attr.uiKeydown);

            elem.on("keydown", function(e){
               var keyCode = e.keyCode,
                   action = config[keyCode] || config[keyCode + '']  || config["*"];

               if (!action) return;

               scope.$apply(function(){
                   if (angular.isFunction(action)) {
                       action();
                   } else {
                       scope.$eval(action);
                   }
               });
            });
        }
    }]);