angular.module("app", ["app.config", "ui.ssnau.resize", "ui.ssnau.waitable", "ui.ssnau.flexBox",
    "ui.ssnau.dockableFlexBox", 'ui.ssnau.upload', "LocalStorageModule", "ui.ssnau.dnd",
    "ui.ssnau.contextmenu", "ui.bootstrap", "simple.ssnau", "ui.ssnau.draggable", "message.service",
    "resource.notebook", "resource.note", "ui.ssnau.keyboard", "ui.event", "util.ssnau"]);;(function(){
   angular.module('app').controller("noteController", ['$scope', '$sce', 'NoteBook', 'Note', '$dialog', 'localStorageService', 'urlBase', 'debounce', 'MessageBox',
           function($scope, $sce, NoteBook, Note, $dialog, localStorageService, urlBase, debounce, MessageBox) {
           NoteBook.query({}, function success(data) {
               $scope.notebooks = data;
               init();
           });

           //初始化所选的笔记，如果到不到的话，自动选第一个
           function init() {
               var prevSelectedBook = localStorageService.get("selectedBook"),
                   prevSelectedNote = localStorageService.get("selectedNote"),
                   found = false,
                   book = null;

               if ($scope.notebooks) {
                   if (prevSelectedBook) {
                       for (var i = 0; book = $scope.notebooks[i]; i++ ) {
                           if (book.id == prevSelectedBook.id) {
                               found = true;
                               break;
                           }
                       }
                   }

                   book = found ? book : $scope.notebooks[0];

                   $scope.selectNotebook(book);
                   $scope.queryNotesFromNoteBookId(book.id, {
                       success: function(){
                           var note = null;
                           if (prevSelectedNote && $scope.notes && $scope.notes.length) {
                               note = $scope.notes.filter(function(note){
                                   return note.id == prevSelectedNote.id;
                               })[0];
                           }
                           $scope.selectNote(note ? note : $scope.notes[0]);
                       }
                   });
               }

               //init accordion status
               ["isShortcutOpen", "isNotebookOpen", "isTagOpen"].forEach(function(status){
                   $scope[status] = (localStorageService.get(status) + '') != "false"; //get会返回字符串，为了让以后阅读更确定这是字符串，故+''
               });
           }

           $scope.$watch("selectedBook", function(v){
               if (!v || !v.id) return;//加上判断的原因是，init()函数还没还得及调，这边就已经赋上值了
               localStorageService.add("selectedBook",  v);
           });

           $scope.$watch("selectedNote", function(v){
               if (!v || !v.id) {
                   if ($scope.markdownEditor) {
                       $scope.markdownEditor.setReadOnly(true);
                   }
                   return;
               }
               $scope.markdownEditor.setReadOnly(false);
               localStorageService.add("selectedNote", v);
           });

           ["isShortcutOpen", "isNotebookOpen", "isTagOpen"].forEach(function(status){
               $scope.$watch(status, function(v){
                   if (typeof v != "undefined") {
                       localStorageService.add(status, !!v);
                   }
               });
           });

           var sortMethods =  [
               {
                   name: "创建时间",
                   prop: "created_at"
               },
               {
                   name: '修改时间',
                   prop: "updated_at"
               },
               {
                   name: '标题(试验)',
                   prop: 'title_pinyin'
               }
           ];

           /**
            * if a > b, return 1.
            * if a < b, return -1
            * if a == b, return 0
            * @param a
            * @param b
            */
           function compare(a, b) {
               if (angular.isString(a) || angular.isString(b)) {
                   if (String.prototype.localeCompare) {
                       return (a+'').localeCompare(b);
                   }
               }

               if (a == b) return 0;
               return (a - b) > 0 ? 1 : -1;
           }

           function sortNotes() {
               var sm = $scope.sortMethod;
               var so = $scope.sortOrder;
               $scope.notes.sort(function(note1, note2){
                   var res = compare(note1[sm.prop], note2[sm.prop]);
                   return (so == 'asc') ? res : -res;
               });
           }

           //每30秒自动保存一次
           setInterval(function(){
               var cnote = $scope.currentNote;
               cnote && $scope.saveNote(cnote);
           }, 1000 * 30);

           angular.extend($scope, {
               aceThemes: [{name: "ambiance", path: "ace/theme/ambiance"},
                   {name: "chaos", path: "ace/theme/chaos"},
                   {name: "chrome", path: "ace/theme/chrome", selected: true},
                   {name: "clouds", path: "ace/theme/clouds"},
                   //{name: "clouds midnight", path: "ace/theme/clouds_midnight"},
                   {name: "cobalt", path: "ace/theme/cobalt"},
                   {name: "crimson editor", path: "ace/theme/crimson_editor"},
                   {name: "dawn", path: "ace/theme/dawn"},
                   {name: "dreamweaver", path: "ace/theme/dreamweaver"},
                   {name: "eclipse", path: "ace/theme/eclipse"},
                   {name: "github", path: "ace/theme/github"},
                   {name: "idle fingers", path: "ace/theme/idle_fingers"},
                   {name: "merbivore", path: "ace/theme/merbivore"},
                   {name: "merbivore soft", path: "ace/theme/merbivore_soft"},
                   {name: "mono industrial", path: "ace/theme/mono_industrial"},
                   {name: "monokai", path: "ace/theme/monokai"},
                   {name: "pastel on dark", path: "ace/theme/pastel_on_dark"},
                   {name: "solarized dark", path: "ace/theme/solarized_dark"},
                   {name: "solarized light", path: "ace/theme/solarized_light"},
                   {name: "terminal", path: "ace/theme/terminal"},
                   {name: "textmate", path: "ace/theme/textmate"},
                   {name: "tomorrow", path: "ace/theme/tomorrow"},
                   {name: "tomorrow night", path: "ace/theme/tomorrow_night"},
                   // {name: "tomorrow night blue", path: "ace/theme/tomorrow_night_blue"},
                   {name: "tomorrow night bright", path: "ace/theme/tomorrow_night_bright"},
                   {name: "tomorrow_night eighties", path: "ace/theme/tomorrow_night_eighties"},
                   {name: "twilight", path: "ace/theme/twilight"},
                   // {name: "vibrant ink", path: "ace/theme/vibrant_ink"},
                   {name: "xcode", path: "ace/theme/xcode"}],
               applyTheme: function(theme) {
                   try {
                       $scope.markdownEditor.setTheme(theme.path);
                       $scope.aceThemes.forEach(function(t){
                           t.selected = false;
                       });
                       theme.selected = true;
                   } catch(e){}
               },
               searchText: '',
               searchType: 'all',
               searchScope: -1,
               outline: [],
               setSearchType: function(st){
                   $scope.searchType = st;
               },
               setSearchScope: function(ss){
                   $scope.searchScope = ss;
               },
               performSearch: function () {
                   Note.search({
                       text: $scope.searchText, /*要搜索的内容*/
                       scope: $scope.searchScope, /*{-1|Integer} 值为all或notebookId*/
                       type: $scope.searchType /*{'all'|'title'}*/
                   }, function (notes) {
                       $scope.notes = notes;
                   });
               },
               sortMethods: sortMethods,
               sortMethod: sortMethods[0],
               sortOrder: 'asc',
               applySortMethod: function (sm) {
                   $scope.sortMethod = sm;
                   sortNotes();
               },
               applySortOrder: function (so) {
                   $scope.sortOrder = so;
                   sortNotes();
               },
               applyURL: function (url) {
                   $scope.insertText("![altText](" + url + ")");
                   $scope.markdownEditor.$notify();
               },
               bShowEditBox: true,
               bShowPreviewBox: true,
               /**
                * @param show [boolean|optional], 如果传入了show, 则将对应值设为show的值，否则toggle之。
                */
               toggleEditBox: function(show){
                   $scope.bShowEditBox = arguments.length === 0 ? !$scope.bShowEditBox : show;
               },
               togglePreviewBox: function(show) {
                   $scope.bShowPreviewBox = arguments.length === 0 ?  !$scope.bShowPreviewBox : show;
               },
               notebooks: mockup.notebooks,
               tags: mockup.notetags,
               shortcuts: mockup.shortcuts,
               notes: mockup.notes,
               currentNote: null,
               selectedBook: null,
               selectedTag: null,
               selectNotebook: function selectNotebook (notebook) {
                   $scope.selectedBook = notebook;
               },
               selectTag: function selectTag (tag) {
                   $scope.selectedTag = tag;
               },
               selectShortcut: function selectShortcut(shortcut) {
                   $scope.selectedShortcut = shortcut;
               },
               selectNote: function selectNote(note) {
                   if (!note) return;
                   //首先，保存正在编辑的
                   if ($scope.selectedNote) {
                       $scope.saveNote($scope.selectedNote);
                   }

                   $scope.selectedNote = note;

                   function delayfinishLoading(){
                       setTimeout(function(){
                           //必须判断，因为回调后的状态不可预知
                           if ($scope.selectedNote == note) {
                               $scope.loadingNote = false;
                               $scope.$apply();
                           }
                       }, 200);
                   }
                   if (note.id) {
                       $scope.loadingNote = true;
                       note.$get(delayfinishLoading, delayfinishLoading);
                   }
               },
               createNote: function createNote(){
                   var note = new Note(),
                       nbId = $scope.selectedBook.id;
                   note.title = "新建笔记";
                   note.nbId = $scope.selectedBook.id;
                   $scope.loadingNoteList = true;
                   note.$save(function(){
                       if ($scope.selectedBook && $scope.selectedBook.id === nbId) {
                           $scope.notes.unshift(note);
                           $scope.loadingNoteList = false;

                           // shit! visiting dom node again!
                           setTimeout(function(){
                               var ndNote =  $(".note-index-" + note.id);
                               if (!ndNote.length) return;
                               ndNote.focus();
                               var range = document.createRange();
                               range.selectNodeContents(ndNote[0]);
                               var sel = window.getSelection();
                               sel.removeAllRanges();
                               sel.addRange(range);

                           });
                       }
                   }, function(){
                       $scope.loadingNoteList = false;
                   });
               },
               saveNote: function saveNote(note) {
                   if (!note.dirty) return;
                   $scope.savingNote = true;
                   //1. 因为服务器返回的Note是不带正文的，因此用一个tempNote做缓冲
                   //2. 不应该重设body值，这样会导致编辑器光标回到起点
                   var tempNote = new Note(note);
                   tempNote.$save(function(){
                       angular.extend(note, {
                           dirty: false,
                           lastModifiedDate: tempNote.lastModifiedDate, //更新lastModifiedDate
                           id: tempNote.id //对于新建的note,需要为其设上id
                       });
                       $scope.savingNote = false;
                   }, function(){
                       $scope.savingNote = false;
                   });

               },
               deleteNote: function deleteNote(note) {
                   note.$delete(function success(){
                       var index = $scope.notes.indexOf(note);
                       if (index !== -1) { //等于-1的情况可能是，当前用户已经切换了notebook
                           if ($scope.selectedNote == note) {
                               $scope.selectedNote = null;
                           }
                           $scope.notes.splice(index, 1);
                       }
                   }, function failure(){

                   });
               },
               dropNotebook: function (note, nbScope) {
                   var nbId = nbScope.notebook.id;
                   if (!nbId) return;
                   note.$set({
                       nb_id: nbScope.notebook.id
                   }, function(note){
                       if (note.nb_id !== $scope.selectedBook.id) {
                           note.detach($scope.notes);
                       }
                   });
               },
               notebookAllowDrop: function (item, nbScope) {
                   if (item.id && item.nb_id && item.nb_id !== nbScope.notebook.id) {
                       return true;
                   }
               },
               dragNote: function(note){
                   return note;
               },
               renameNotebook: function (notebook) {
                   var outScope = $scope;
                   $dialog.dialog({
                       backdrop: true,
                       keyboard: true,
                       backdropClick: true,
                       dialogClass:"modal auto-modal",
                       templateUrl: urlBase + "public/template/new-notebook.html", // OR: templateUrl: 'path/to/view.html',
                       controller: ['$scope', 'dialog', function ($scope, dialog) { //注：此处注入的dialog和$dialog是完全不同的
                           angular.extend($scope, {
                               title: '重命名笔记本',
                               name: notebook.name,
                               placeholder: '请输入新的笔记本名字',
                               save: function (name) {
                                   notebook.name = name;
                                   //todo: handle error
                                   notebook.$save(function(){
                                       dialog.close();
                                   });
                               },
                               close: function() {
                                   dialog.close();
                               }
                           });
                       }]
                   }).open();
               },
               deleteNotebook: function (notebook) {
                   MessageBox({
                       title: "删除笔记本",
                       message: "删除笔记本会导致其下的笔记全部丢失，确认删除?",
                       buttons:[{label: "取消", result: false}, {label: "确认", result: true, cssClass: "btn-primary"}],
                       close: function(shouldDelete) {
                           console.log('on close...')
                           if (!shouldDelete) return;
                           notebook.$delete(function(){
                               var index = $scope.notebooks.indexOf(notebook);
                               if (index !== -1) {
                                   if ($scope.selectedBook == notebook) {
                                       $scope.selectedBook = null;
                                   }
                                   $scope.notebooks.splice(index, 1);
                               }
                           });
                       }
                   }).open();
               },
               togglePreviewHandlerStyle: function togglePreviewHandlerStyle() {
                   var style = $scope.previewHandlerStyle,
                       show = !(style && style._flag);

                   $scope.previewHandlerStyle = show ? {
                       display: "flex",
                       _flag: 1
                   } : {
                       display: "a_invalid_value"
                   };
                   $scope.isPreviewHandlerShow = show;
               },
               insertText: function(text, position) {
                   var editor = $scope.markdownEditor;
                   position = position || editor.getCursorPosition();
                   editor.getSession().insert(position, text);
               },
               openPurePreview: function(){
                   $scope.purePreview = true;
               },
               queryNotesFromNoteBookId: function queryNotesFromNoteBookId(id, cb) {
                   $scope.loadingNoteList = true;
                   Note.query({
                       "notebook_id": id
                   }, function(notes){//success
                       $scope.notes = notes;
                       sortNotes();
                       $scope.loadingNoteList = false;
                       cb && cb.success();
                   }, function() {//fail
                       $scope.loadingNoteList = false;
                       cb && cb.fail();
                   });
               },
               resizeEditor: function() {
                   this.markdownEditor.resize(true);
               },
               markdown: (function(){
                   marked.setOptions({
                       gfm: true,
                       highlight: function (code, lang) {
                           var result = code,
                               lang = lang || "javascript";
                           try {
                               result = hljs.highlight(lang, code).value;//如果lang不在支持范围内，会报错
                           } catch (ex) {
                               result = hljs.highlightAuto(code).value;
                           }
                           return result;
                       },
                       tables: true,
                       breaks: false,
                       pedantic: false,
                       sanitize: false,
                       smartLists: true,
                       smartypants: false,
                       langPrefix: 'lang-'
                   });

                   // simple cache for performance reason.
                   var cacheKey = '';
                   var cacheValue = '';
                   var count = 0;

                   return function (input) {
                       if (cacheKey === input) {
                           return cacheValue;
                       }

                       cacheKey = input || "";

                       var tokens = marked.lexer(cacheKey);
                       var outline = $scope.outline = [];
                       angular.forEach(tokens, function(token) {
                           token.class = token.random + " mdSection";
                           if (token.type === "heading") {
                               token.id = "h" + (count++)%100000;
                               outline.push(token);
                           }
                       });
                       $scope.mdTokens = angular.copy(tokens);
                       cacheValue = $sce.trustAsHtml(marked.parser(tokens));

                       return cacheValue;
                   };
               }()),
               setTargetPlace: function(selector, offset) {
                   $scope.preview_target = selector;
                   $scope.preview_target_offset = offset || 0;
               },
               opencreateNoteBookDialog: function opencreateNoteBookDialog() {
                   var outScope = $scope;
                   $dialog.dialog({
                       backdrop: true,
                       keyboard: true,
                       backdropClick: true,
                       dialogClass:"modal auto-modal",
                       templateUrl: urlBase + "public/template/new-notebook.html", // OR: templateUrl: 'path/to/view.html',
                       controller: ['$scope', 'dialog', function ($scope, dialog) { //注：此处注入的dialog和$dialog是完全不同的
                           angular.extend($scope, {
                               title: '新建笔记本',
                               placeholder: '请输入新笔记本的名字',
                               save: function (name) {
                                   var nb = new NoteBook({
                                       name: name
                                   });
                                   nb.$save(function(){
                                       outScope.notebooks.push(nb);
                                       dialog.close();
                                   })
                               },
                               close: function() {
                                   dialog.close();
                               }
                           });
                       }]
                   }).open();
               },
               /**
                * 滑动的计数引用，为了防止循环滑动作用。
                * 当任意一边开始滑动时，如果scrollCount > 0, scrollCount--，然后直接return掉。
                * 当确认要作用于对方时，scrollCount ++
                */
               scrollCount: 0,
               /**
                * 当用户滑动preview区域时，触发。
                * @param evt
                * @param elm
                */
               previewScroll: function(evt, elm){
                   if ($scope.scrollCount) {
                       $scope.scrollCount--;
                       return;
                   }

                   var mdSectionNodese = elm.find(".mdSection"),
                       offsetTop = elm.offset().top,
                       offset,
                       top,
                       lastNode,
                       currentNode,
                       min_height = Number.MAX_VALUE,
                       height,
                       lookup;

                   for (var i = 0; i < mdSectionNodese.length; i++) {
                       currentNode = $(mdSectionNodese[i]);
                       top = currentNode.offset().top;
                       height = currentNode.height();
                       if (top - offsetTop > 0 ) {
                           if (top - offsetTop <= 20) {
                               lastNode = currentNode;
                               offset = 0;
                           }
                           break;
                       }
                       if (top < offsetTop && top + height > offsetTop && height < min_height) {
                           lastNode = currentNode;
                           offset = Math.abs(offsetTop - top) / height;
                           min_height = height;
                       }
                   }

                   if (!lastNode && !currentNode) {
                       console.log("no found!");
                       $scope.scrollCount++;
                       $scope.markdownEditor.scrollToLine(0);
                       return;
                   }
                   if (!lastNode) {
                       lastNode = currentNode;
                       offset = 0;
                   }

                   var classNames = lastNode.attr('class').split(/\s+/);
                   for (var i = 0; i < classNames.length; i++) {
                       if (/^_md-/.test(classNames[i].trim())) {
                           lookup = classNames[i].trim();
                       }
                   }

                   var mdTokens = $scope.mdTokens;
                   angular.forEach(mdTokens, function(token) {
                       if (token.random === lookup) {
                           console.log(lastNode.text());
                           console.log(JSON.stringify(token));
                           $scope.scrollCount++;
                           // $scope.markdownEditor.scrollToRow(token.startLine + Math.floor(token.span*offset));
                           $scope.markdownEditor.scrollToLine(token.startLine + Math.floor(token.span*offset));
                           return false;
                       }
                   });
               },
               /**
                * synchronizing scroll
                * 当用户滑动editor时，触发事件
                */
               mdScroll: function(){
                   console.log($scope.scrollCount);

                   if ($scope.scrollCount) {
                       $scope.scrollCount--;
                       return;
                   }
                   //1. find the target in current row
                   var tokens = $scope.mdTokens,
                       row = $scope.markdownEditor && $scope.markdownEditor.getFirstVisibleRow(),
                       target,
                       near,
                       diff =  Number.MAX_VALUE,
                       end_diff = Number.MAX_VALUE,
                       offset = 0;

                   if (row === undefined || !tokens || !tokens.length) {
                       return;
                   }

                   //row = row === 0 ? 0 : row + 2;
                   for(var i = 0, len = tokens.length; i < len; i++) {
                       var token = tokens[i];
                       if (token.type === 'space') continue;
                       if (token.type === 'html') continue;

                       if (token.startLine <= row && token.endLine >= row) {
                           if (token.endLine - token.startLine < diff && token.random) {
                               diff = token.span;
                               target = token;
                           }
                       }

                       if (token.endLine <= row && row - token.endLine < end_diff) {
                           near = token;
                       }
                   }

                   //compute offset
                   if (target) {
                       offset = (row - target.startLine) / diff * 100 + "%";
                   } else if (near) {
                       target = near;
                       offset = "100%";
                   } else {
                       $scope.setTargetPlace(0, 0);
                       return;
                   }

                   // offset may be computed as NaN if diff === 0
                   if (target.actual_span === 0) {
                       offset = $scope.markdownEditor.getFirstVisibleRowOffsetPercent();
                       offset = parseInt(offset * 100) + "%";
                       console.log("NaN:" + offset)
                   }
                   $scope.scrollCount++;
                   $scope.setTargetPlace("." + target.random, offset);
               }
           });

           $scope.$watch(function(){
               console.log($scope.scrollCount) ;
           });


           ["markdownEditorChange", "titleChange"].forEach(function(eventName){
               $scope.$on(eventName, function(event, dirty){
                   if ($scope.selectedNote) {
                       $scope.selectedNote.dirty = dirty;
                   }
               });
           });

       }])
       .config(["$httpProvider", function($httpProvider) {
           $httpProvider.interceptors.push(['$injector', '$q', function($injector, $q) {
               var isAjaxLogining = false;
               return {
                   'responseError': function (response) {
                       if (isAjaxLogining) return $q.reject(response);
                       // 401: Unauthorized, 表示用户需要登录
                       var $dialog = $injector.get("$dialog");
                       var $http = $injector.get("$http");
                       var urlBase = $injector.get("urlBase");
                       if (response.status === 401) {
                           $dialog.dialog({
                               backdrop: true,
                               keyboard: false,
                               backdropClick: false,
                               dialogClass: "modal auto-modal",
                               templateUrl: urlBase + "public/template/login-modal.html", // OR: templateUrl: 'path/to/view.html',
                               controller: ['$scope', 'dialog', function ($scope, dialog) { //注：此处注入的dialog和$dialog是完全不同的
                                   isAjaxLogining = true;
                                   angular.extend($scope, {
                                       login: function(email, password) {
                                           $http({
                                               method: "POST",
                                               url: urlBase + "rest/login",
                                               data: $.param({
                                                   email: email,
                                                   password: password
                                               }),
                                               headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                                           }).success(function(response){
                                                   console.log(response);
                                                   isAjaxLogining = false;
                                                   dialog.close();
                                               }).error(function(response){
                                                   console.log(response);
                                                   $scope.error = true;
                                               });
                                       }
                                   })
                               }]
                           }).open();
                       }
                       console.log(response);
                       return $q.reject(response);
                   }
               };
           }]);
       }])
       .value("prefix", "gn")
       .run(function(){
           //dirty jquery
           $(function(){
               //在Pure Preview时，当鼠标移上去时，让代码区尽可能大
               var ndPurePreview = $("#pure-preview");
               ndPurePreview.on("mouseenter mouseleave", "pre code", function(e){
                   var ndCode = $(this),
                       ndPre = ndCode.parent();
                   if (e.type === "mouseenter") {
                       var scrollWidth = ndCode[0].scrollWidth;
                       ndPre.animate({
                           width:  (scrollWidth + 10) + "px"
                       }, 200);
                   } else {
                       ndPre.css('width', "auto");
                   }
               });
           });
       });
}());;/**
 * Created with JetBrains WebStorm.
 * User: Administrator
 * Date: 5/13/13
 * Time: 11:15 PM
 * To change this template use File | Settings | File Templates.
 */
angular.module("ui.ssnau.buttonInput", [])

.directive("buttonInput", function(){
	return {
		restrict: "E",
		replace: true,
		scope: {
			prependList: "=prependList",
			appendList: "=appendList",
			prependModel: "=prepend",
			appendModel: "=append",
			inputModel: "=input"
		},
		template: '<div class="{{className}}">' +
								'<div class="btn-group">' +
									'<button data-toggle="dropdown" class="btn dropdown-toggle" style="display:{{displayPrepend}}">{{prependModel}}&nbsp;<span class="caret"></span></button>' +
									'<ul class="dropdown-menu">' +
										'<li ng-repeat="m in prependList"><a data-index="{{$index}}" type="prepend">{{m}}</a></li>'+
									'</ul>' +
								'</div>' +
								'<input type="text" ng-model="inputModel">' +
								'<div class="btn-group" style="display:{{displayAppend}}">' +
									'<button data-toggle="dropdown" class="btn dropdown-toggle">{{appendModel}}<span class="caret"></span></button>' +
									'<ul class="dropdown-menu">' +
										'<li ng-repeat="m in appendList"><a data-index="{{$index}}" type="append">{{m}}</a></li>' +
									'</ul>' +
								'</div>' +
							'</div>',
		link: function(scope, element, attrs) {
			scope.displayAppend = attrs.append ? '' : 'none';
			scope.displayPrepend = attrs.prepend ? '' : 'none';
			var className = '';
			className += attrs.append ? "input-append" : "";
			className += attrs.prepend ? "input-prepend" : "";
			scope.className = className;
			element.on("click", "a", function(){
				var index = $(this).attr("data-index"),
						type = $(this).attr("type");
				scope.$apply(function(){
						(type == "prepend") ?
						(scope.prependModel = scope.prependList[index]):
							(scope.appendModel = scope.appendList[index]);

				});
			});
		}
	}
});

/*用例
<button-input prepend="searchType" input="keyword"></button-input>
其中searchType和keyword均为当前scope中的变量,searchType= ["content", "tag"]
*/;angular.module("ui.ssnau.contextmenu", [])
.directive("hasContextmenu", ["$parse", function($parse){
        var contextMenus = [];

        var contextables = [];


        function closeAll() {
            contextMenus.forEach(function (ndMenu) {
                ndMenu.hide();
            });
        }

        var hasBind = false;
        function bindWindowEvent() {
            if (hasBind) return;
            $(document).on("click", function () {
                closeAll();
            });
            hasBind = true;
        }

        function positionMenu(e, ndMenu){
            var pos = {left: e.pageX, top: e.pageY},
                dem = {width: 5, height: 5},
                limit = {width: $(window).width(), height: $(window).height()};

            var left, top, right, bottom, useBottom, useRight;

            left = pos.left;
            top = pos.top + dem.height;

            var mHeight = ndMenu.height(),
                mWidth  = ndMenu.width(),
                beyondBottom = mHeight + top - limit.height,
                beyondRight  = mWidth + left - limit.width;

            //1. 超下界
            if (beyondBottom > 0) {
                //如果超上界(或不超)超的比下界小，则采用上界
                if (mHeight - top < beyondBottom) {
                    bottom = limit.height - pos.top;
                    useBottom = true;
                }
            }

            //2. 超右界
            if (beyondRight > 0) {
                right = 0;
                useRight = true;
            }

            //set to auto first
            ndMenu.css("top", "auto");
            ndMenu.css("bottom", "auto");
            ndMenu.css("left", "auto");
            ndMenu.css("right", "auto");

            if (useBottom) {
                ndMenu.css("bottom", bottom + "px");
            } else {
                ndMenu.css("top", top + "px");
            }

            if (useRight) {
                ndMenu.css("right", right + "px");
            } else {
                ndMenu.css("left", left + "px");
            }

        }

        return {
            link: function (scope, elem) {
                var ndMenu = $(elem).next();

                if (!ndMenu.hasClass("context-menu")) {
                    console.error("cannot found context menu for has-context element")
                }

                bindWindowEvent();

                ndMenu.css("position", "fixed");
                contextMenus.push(ndMenu);
                contextables.push(elem);

                elem.on('contextmenu', function (e) {
                    e.stopPropagation();
                    e.preventDefault();

                    console.log(e)
                    closeAll();

                    ndMenu.show();

                    positionMenu(e, ndMenu);
                });
            }
        }
}]);;angular.module("ui.ssnau.dnd", [])
    .directive("uiDnd", function() {

        var isDragging = false,
            info = null,
            cssOver = "dragging-over";

        function clear() {
            isDragging = false;
            info = null;
        }

        return function(scope, elem, attrs) {
            var config = scope.$eval(attrs['uiDnd']),
                dragFun = resolveFunc(config.drag || angular.noop),
                dropFun = resolveFunc(config.drop || angular.noop),
                allowDropFun = resolveFunc(config.allowDrop || angular.noop);

            function resolveFunc(f) {
                return angular.isString(f) ? function(){
                    return scope.$eval(f);
                } : f;
            }

            //如果允许drag
            if (config.drag) {
                elem.attr("draggable", true);
                elem.on('dragstart', function(){
                    clear();
                    info = dragFun();
                    isDragging = true;
                });
            }

            //如果允许drop
            if (config.drop) {
                //如果有allowDrop的属性，则通过查看allowDrop是否反正true来判断
                //是否allow drop
                elem.on('dragover', function(e) {
                    if (config.allowDrop) {
                        /**
                         * info即为dragstart时设进去的东西
                         * scope即为当前elem所在的scope,用以辅助AllowDropFunc做判断
                         * e：如果scope还不够用的话，就上e
                         */
                        if(allowDropFun(info, scope, e)) {
                            e.preventDefault();
                            elem.addClass(cssOver);
                        }
                    } else {
                        e.preventDefault();
                        elem.addClass(cssOver);
                    }
                });
                elem.on('dragleave', function(e) {
                    elem.removeClass(cssOver);
                });
                elem.on("drop", function(e){
                   if (isDragging && info) {
                       dropFun(info, scope, e);
                   }
                    clear();
                    elem.removeClass(cssOver);
                });
            }
        }
    });;angular.module("ui.ssnau.dockableFlexBox", [])
    .directive("dockableFlexBox", function () {
        var dragging = false;
        var meta = {};
        var ndBox = null;
        var hasBindDoc = false;

        function setCss(elem, name, value) {
            var n = name.substr(0,1).toUpperCase() + name.substr(1),
                elem = elem.length ? elem[0] : elem;

            //`Moz` the M is capitalized.
            ['webkit', 'Moz', 'ms', 'o'].forEach(function(prefix){
                elem.style[prefix + n] = value;
            });
            elem.style[name] = value;
        }

        function bindEvent(elem) {
            var ndElem = $(elem);
            ndElem.on("mousedown", "[dockable-handler]", handleDragStart);
            ndElem.on("mousemove", "[dockable]", handleDragOver);
            ndElem.on("mouseup", "[dockable]", handleDrop);
            bindDocument();
        }

        function bindDocument() {
            if (hasBindDoc) return;
            $(document).on("mouseup", clear);
            hasBindDoc = true;
        }

        function handleDragStart(e) {
            clear();
            dragging = true;
            var dockable = $(this).closest('[dockable]');
            meta = {
                source: $(dockable)
            };
            // Make every element on page unselectable
            $('*').addClass('unselectable');

            console.log("start....")
        }

        function handleDragOver(e) {
            var ndThis = $(this);
            if (!meta.source) return;//如果完全没有meta.source，比如从外部拖文件，直接关掉
            if (ndThis[0] === meta.source[0]) return; //如果跟source是同一个元素，无视之

            e.stopPropagation();
            e.preventDefault();
            console.log("over....")

            var region = computeRegion(ndThis, e);
            if (!region) {
                removeBox();
                return;
            }

            var direction = (region === "up" || region === "down") ? "column" : "row";
            drawBox(ndThis, region);
            meta.direction = direction;
            meta.region = region;

        }

        function clear() {
            //clear shits.
            dragging = false;
            removeBox();
            meta = {};
            $('*').removeClass('unselectable');
        }

        function handleDrop(e) {
            var ndTarget = $(this),
                ndSource = $(meta.source);

            if (!ndSource.length) {
                clear();
                return;
            }

            if (ndSource[0] == ndTarget[0]) {
                clear();
                return;
            }
            console.log("drop....")

            e.stopPropagation();
            e.preventDefault();

            //当两个对象具有相同父节点时，最简单情况。
            if (ndTarget.parent()[0] == ndSource.parent()[0]) {
                var ndParent = ndTarget.parent(),
                    sourceFisrt = (meta.region === "left" || meta.region === "up"),
                    order1 = 10,
                    order2 = 15,
                    order3 = 20;

                setCss(ndParent, "flexDirection", meta.direction);
                setCss(ndSource, "order", sourceFisrt ? order1 : order3);

                //We assume dockable flex box always allow user resize the inner box
                var ndDelimeter = ndParent.find(">.delimeter");
                if (!ndDelimeter.length) {
                    $("<div class='delimeter'></div>").appendTo(ndParent);
                }
                setCss(ndDelimeter, "order", order2);
                ndDelimeter.removeClass("delimeter-column");
                ndDelimeter.removeClass("delimeter-row");
                ndDelimeter.addClass("delimeter-" + meta.direction);
                setCss(ndTarget, "order", sourceFisrt ? order3 : order1);

                initDirectionProperties(meta.direction, [ndSource, ndTarget]);
            }

            //TODO: other condition.
            clear();

            //执行fb-onrsize中对应的代码
            ndTarget.data("fbOnresize") && ndTarget.data("fbOnresize")();
            ndSource.data("fbOnresize") && ndSource.data("fbOnresize")();

            function initDirectionProperties(direction, nodes) {
                angular.forEach(nodes, function (node) {
                    node = $(node);
                    node.css("width", "auto");
                    node.css("height", "auto");
                    setCss(node, 'flex', 1);
                });
            }
        }

        function getDockableId(node) {
            if (!node.data("dockable-id")) {
                node.data("dockable-id", s4() + s4());
            }
            return node.data("dockable-id");
        }

        function removeBox() {
            ndBox && ndBox.remove();
            ndBox = null;
        }

        function drawBox(node, region) {
            var direction = (region === "up" || region === "down") ? "column" : "row";
            var config = getDockableId(node) + region;

            if (meta.lastBoxConfig == config && ndBox) {
                return;
            };

            removeBox();

            var demension = {width: node.width(), height: node.height()},
                isFirst = region === 'up' || region === 'left';

            var html = ("<div class='flex' style='flex-direction:{{direction}};-webkit-flex-direction:{{direction}};position:absolute;left:0;top:0;z-index:9999;width:{{width}}px;height:{{height}}px'>" +
                "<div class='f1' style='border:2px #528CE0 solid;margin:2px;background:{{box1-bg}}'></div>" +
                "<div class='f1' style='border:2px #528CE0 solid;margin:2px;background:{{box2-bg}}'></div>" +
                "</div>").replace("{{direction}}", direction).replace("{{direction}}", direction)
                .replace("{{width}}", demension.width)
                .replace("{{height}}", demension.height)
                .replace("{{box1-bg}}", function(){
                    return isFirst ? "rgba(200,200,200,.4)" : "none";
                })
                .replace("{{box2-bg}}", function(){
                    return !isFirst ? "rgba(200,200,200,.4)" : "none";
                });

            ndBox = $(html);

            meta.lastBoxConfig = config;
            ndBox.appendTo($(node));
        }

        function computeRegion(node, e) {
            var offset = node.offset(), /*{top: int, left: int}*/
                demension = {width: node.width(), height: node.height()}, /*{width: int, height: int}*/
                pos = {left: e.pageX || e.originalEvent.pageX, top: e.pageY || e.originalEvent.pageY};

            //计算出当前位置离四条边的距离
            var info = {
                    up: pos.top - offset.top,
                    down: offset.top + demension.height - pos.top,
                    left: pos.left - offset.left,
                    right: offset.left + demension.width - pos.left
                },
                result = "up";

            angular.forEach(info, function (v, k) {
                result = (v < info[result]) ? k : result;
            });

            return result;
        }

        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };

        function activeChild(elem) {
            var ndDockables = $(elem).find("[dockable]")
            angular.forEach(ndDockables, function(dockable) {
                if ($(dockable).css("position") === "static") {
                    $(dockable).css("position", "relative");
                }
            });
        }

        function _closeDockable(dockable) {
            $(dockable).hide();

            var ndContainer = $(dockable).parent();
            ndContainer.find('>.delimeter').hide();
            angular.forEach(ndContainer.find('[dockable]'),function(dockable){
                dockable.style.webkitFlex = 1;
                dockable.style.flex = 1;
                $(dockable).data("fbOnresize") && $(dockable).data("fbOnresize")();
            });
        }

        function _showDockable(dockable) {
            $(dockable).show();
            var ndContainer = $(dockable).parent();
            ndContainer.find('>.delimeter').hide();
            var count = 0;
            angular.forEach(ndContainer.find('[dockable]'),function(dockable){
                if (dockable.style.display !== "none") count++;
                dockable.style.webkitFlex = 1;
                dockable.style.flex = 1;
                $(dockable).data("fbOnresize") && $(dockable).data("fbOnresize")();
            });

            if (count == 2) {
                ndContainer.find('>.delimeter').show();
            }
        }

        function bindClose(elem) {
            elem.on("click","[dfb-hide-dockable]", function(){
                _closeDockable($(this).parents("[dockable]")[0]);
            });
        }

        return {
            restrict: "A",
            link: function (scope, elem, attr) {
                activeChild(elem);
                bindEvent(elem);
                bindClose(elem);

                angular.extend(scope, {
                    toggleDockable: function (arg, bShow) {
                        var dockable = typeof arg === "string" ? document.getElementById(arg) : $(arg)[0];
                        if (!dockable.hasAttribute("dockable")) {
                            console.error("The target dockable {{id}} seems not to be a dockable".replace("{{id}}", id));
                        }

                        if (bShow) {
                            _showDockable(dockable);
                        } else {
                            _closeDockable(dockable);
                        }

                    }
                });

            }
        };
    })
/**
 * 1. 监听ngShow的变化，一旦改变，调用相应的show/close方法，引发容器变化
 */
    .directive("dockable", function(){
        return {
            link: function(scope, elem, attr) {
                if (attr.ngShow) {
                    scope.$watch(attr.ngShow, function(show) {
                           scope.toggleDockable(elem, show);
                    });
                }
            }
        }
    })





/**
 用法：
 <div dockable-flex-box> <!-- 定义了一个dockable flex box-->
    <div dockable>     <!-- 定义了其中的一个dockable的部件 -->
        <div dockable-handler></div>   <!-- handler是给用户拖拽的 -->
        <button dfb-hide-dockable>close</button> <!-- 点击这个按钮关闭 -->
    </div>
    <div dockable><div>
 </div>







 **/;angular.module("ui.ssnau.draggable", [])
.directive("uiDraggable", function() {
      var isDragging = false;
      var curtain = $("<div class='waitable-container unselectable' onselectstart='e.preventDefault();return false;'  style='position:absolute;cursor:all-scroll;z-index:9999;top:0px;left:0px;bottom:0px;right:0px;'></div>");
      var pos = {};
      var mousemoveHandlers = [];
      var ndDoc = $(document);

      ndDoc.on("mouseup", clear);

      function clear() {
          isDragging = false;
          curtain.hide();
          mousemoveHandlers.forEach(function(f){
              ndDoc.off("mousemove", f);
          });
          mousemoveHandlers = [];
      }
      return function(scope, elem, attrs) {
          var config = scope.$eval(attrs.uiDraggableConfig),
              target = elem;

          if (config && config.target) {
              target = $(elem.parents(config.target)[0]);
          }

          elem.css("cursor", "all-scroll");

          elem.on("mousedown", function(e){
              //自已开始，往上找直到elem[0]（不包括elem[0]）,看看是否有绑定ng-click的
              //如果有，则取消mousedown的处理
              var ndTarget = $(e.target);
              var hasClickEvent = ndTarget.closest("[ng-click]", elem[0]).length;

              //再判断一下是否有`no-draggable`的东西，这是一个用于标记的属性
              if (hasClickEvent || ndTarget.closest("[no-draggable]", elem[0]).length) {
                  return;
              }

              isDragging = true;
              curtain.appendTo(target);
              curtain.show();

              pos = {
                  x: e.pageX || e.originalEvent.pageX,
                  y: e.pageY || e.originalEvent.pageY,
                  left: parseInt(target.css("left")),
                  top: parseInt(target.css("top"))
              };

              ndDoc.on("mousemove", mousemoveHandler);
              mousemoveHandlers.push(mousemoveHandler);
          });

          elem.on("mouseup", clear);

          target.on("mousemove", mousemoveHandler);

          function mousemoveHandler(e) {
              if (!isDragging) return;
              var cur = {
                  x: e.pageX || e.originalEvent.pageX,
                  y: e.pageY || e.originalEvent.pageY
              };

              var offsetX = cur.x - pos.x,
                  offsetY = cur.y - pos.y;

              target.css("top", pos.top + offsetY + "px");
              target.css("left", pos.left + offsetX + "px");
          }

      }
    });;angular.module("ui.ssnau.flexBox", [])
    .directive("flexBox", function () {
        var isDragging = false;
        var $ = angular.element;
        var meta = {};
        var hasBind = false;
        var ndCurtain = $("<div style='position:absolute;width:100%;height:100%;opacity:0;z-index:1000;left:0px;top:0px;'></div>");
        var curtains = [];
        var ndImproveCurtain = null;

        function bindMouseEvent(elem) {
            $(elem).on("mousemove", function (e) {
                if (!isDragging) return;
                var ndElem = meta.delimeter,
                    ndPrev = ndElem.prev(),
                    ndNext = ndElem.next() == ndCurtain ? [] : ndElem.next(),
                    pageX = e.pageX || e.originalEvent.pageX,
                    pageY = e.pageY || e.originalEvent.pageY;


                //adjust prev and next based on order
                if (ndPrev[0].style.order > ndNext[0].style.order) {
                    var tmp;
                    tmp = ndPrev;
                    ndPrev = ndNext;
                    ndNext = tmp;
                }

                if (meta.direction === "row") {
                    processDiff([ndPrev, ndNext], "width", pageX - meta.x, ["+", "-"]);
                }

                if (meta.direction === "column") {
                    processDiff([ndPrev, ndNext], "height", pageY - meta.y, ["+", "-"]);
                }

                //renew meta info
                meta.x = pageX;
                meta.y = pageY;

                //执行fb-onrsize中对应的代码
                ndPrev.data("fbOnresize") && ndPrev.data("fbOnresize")();
                ndNext.data("fbOnresize") && ndNext.data("fbOnresize")();
            });

            $(elem).on("mousedown", ">.delimeter", function (e) {
                processMouseDown(this, e);
                e.preventDefault();
                e.stopPropagation();
            });

            function processMouseDown(ndDelimeter, e) {
                isDragging = true;
                meta = {
                    delimeter: $(ndDelimeter), //delimeter is self
                    direction: flexDirection(elem),
                    x: e.pageX || e.originalEvent.pageX,
                    y: e.pageY || e.originalEvent.pageY
                };
                ndCurtain.appendTo(elem);
                curtains.push(ndCurtain);
                ndCurtain.css("cursor", (meta.direction === "row") ? "col-resize" : "row-resize");
            }

            /**
             * 如果delimeter很小，则不方便定位，故显示出ndImproveCurtain来代理delimeter.
             */
            $(elem).on("mouseover", ">.delimeter", function(e){
                var offset = $(this).offset(),
                    me = $(this),
                    direction = /row/.test(me.css("cursor")) ? "column" : "row";

                ndImproveCurtain && ndImproveCurtain.remove();

                var html = ("<div style='position:fixed;left:{{left}};top:{{top}};cursor:{{cursor}};width:{{width}};height:{{height}};background:rgba(100,100,100,0.3);z-index:1000'></div>")
                            .replace("{{left}}", (direction === "row" ? offset.left : offset.left) + "px")
                    .replace("{{top}}", (direction === "row" ? offset.top : offset.top) + "px")
                    .replace("{{cursor}}", me.css("cursor"))
                    .replace("{{width}}", function(){
                        return direction === "row" ? "10px" : me.width() + "px";
                    })
                    .replace("{{height}}", function(){
                        return direction === "column" ? "10px" : me.height() + "px";
                    });

                ndImproveCurtain = $(html);
                ndImproveCurtain.appendTo($(document.body));
                curtains.push(ndImproveCurtain);
                ndImproveCurtain.on("mousedown", function(e){
                    processMouseDown(me, e);
                    e.preventDefault();
                    e.stopPropagation();
                });
                ndImproveCurtain.on("mouseout", function(){
                    ndImproveCurtain && ndImproveCurtain.remove();
                    ndImproveCurtain = null;
                })
            });
            function clearDragging() {
                isDragging = false;
                meta = {};
                curtains.forEach(function(curtain) {
                    curtain.remove();
                });
                curtains = [];
            }

            $(elem).on("mouseup", clearDragging);

            //only bind globally once
            if (hasBind) return;
            hasBind = true;
            $(document).on("mouseup", clearDragging);
        }

        /**
         *
         * @param nodes Array [JQueryNode]
         * @param demension "width" | "height"
         * @param diff integer
         * @param ops Array ["+" | "-"]
         */
        function processDiff(nodes, demension, diff, ops) {
            var results = [], node;

            for (var i = 0; node = nodes[i]; i++) {
                if (!node.length) {
                    results.push(null);
                    continue;
                }
                ;
                var orig = (demension === "width") ? node.width() : node.height(),
                    result = (ops[i] === "+") ? (orig + diff) : (orig - diff),
                    min = parseInt(window.getComputedStyle(node[0])["min" + demension[0].toUpperCase() + demension.substr(1)]) || 0;

                if (result <= min) return; //Only if there is a result being 0, end this function
                results.push(result);
            }

            results.forEach(function (result, index) {
                    if (nodes[index].length) {
                        if (!nodes[index][0].hasAttribute("fill-flexable")) {
                            nodes[index][0].style.flex = "initial";//基于某种奇怪的原因，还是去掉flex属性比较好
                            nodes[index].css(demension, result + "px");
                        }
                    }
                }
            );
        }

        function flexDirection(elem) {
            var el = elem.length ? elem[0] : elem,
                cs = window.getComputedStyle(el);
            return cs.webkitFlexDirection || cs.flexDirection;
        }

        return {
            restrict: "A",
            link: function (scope, elem) {
                $(elem).css("display", "flex");
                bindMouseEvent(elem);
            }
        };
    })
/**
 * 用法：当元素被resize时，会触发fb-onresize中的代码
 * <div fb-onresize="editor.resize()">
 * 当一个元素的fb-onresize调用时，会联动其所有的子元素。
 */
    .directive("fbOnresize", ['$parse', function ($parse) {
        return function (scope, element, attr) {
            var fn = $parse(attr["fbOnresize"]);

            element.data("fbOnresize", function(isCallFromInside/*Never Pass `true` unless you know it!*/){
                fn(scope);
                if (!isCallFromInside) {
                    var nodes = element.find("[fb-onresize]");
                    angular.forEach(nodes, function(node){
                        $(node).data("fbOnresize")(true);
                    })
                }

            });
        };
    }])

;/**
 * Created with JetBrains WebStorm.
 * User: Administrator
 * Date: 5/11/13
 * Time: 12:54 AM
 * To change this template use File | Settings | File Templates.
 */
angular.module("renderer.ssnau.highlightWord", [])

.directive("highlightWord", function(){
	return {
		restrict: "A",
		link: function(scope, element, attrs){
			var word = '', content, count = 0;
			scope.$watch(attrs["highlightWord"], function(val){
				word = val;
				//如果有listen属性，则调用render方法
				if (attrs['listen']) {
					render();
				} else {
					//仅第一次从当中取，因为只有第一次是干净的
					content = ((count++) === 0) ? element.html() : content;
					if (!val) return;
					var c = content.replace(new RegExp(val, "g"), function(match){
						return "<span class='highlighted'>" + match + "</span>"
					});
					element.html(c);
				}
			});

			scope.$watch(attrs['listen'], function(val){
				content = val;
				render();
			});

			function render(){
				if (content) {
					var c = word ? content.replace(new RegExp(word, "g"), function(match){
						return "<span class='highlighted'>" + match + "</span>"
					}) : content;
					element.html(c);
				}
			}

		}
	}
});

/**
 用法：
 <div highlight-word="keyword" listen="doc">
  {{doc}}
 </div>

 说明：
 1. 每当keyword变化，会重新根据已有内容进行高亮。
 2. 如果有listen属性，则会监听listen对应的值，用该值来render内容及高亮之。
 3. 如果没有listen属性，则直接取其innerHTML的内容进行高亮。
 */;angular.module("ui.ssnau.keyboard", [])
/**
 * 用法：
 * ui-keydown="{keyCode: functionName}" 或
 * ui-keydown="{keyCode: expression}"
 * 如果keyCode为`*`，则表示任何键的按下都会触发事件。
 * 但对于已经设定了事件的keyCode则无效。
 * 如{
 *   "*": run,
 *   "13": fly
 * }
 * 当用户按下`Enter`时，只会触发fly，而不是run+fly.
 * 而当用户按下其它键时，都会触发run.
 *
 * <input type='text' ui-keydown="{13: submit}" />
 * $scope.submit = function(){
 *  console.log("submiting");
 * }
 * <input type='text' ui-keydown="{13: 'save(note)'}" />
 * $scope.save = function(note){
 *   note.$save();
 * }
 */
    .directive("uiKeydown", ["$parse", function ($parse) {
        return function (scope, elem, attr) {
            var config = scope.$eval(attr.uiKeydown);

            elem.on("keydown", function(e){
               var keyCode = e.keyCode,
                   action = config[keyCode] || config[keyCode + '']  || config["*"];

               if (!action) return;

               scope.$apply(function(){
                   if (angular.isFunction(action)) {
                       action();
                   } else {
                       scope.$eval(action);
                   }
               });
            });
        }
    }]);;/**
 * Created with JetBrains WebStorm.
 * User: Administrator
 * Date: 5/10/13
 * Time: 10:04 PM
 * To change this template use File | Settings | File Templates.
 */

angular.module("ui.ssnau.powerButton", [])

.directive("powerButton", function(){
		return {
			restrict: "E",
			require: '?ngModel',
			replace: true,
			scope: {
				label: "@label",
				innerModel: "=ngModel"//让isolate scope内部可以引用到外部ngModel的对象
			},
			template: '<div>' +
									'<label class="share-label">{{label}}</label>' +
									'<div class="toggle">' +
											'<label class="toggle-radio">ON</label>' +
											'<label class="toggle-radio">OFF</label>' +
									'</div>' +
								'</div>',
			link: function(scope, element) {
				var toggleNode = $(element[0].children[1]);

				element.on('click', 'label', function(){
					toggleNode.toggleClass("toggle-off");
					var isOn = !toggleNode.hasClass('toggle-off');
					scope.$apply(function() {
						scope.innerModel = isOn;
					});
				});

				//innerModel绑定了外部的ngModel，故在此处watch即可知晓外部变化
				scope.$watch("innerModel", function(newVal){
					render(newVal);
				});

				function render(isOn){
					isOn ? toggleNode.removeClass("toggle-off") : toggleNode.addClass("toggle-off");
				}
			}
		}
});

/**
 * @ngdoc directive
 * @name ui.ssnau.directive.powerButton
 *
 * @element power-button
 *
 * @example
 <doc:example>
 <doc:source>
		<power-button label="WIFI" ng-model="wifiEnable"></power-button>
 </doc:source>
 </doc:example>
 */;angular.module("ui.ssnau.resize", [])
.directive("fullHeight", function () {
    return {
        link: function (scope, elem) {
            var ndParent = elem.parent();
            ndParent.on("resize", function () {
                elem.css("width", ndParent.width() + "px");
            });
        }
    }
})
.directive("fullWindow", function () {
    return {
        link: function (scope, elem) {
            $(window).on("resize", function () {
                elem.css("width", $(window).width() + "px");
                elem.css("height", $(window).height() + "px");
            });
            elem.css("marginLeft", 0);
            elem.css("marginTop", 0);
            elem.css("marginRight", 0);
            elem.css("marginBottom", 0);
            elem.css("width", $(window).width() + "px");
            elem.css("height", $(window).height() + "px");
        }
    }
});;angular.module("simple.ssnau", [])
    .directive("showOnHoverParent", function () {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };

        return {
            link: function (scope, elem) {
                var ndElem = $(elem),
                    ndParent = ndElem.parent();

                if (!ndParent.attr("id")) ndParent.attr("id", "n" + s4() + s4() + s4());
                if (!ndElem.attr("id")) ndElem.attr("id", "n" + s4() + s4() + s4());

                var display = ndElem.css("display");
                var css = ('#==pid==:hover #==eid== {display: ==display== }\n ' +
                    '#==pid== #==eid== {display:none;}')
                    .replace(/==display==/g, display)
                    .replace(/==pid==/g, ndParent.attr("id"))
                    .replace(/==eid==/g, ndElem.attr("id"));

                var style = document.createElement('style');
                if (style.styleSheet)
                    style.styleSheet.cssText = css;
                else
                    style.appendChild(document.createTextNode(css));

                document.getElementsByTagName('head')[0].appendChild(style);
            }
        }
    })

/**
 * <div ace-editor="{name: 'markdownEditor', scroll: 'expression'}"/>, so $scope.markdownEditor = {{The editor instance}}
 */
    .directive("aceEditor", function () {
        return {
            require: '?ngModel',
            link: function (scope, elem, attr, ctrl) {
                if (typeof ace === undefined) {
                    alert("no ACE Editor found!!");
                    return;
                }

                var editor = ace.edit(elem[0]),
                    config = scope.$eval(attr.aceEditor);

                //Sets whether or not line wrapping is enabled
                editor.getSession().setUseWrapMode(true);
                editor.getSession().setUseSoftTabs(true);
                editor.setShowPrintMargin(false);

                var editorName = config.name;
                if (editorName) {
                    scope[editorName] = editor;
                }

                if (config.scroll) {
                    editor.getSession().on("changeScrollTop", _.debounce(function() {
                        if (angular.isFunction(config.scroll)) {
                            config.scroll();
                        } else {
                            scope.$eval(config.scroll);
                        }
                        scope.$apply();
                    }, 500));
                    editor.getSession().on("changeCursor", function() {
                        console.log('cursor gone...')
                    });
                    editor.getSession().on("changeSelection", function() {
                        console.log('selction gone...')
                    });

                }

                editor.getSession().setMode('ace/mode/markdown');
                editor.setTheme("ace/theme/monokai");

                editor.$notify = function() {
                    ctrl.$setViewValue(editor.getSession().getValue());
                    //发出事件到上级Scope,依赖于editorName
                    if (editorName) {
                        scope.$emit(editorName + "Change", editor.getSession().getUndoManager().$undoStack.length);
                    }
                    scope.$apply();
                };



                elem.on('keyup', 'textarea', function () {
                    editor.$notify();
                });

                ctrl.$render = function() {
                    editor.getSession().setValue((ctrl.$viewValue ? ctrl.$viewValue : ''));
                };

                ctrl.$setViewValue(editor.getSession().getValue());

                //enhancement API
                editor.getFirstVisibleRowOffsetPercent = function() {
                    var contentNode = elem.find(".ace_content"),
                        topOffset = parseFloat(contentNode.css("margin-top")),
                        firstRowNode = elem.find(".ace_content>.ace_text-layer>.ace_line_group").first();

                    if (!firstRowNode.length) return null;

                    var rowHeight = firstRowNode.outerHeight();

                    return Math.abs(topOffset) / rowHeight;
                }
            }
        }
    })
/**
 * Usage（如果我们希望一个按钮有dropdown menu,则紧随其后定义一个class为dropdown-menu的元素即可）:
 <div class="edit-box-tool-icon clickable" has-dropdown="">
    <i class="icon-beaker"></i>
 </div>
 <ul class="dropdown-menu">
    <li>1</li>
    <li>2</li>
    <li>3</li>
 </ul>
 */
    .directive("hasDropdown", function () {

        var dropdownMenus = [];

        var dropdownable = [];

        var lastOpen = null;

        function closeAll() {
            dropdownMenus.forEach(function (ndMenu) {
                ndMenu.hide();
            });
            dropdownable.forEach(function(dropdown){
                dropdown.removeClass("selected");
            });

            lastOpen = null;
        }

        var hasBind = false;
        function bindWindowEvent() {
            if (hasBind) return;
            $(document).on("click", function () {
                closeAll();
            });
            hasBind = true;
        }


        function positionMenu(elem, ndMenu){
            var pos = {left: elem.offset().left, top: elem.offset().top},
                dem = {width: elem.outerWidth(), height: elem.outerHeight()},
                limit = {width: $(window).width(), height: $(window).height()};

            var left, top, right, bottom, useBottom, useRight;

            left = pos.left;
            top = pos.top + dem.height;

            var mHeight = ndMenu.outerHeight(),
                mWidth  = ndMenu.outerWidth(),
                beyondBottom = mHeight + top - limit.height,
                beyondRight  = mWidth + left - limit.width;

            //1. 超下界
            if (beyondBottom > 0) {
                //如果超上界(或不超)超的比下界小，则采用上界
                if (mHeight - top < beyondBottom) {
                    bottom = limit.height - pos.top;
                    useBottom = true;
                }
            }

            //2. 超右界
            if (beyondRight > 0) {
                right = 0;
                useRight = true;
            }

            //set to auto first
            ndMenu.css("top", "auto");
            ndMenu.css("bottom", "auto");
            ndMenu.css("left", "auto");
            ndMenu.css("right", "auto");

            if (useBottom) {
                ndMenu.css("bottom", bottom + "px");
            } else {
                ndMenu.css("top", top + "px");
            }

            if (useRight) {
                ndMenu.css("right", right + "px");
            } else {
                ndMenu.css("left", left + "px");
            }

        }

        return {
            link: function (scope, elem, attr, ctrl) {
                var ndMenu = $(elem).next();

                if (!ndMenu.hasClass("dropdown-menu")) {
                    console.error("cannot found dropdown menu for has-dropdown element")
                }

                bindWindowEvent();

                ndMenu.css("position", "fixed");
                ndMenu.css("zIndex", 9999);
                dropdownMenus.push(ndMenu);
                dropdownable.push(elem);

                elem.on('click', function (e) {
                    e.stopPropagation();
                    e.preventDefault();

                    if (lastOpen == ndMenu) {
                        closeAll(); //cannot put it outside the if-statement, because closeAll() would change `lastOpen`
                        return;
                    }

                    closeAll();

                    $(elem).addClass("selected");

                    ndMenu.show();

                    positionMenu(elem, ndMenu);

                    lastOpen = ndMenu;
                });
            }
        }
    })

/**
 * config的参数：
 * onchange: 当内容改变时，触发的事件
 * onreset: 当内容恢复成初始的$viewValue时
 * max: 内容最大长度
 * singleLine: 是否是单行
 * enable: 监听变量，表示是否启用contenteditable
 * <span contenteditable="true" contenteditable-config="{onchange:[func|expression]}">
 */
    .directive('contenteditable', function() {
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                var editorName = attrs.contenteditableName,
                    config = scope.$eval(attrs.contenteditableConfig),
                    isSingleLine = config.singleLine,
                    maxLength = config.max - 0,
                    onchangeFunc = resolveFunc(config.onchange || angular.noop),
                    onresetFunc = resolveFunc(config.onreset || angular.noop),
                    enable = config.enable,
                    oriText = "",
                    dirty = false;

                function resolveFunc(f) {
                    return angular.isString(f) ? function(){
                        scope.$eval(f);
                    } : f;
                }

                //如果有设enable监听属性的话
                if (enable) {
                    scope.$watch(enable, function(v){
                        elm.attr('contenteditable', !!v);
                    })
                }

                //只有keydown才能阻止事件的发生
                elm.on('keydown', function(e) {
                    var keyCode = e.keyCode;

                    //Nothing happens for Up/Down/Left/Right
                    if (keyCode >=37 && keyCode <=40) { //UP,DOWN,LEFT,RIGHT
                        return;
                    }

                    //Enter表示确认，因为signleLine的enter没有其它意义
                    if (isSingleLine && keyCode == 13) {//Enter
                        elm.blur();
                        e.preventDefault();
                    }

                    // 如果有maxLength
                    // 1. 但仅Up/Down/Left/Right/Backspace/Delete有效
                    // 2. 组合键仍有效，因为用户可能想ctrl + A
                    // 3. 如果用户已经选中了文字，则表示要替换内容，此时放行 TODO: 是否考虑Mac的Command键
                    if (maxLength) {
                        //如果有selection.
                        var selection = window.getSelection();
                        if (selection && selection.type == "Range") {
                            return;
                        }
                        if (elm.text().length >= maxLength) {
                            if (keyCode != 8 && keyCode != 46 && e.ctrlKey == false) {//8->Backspace, 46->Delete
                                e.preventDefault();
                            }
                        }
                    }
                });

                // view -> model
                elm.on('keyup', function() {
                    var innerText = elm.text(),
                        text      = normalize(innerText);

                    // Will here run into preformance issue?
                    if (innerText != text) {
                        elm.text(text); //browser compatible?
                    }

                    dirty = text != oriText;

                    if (dirty){
                        onchangeFunc();
                    } else {
                        onresetFunc();
                    }
                    scope.$apply(function() {
                        ctrl.$setViewValue(text);
                    });
                });

                // model -> view
                ctrl.$render = function() {
                    oriText = ctrl.$viewValue ? ctrl.$viewValue : '';
                    dirty = false;
                    elm.html(oriText);
                };

                // load init value from DOM
                var initText = elm.text();
                if (initText) {
                    oriText = normalize(initText);
                    ctrl.$setViewValue(oriText);
                }


                function normalize(text) {
                    var res = text;
                    if (isSingleLine) {
                        res = text.split("\n")[0];
                    }

                    if (maxLength) {
                        res = text.substr(0, maxLength);
                    }
                    return res;
                }
            }
        };
    })
/**
 * config的参数：
 * target: required target应该是一个selector或一个数字，表示要scroll过去的目标/坐标
 * offset: optional 表示在target的基础上的偏移量，可接收数字或百分比。
 *         如果是数字的话，则offset即为数字本身。
 *         如果是百分比，则offset为target.height() * 百分比
 *                     如果target为数字的话，则 target * 百分比
 *
 * <div scroll-top="{target:selectedId, animate: speed}">
 */
.directive("scrollTop", function(){
        return {
            link: function(scope, elm, attrs, ctrl) {
                var config = scope.$eval(attrs.scrollTop);

                if (config.target || config.offset) {
                    var key = config.target;
                    key += '+' + (config.offset || '" "');
                    scope.$watch(key, function() {
                        var selector = scope[config.target],
                            offset = (config.offset && scope[config.offset]) || 0,
                            pTop = elm.offset().top,
                            ndTarget = (typeof selector === "string") && elm.find(selector),
                            dest;

                        var cTop = (ndTarget.length && ndTarget.offset().top),
                            sTop = elm.scrollTop();


                        if (cTop) {//表示传入来的是Selector
                            dest = cTop-pTop+sTop;
                        } else {
                            //传进来的是一个绝对值
                            dest = selector;
                        }


                        // if offset is percent, convert it to number base on target
                        if (/%$/.test(offset + '')) {
                            // 正负号还会保留
                            offset = parseFloat(offset) / 100;
                            if (ndTarget.length) {
                                offset = ndTarget.outerHeight() * offset;
                            } else {
                                offset = ndTarget * offset;
                            }
                        }

                        if (!isNaN(dest)) {
                            elm.animate({scrollTop:dest + offset}, '500');
                        }
                    });
                }
            }
        }
    })
.directive("scroll", ['$parse', function($parse){
        return function ($scope, elm, attrs) {
            var fn = $scope.$eval(attrs.scroll);
            elm.bind("scroll", _.debounce(function (evt) {
                fn(evt, elm);
                if (!$scope.$$phase) {
                    $scope.$apply();
                }
            }, 300));
        };
    }]);/**
 * Created with JetBrains WebStorm.
 * User: Administrator
 * Date: 5/9/13
 * Time: 11:19 PM
 * To change this template use File | Settings | File Templates.
 */
angular.module("ui.ssnau.tagInput", [])

.directive("tagInput", function(){
		return {
			require: '?ngModel',
			link: function(scope, element, attrs, ctrl) {
				//这个插件在运行时会动态插入元素并设ID，
				//同时又会用query(ID)的方式来获取元素
				//因为在compile的时候可能尚未将元素放入DOM树中
				//所以导致挂掉，因此在这里弄个timeout，延后执行
				setTimeout(function(){
					element.tagsInput({
						onChange: function(){ //绑定onChange，使angular知晓tags的变化，响应到ng-model绑定的对象中
							if (!ctrl) return;
							scope.$apply(function() {
								ctrl.$setViewValue(element.val());
							})
						},
						height: "50px",
						width: "100%"
					});
				}, 1);
			}
		};
});

/*
* 下面的代码是一个JQuery插件，拥有它就可以使得$("element").tagInput可使用了。
* 因为directive中的内容在compile时才会执行到，而下面的代码在文件加载时便执行了，所以
* 可以将其放在下面而不必担心direcitve引用不到。
* */

/*

 jQuery Tags Input Plugin 1.3.3

 Copyright (c) 2011 XOXCO, Inc

 Documentation for this plugin lives here:
 http://xoxco.com/clickable/jquery-tags-input

 Licensed under the MIT license:
 http://www.opensource.org/licenses/mit-license.php

 ben@xoxco.com

 */

/*
(function($) {

	var delimiter = new Array();
	var tags_callbacks = new Array();
	$.fn.doAutosize = function(o){
		var minWidth = $(this).data('minwidth'),
			maxWidth = $(this).data('maxwidth'),
			val = '',
			input = $(this),
			testSubject = $('#'+$(this).data('tester_id'));

		if (val === (val = input.val())) {return;}

		// Enter new content into testSubject
		var escaped = val.replace(/&/g, '&amp;').replace(/\s/g,' ').replace(/</g, '&lt;').replace(/>/g, '&gt;');
		testSubject.html(escaped);
		// Calculate new width + whether to change
		var testerWidth = testSubject.width(),
			newWidth = (testerWidth + o.comfortZone) >= minWidth ? testerWidth + o.comfortZone : minWidth,
			currentWidth = input.width(),
			isValidWidthChange = (newWidth < currentWidth && newWidth >= minWidth)
				|| (newWidth > minWidth && newWidth < maxWidth);

		// Animate width
		if (isValidWidthChange) {
			input.width(newWidth);
		}


	};
	$.fn.resetAutosize = function(options){
		// alert(JSON.stringify(options));
		var minWidth =  $(this).data('minwidth') || options.minInputWidth || $(this).width(),
			maxWidth = $(this).data('maxwidth') || options.maxInputWidth || ($(this).closest('.tagsinput').width() - options.inputPadding),
			val = '',
			input = $(this),
			testSubject = $('<tester/>').css({
				position: 'absolute',
				top: -9999,
				left: -9999,
				width: 'auto',
				fontSize: input.css('fontSize'),
				fontFamily: input.css('fontFamily'),
				fontWeight: input.css('fontWeight'),
				letterSpacing: input.css('letterSpacing'),
				whiteSpace: 'nowrap'
			}),
			testerId = $(this).attr('id')+'_autosize_tester';
		if(! $('#'+testerId).length > 0){
			testSubject.attr('id', testerId);
			testSubject.appendTo('body');
		}

		input.data('minwidth', minWidth);
		input.data('maxwidth', maxWidth);
		input.data('tester_id', testerId);
		input.css('width', minWidth);
	};

	$.fn.addTag = function(value,options) {
		options = jQuery.extend({focus:false,callback:true},options);
		this.each(function() {
			var id = $(this).attr('id');

			var tagslist = $(this).val().split(delimiter[id]);
			if (tagslist[0] == '') {
				tagslist = new Array();
			}

			value = jQuery.trim(value);

			if (options.unique) {
				var skipTag = $(this).tagExist(value);
				if(skipTag == true) {
					//Marks fake input as not_valid to let styling it
					$('#'+id+'_tag').addClass('not_valid');
				}
			} else {
				var skipTag = false;
			}

			if (value !='' && skipTag != true) {
				$('<span>').addClass('tag').append(
					$('<span>').text(value).append('&nbsp;&nbsp;'),
					$('<a class="tagsinput-remove-link fui-cross-16">', {
						href  : '#',
						title : 'Remove tag',
						text  : ''
					}).click(function () {
							return $('#' + id).removeTag(escape(value));
						})
				).insertBefore('#' + id + '_addTag');

				tagslist.push(value);

				$('#'+id+'_tag').val('');
				if (options.focus) {
					$('#'+id+'_tag').focus();
				} else {
					$('#'+id+'_tag').blur();
				}

				$.fn.tagsInput.updateTagsField(this,tagslist);

				if (options.callback && tags_callbacks[id] && tags_callbacks[id]['onAddTag']) {
					var f = tags_callbacks[id]['onAddTag'];
					f.call(this, value);
				}
				if(tags_callbacks[id] && tags_callbacks[id]['onChange'])
				{
					var i = tagslist.length;
					var f = tags_callbacks[id]['onChange'];
					f.call(this, $(this), tagslist[i-1]);
				}
			}

		});

		return false;
	};

	$.fn.removeTag = function(value) {
		value = unescape(value);
		this.each(function() {
			var id = $(this).attr('id');

			var old = $(this).val().split(delimiter[id]);

			$('#'+id+'_tagsinput .tag').remove();
			str = '';
			for (i=0; i< old.length; i++) {
				if (old[i]!=value) {
					str = str + delimiter[id] +old[i];
				}
			}

			$.fn.tagsInput.importTags(this,str);

			if (tags_callbacks[id] && tags_callbacks[id]['onRemoveTag']) {
				var f = tags_callbacks[id]['onRemoveTag'];
				f.call(this, value);
			}
		});

		return false;
	};

	$.fn.tagExist = function(val) {
		var id = $(this).attr('id');
		var tagslist = $(this).val().split(delimiter[id]);
		return (jQuery.inArray(val, tagslist) >= 0); //true when tag exists, false when not
	};

	// clear all existing tags and import new ones from a string
	$.fn.importTags = function(str) {
		id = $(this).attr('id');
		$('#'+id+'_tagsinput .tag').remove();
		$.fn.tagsInput.importTags(this,str);
	}

	$.fn.tagsInput = function(options) {
		var settings = jQuery.extend({
			interactive:true,
			defaultText:'',
			minChars:0,
			width:'',
			height:'',
			autocomplete: {selectFirst: false },
			'hide':true,
			'delimiter':',',
			'unique':true,
			removeWithBackspace:true,
			placeholderColor:'#666666',
			autosize: true,
			comfortZone: 20,
			inputPadding: 6*2
		},options);

		this.each(function() {
			if (settings.hide) {
				$(this).hide();
			}
			var id = $(this).attr('id');
			if (!id || delimiter[$(this).attr('id')]) {
				id = $(this).attr('id', 'tags' + new Date().getTime()).attr('id');
			}

			var data = jQuery.extend({
				pid:id,
				real_input: '#'+id,
				holder: '#'+id+'_tagsinput',
				input_wrapper: '#'+id+'_addTag',
				fake_input: '#'+id+'_tag'
			},settings);

			delimiter[id] = data.delimiter;

			if (settings.onAddTag || settings.onRemoveTag || settings.onChange) {
				tags_callbacks[id] = new Array();
				tags_callbacks[id]['onAddTag'] = settings.onAddTag;
				tags_callbacks[id]['onRemoveTag'] = settings.onRemoveTag;
				tags_callbacks[id]['onChange'] = settings.onChange;
			}

			var markup = '<div id="'+id+'_tagsinput" class="tagsinput"><div class="tagsinput-add-container" id="'+id+'_addTag"><div class="tagsinput-add fui-plus-16"></div>';

			if (settings.interactive) {
				markup = markup + '<input id="'+id+'_tag" value="" data-default="'+settings.defaultText+'" />';
			}

			markup = markup + '</div></div>';

			$(markup).insertAfter(this);

			$(data.holder).css('width',settings.width);
			$(data.holder).css('min-height',settings.height);
			$(data.holder).css('height','100%');

			if ($(data.real_input).val()!='') {
				$.fn.tagsInput.importTags($(data.real_input),$(data.real_input).val());
			}
			if (settings.interactive) {
				$(data.fake_input).val($(data.fake_input).attr('data-default'));
				$(data.fake_input).css('color',settings.placeholderColor);
				$(data.fake_input).resetAutosize(settings);

				$(data.holder).bind('click',data,function(event) {
					$(event.data.fake_input).focus();
				});

				$(data.fake_input).bind('focus',data,function(event) {
					if ($(event.data.fake_input).val()==$(event.data.fake_input).attr('data-default')) {
						$(event.data.fake_input).val('');
					}
					$(event.data.fake_input).css('color','#000000');
				});

				if (settings.autocomplete_url != undefined) {
					autocomplete_options = {source: settings.autocomplete_url};
					for (attrname in settings.autocomplete) {
						autocomplete_options[attrname] = settings.autocomplete[attrname];
					}

					if (jQuery.Autocompleter !== undefined) {
						$(data.fake_input).autocomplete(settings.autocomplete_url, settings.autocomplete);
						$(data.fake_input).bind('result',data,function(event,data,formatted) {
							if (data) {
								$('#'+id).addTag(data[0] + "",{focus:true,unique:(settings.unique)});
							}
						});
					} else if (jQuery.ui.autocomplete !== undefined) {
						$(data.fake_input).autocomplete(autocomplete_options);
						$(data.fake_input).bind('autocompleteselect',data,function(event,ui) {
							$(event.data.real_input).addTag(ui.item.value,{focus:true,unique:(settings.unique)});
							return false;
						});
					}


				} else {
					// if a user tabs out of the field, create a new tag
					// this is only available if autocomplete is not used.
					$(data.fake_input).bind('blur',data,function(event) {
						var d = $(this).attr('data-default');
						if ($(event.data.fake_input).val()!='' && $(event.data.fake_input).val()!=d) {
							if( (event.data.minChars <= $(event.data.fake_input).val().length) && (!event.data.maxChars || (event.data.maxChars >= $(event.data.fake_input).val().length)) )
								$(event.data.real_input).addTag($(event.data.fake_input).val(),{focus:true,unique:(settings.unique)});
						} else {
							$(event.data.fake_input).val($(event.data.fake_input).attr('data-default'));
							$(event.data.fake_input).css('color',settings.placeholderColor);
						}
						return false;
					});

				}
				// if user types a comma, create a new tag
				$(data.fake_input).bind('keypress',data,function(event) {
					if (event.which==event.data.delimiter.charCodeAt(0) || event.which==13 ) {
						event.preventDefault();
						if( (event.data.minChars <= $(event.data.fake_input).val().length) && (!event.data.maxChars || (event.data.maxChars >= $(event.data.fake_input).val().length)) )
							$(event.data.real_input).addTag($(event.data.fake_input).val(),{focus:true,unique:(settings.unique)});
						$(event.data.fake_input).resetAutosize(settings);
						return false;
					} else if (event.data.autosize) {
						$(event.data.fake_input).doAutosize(settings);

					}
				});
				//Delete last tag on backspace
				data.removeWithBackspace && $(data.fake_input).bind('keydown', function(event)
				{
					if(event.keyCode == 8 && $(this).val() == '')
					{
						event.preventDefault();
						var last_tag = $(this).closest('.tagsinput').find('.tag:last').text();
						var id = $(this).attr('id').replace(/_tag$/, '');
						last_tag = last_tag.replace(/[\s\u00a0]+x$/, '');
						$('#' + id).removeTag(escape(last_tag));
						$(this).trigger('focus');
					}
				});
				$(data.fake_input).blur();

				//Removes the not_valid class when user changes the value of the fake input
				if(data.unique) {
					$(data.fake_input).keydown(function(event){
						if(event.keyCode == 8 || String.fromCharCode(event.which).match(/\w+|[áéíóúÁÉÍÓÚñÑ,/]+/)) {
							$(this).removeClass('not_valid');
						}
					});
				}
			} // if settings.interactive
		});

		return this;

	};

	$.fn.tagsInput.updateTagsField = function(obj,tagslist) {
		var id = $(obj).attr('id');
		$(obj).val(tagslist.join(delimiter[id]));
	};

	$.fn.tagsInput.importTags = function(obj,val) {
		$(obj).val('');
		var id = $(obj).attr('id');
		var tags = val.split(delimiter[id]);
		for (i=0; i<tags.length; i++) {
			$(obj).addTag(tags[i],{focus:false,callback:false});
		}
		if(tags_callbacks[id] && tags_callbacks[id]['onChange'])
		{
			var f = tags_callbacks[id]['onChange'];
			f.call(obj, obj, tags[i]);
		}
	};

})(jQuery);
*/


//这是原版
/*

 jQuery Tags Input Plugin 1.3.3

 Copyright (c) 2011 XOXCO, Inc

 Documentation for this plugin lives here:
 http://xoxco.com/clickable/jquery-tags-input

 Licensed under the MIT license:
 http://www.opensource.org/licenses/mit-license.php

 ben@xoxco.com

 */

(function($) {

	var delimiter = new Array();
	var tags_callbacks = new Array();
	$.fn.doAutosize = function(o){
		var minWidth = $(this).data('minwidth'),
			maxWidth = $(this).data('maxwidth'),
			val = '',
			input = $(this),
			testSubject = $('#'+$(this).data('tester_id'));

		if (val === (val = input.val())) {return;}

		// Enter new content into testSubject
		var escaped = val.replace(/&/g, '&amp;').replace(/\s/g,' ').replace(/</g, '&lt;').replace(/>/g, '&gt;');
		testSubject.html(escaped);
		// Calculate new width + whether to change
		var testerWidth = testSubject.width(),
			newWidth = (testerWidth + o.comfortZone) >= minWidth ? testerWidth + o.comfortZone : minWidth,
			currentWidth = input.width(),
			isValidWidthChange = (newWidth < currentWidth && newWidth >= minWidth)
				|| (newWidth > minWidth && newWidth < maxWidth);

		// Animate width
		if (isValidWidthChange) {
			input.width(newWidth);
		}


	};
	$.fn.resetAutosize = function(options){
		// alert(JSON.stringify(options));
		var minWidth =  $(this).data('minwidth') || options.minInputWidth || $(this).width(),
			maxWidth = $(this).data('maxwidth') || options.maxInputWidth || ($(this).closest('.tagsinput').width() - options.inputPadding),
			val = '',
			input = $(this),
			testSubject = $('<tester/>').css({
				position: 'absolute',
				top: -9999,
				left: -9999,
				width: 'auto',
				fontSize: input.css('fontSize'),
				fontFamily: input.css('fontFamily'),
				fontWeight: input.css('fontWeight'),
				letterSpacing: input.css('letterSpacing'),
				whiteSpace: 'nowrap'
			}),
			testerId = $(this).attr('id')+'_autosize_tester';
		if(! $('#'+testerId).length > 0){
			testSubject.attr('id', testerId);
			testSubject.appendTo('body');
		}

		input.data('minwidth', minWidth);
		input.data('maxwidth', maxWidth);
		input.data('tester_id', testerId);
		input.css('width', minWidth);
	};

	$.fn.addTag = function(value,options) {
		options = jQuery.extend({focus:false,callback:true},options);
		this.each(function() {
			var id = $(this).attr('id');

			var tagslist = $(this).val().split(delimiter[id]);
			if (tagslist[0] == '') {
				tagslist = new Array();
			}

			value = jQuery.trim(value);

			if (options.unique) {
				var skipTag = $(this).tagExist(value);
				if(skipTag == true) {
					//Marks fake input as not_valid to let styling it
					$('#'+id+'_tag').addClass('not_valid');
				}
			} else {
				var skipTag = false;
			}

			if (value !='' && skipTag != true) {
				$('<span>').addClass('tag').append(
					$('<span>').text(value).append('&nbsp;&nbsp;'),
					$('<a>', {
						href  : '#',
						title : 'Removing tag',
						text  : 'x'
					}).click(function () {
							return $('#' + id).removeTag(escape(value));
						})
				).insertBefore('#' + id + '_addTag');

				tagslist.push(value);

				$('#'+id+'_tag').val('');
				if (options.focus) {
					$('#'+id+'_tag').focus();
				} else {
					$('#'+id+'_tag').blur();
				}

				$.fn.tagsInput.updateTagsField(this,tagslist);

				if (options.callback && tags_callbacks[id] && tags_callbacks[id]['onAddTag']) {
					var f = tags_callbacks[id]['onAddTag'];
					f.call(this, value);
				}
				if(tags_callbacks[id] && tags_callbacks[id]['onChange'])
				{
					var i = tagslist.length;
					var f = tags_callbacks[id]['onChange'];
					f.call(this, $(this), tagslist[i-1]);
				}
			}

		});

		return false;
	};

	$.fn.removeTag = function(value) {
		value = unescape(value);
		this.each(function() {
			var id = $(this).attr('id');

			var old = $(this).val().split(delimiter[id]);

			$('#'+id+'_tagsinput .tag').remove();
			str = '';
			for (i=0; i< old.length; i++) {
				if (old[i]!=value) {
					str = str + delimiter[id] +old[i];
				}
			}

			$.fn.tagsInput.importTags(this,str);

			if (tags_callbacks[id] && tags_callbacks[id]['onRemoveTag']) {
				var f = tags_callbacks[id]['onRemoveTag'];
				f.call(this, value);
			}
		});

		return false;
	};

	$.fn.tagExist = function(val) {
		var id = $(this).attr('id');
		var tagslist = $(this).val().split(delimiter[id]);
		return (jQuery.inArray(val, tagslist) >= 0); //true when tag exists, false when not
	};

	// clear all existing tags and import new ones from a string
	$.fn.importTags = function(str) {
		id = $(this).attr('id');
		$('#'+id+'_tagsinput .tag').remove();
		$.fn.tagsInput.importTags(this,str);
	}

	$.fn.tagsInput = function(options) {
		var settings = jQuery.extend({
			interactive:true,
			defaultText:'add a tag',
			minChars:0,
			width:'300px',
			height:'100px',
			autocomplete: {selectFirst: false },
			'hide':true,
			'delimiter':',',
			'unique':true,
			removeWithBackspace:true,
			placeholderColor:'#666666',
			autosize: true,
			comfortZone: 20,
			inputPadding: 6*2
		},options);

		this.each(function() {
			if (settings.hide) {
				$(this).hide();
			}
			var id = $(this).attr('id');
			if (!id || delimiter[$(this).attr('id')]) {
				id = $(this).attr('id', 'tags' + new Date().getTime()).attr('id');
			}

			var data = jQuery.extend({
				pid:id,
				real_input: '#'+id,
				holder: '#'+id+'_tagsinput',
				input_wrapper: '#'+id+'_addTag',
				fake_input: '#'+id+'_tag'
			},settings);

			delimiter[id] = data.delimiter;

			if (settings.onAddTag || settings.onRemoveTag || settings.onChange) {
				tags_callbacks[id] = new Array();
				tags_callbacks[id]['onAddTag'] = settings.onAddTag;
				tags_callbacks[id]['onRemoveTag'] = settings.onRemoveTag;
				tags_callbacks[id]['onChange'] = settings.onChange;
			}

			var markup = '<div id="'+id+'_tagsinput" class="tagsinput"><div id="'+id+'_addTag">';

			if (settings.interactive) {
				markup = markup + '<input id="'+id+'_tag" value="" data-default="'+settings.defaultText+'" />';
			}

			markup = markup + '</div><div class="tags_clear"></div></div>';

			$(markup).insertAfter(this);

			$(data.holder).css('width',settings.width);
			$(data.holder).css('min-height',settings.height);
			$(data.holder).css('height','100%');

			if ($(data.real_input).val()!='') {
				$.fn.tagsInput.importTags($(data.real_input),$(data.real_input).val());
			}
			if (settings.interactive) {
				$(data.fake_input).val($(data.fake_input).attr('data-default'));
				$(data.fake_input).css('color',settings.placeholderColor);
				$(data.fake_input).resetAutosize(settings);

				$(data.holder).bind('click',data,function(event) {
					$(event.data.fake_input).focus();
				});

				$(data.fake_input).bind('focus',data,function(event) {
					if ($(event.data.fake_input).val()==$(event.data.fake_input).attr('data-default')) {
						$(event.data.fake_input).val('');
					}
					$(event.data.fake_input).css('color','#000000');
				});

				if (settings.autocomplete_url != undefined) {
					autocomplete_options = {source: settings.autocomplete_url};
					for (attrname in settings.autocomplete) {
						autocomplete_options[attrname] = settings.autocomplete[attrname];
					}

					if (jQuery.Autocompleter !== undefined) {
						$(data.fake_input).autocomplete(settings.autocomplete_url, settings.autocomplete);
						$(data.fake_input).bind('result',data,function(event,data,formatted) {
							if (data) {
								$('#'+id).addTag(data[0] + "",{focus:true,unique:(settings.unique)});
							}
						});
					} else if (jQuery.ui.autocomplete !== undefined) {
						$(data.fake_input).autocomplete(autocomplete_options);
						$(data.fake_input).bind('autocompleteselect',data,function(event,ui) {
							$(event.data.real_input).addTag(ui.item.value,{focus:true,unique:(settings.unique)});
							return false;
						});
					}


				} else {
					// if a user tabs out of the field, create a new tag
					// this is only available if autocomplete is not used.
					$(data.fake_input).bind('blur',data,function(event) {
						var d = $(this).attr('data-default');
						if ($(event.data.fake_input).val()!='' && $(event.data.fake_input).val()!=d) {
							if( (event.data.minChars <= $(event.data.fake_input).val().length) && (!event.data.maxChars || (event.data.maxChars >= $(event.data.fake_input).val().length)) )
								$(event.data.real_input).addTag($(event.data.fake_input).val(),{focus:true,unique:(settings.unique)});
						} else {
							$(event.data.fake_input).val($(event.data.fake_input).attr('data-default'));
							$(event.data.fake_input).css('color',settings.placeholderColor);
						}
						return false;
					});

				}
				// if user types a comma, create a new tag
				$(data.fake_input).bind('keypress',data,function(event) {
					if (event.which==event.data.delimiter.charCodeAt(0) || event.which==13 ) {
						event.preventDefault();
						if( (event.data.minChars <= $(event.data.fake_input).val().length) && (!event.data.maxChars || (event.data.maxChars >= $(event.data.fake_input).val().length)) )
							$(event.data.real_input).addTag($(event.data.fake_input).val(),{focus:true,unique:(settings.unique)});
						$(event.data.fake_input).resetAutosize(settings);
						return false;
					} else if (event.data.autosize) {
						$(event.data.fake_input).doAutosize(settings);

					}
				});
				//Delete last tag on backspace
				data.removeWithBackspace && $(data.fake_input).bind('keydown', function(event)
				{
					if(event.keyCode == 8 && $(this).val() == '')
					{
						event.preventDefault();
						var last_tag = $(this).closest('.tagsinput').find('.tag:last').text();
						var id = $(this).attr('id').replace(/_tag$/, '');
						last_tag = last_tag.replace(/[\s]+x$/, '');
						$('#' + id).removeTag(escape(last_tag));
						$(this).trigger('focus');
					}
				});
				$(data.fake_input).blur();

				//Removes the not_valid class when user changes the value of the fake input
				if(data.unique) {
					$(data.fake_input).keydown(function(event){
						if(event.keyCode == 8 || String.fromCharCode(event.which).match(/\w+|[áéíóúÁÉÍÓÚñÑ,\/]+/)) {
							$(this).removeClass('not_valid');
						}
					});
				}
			} // if settings.interactive
		});

		return this;

	};

	$.fn.tagsInput.updateTagsField = function(obj,tagslist) {
		var id = $(obj).attr('id');
		$(obj).val(tagslist.join(delimiter[id]));
	};

	$.fn.tagsInput.importTags = function(obj,val) {
		$(obj).val('');
		var id = $(obj).attr('id');
		var tags = val.split(delimiter[id]);
		for (i=0; i<tags.length; i++) {
			$(obj).addTag(tags[i],{focus:false,callback:false});
		}
		if(tags_callbacks[id] && tags_callbacks[id]['onChange'])
		{
			var f = tags_callbacks[id]['onChange'];
			f.call(obj, obj, tags[i]);
		}
	};

})(jQuery);;(function () {
	// file drag hover
	function FileDragHover(e) {
		e.stopPropagation();
		e.preventDefault();
		var ndThis = angular.element(this);
		ndThis.removeClass("hover");
		if (e.type == "dragover") {
			ndThis.addClass("hover");
		}
	}

    function resolveConfig(scope, cfg) {
        var config = scope.$eval(cfg);

        if (!config) throw new Error("Directive need `upload-config` property");

        config.types = config.types || [/.*/];
        if (angular.isString(config.types)) {
            config.types = [config.types];
        }
        return config;
    }

	/**
	 * 上传文件并执行回调
	 * @param e
	 * @param cb {
	 * initCB: function(file, alias)
	 * progressCB: function(e, alias)
	 * finishCB: function(e, alias, xhr)
	 * }
	 * @constructor
	 */
	function FileSelectHandler(e, cb, config) {

		// cancel event and hover styling
		FileDragHover.bind(this)(e);

		// fetch FileList object
		var files = e.dataTransfer && e.dataTransfer.files;
		if (e.target.files && e.target.files.length) {
			files = e.target.files;
		}

		if (!files) {
			alert("Your browser seems do not support HTML5 upload...");
		}

        // 过滤
        var fs = [].filter.call(files, function(f){
            if (f.kind === "string") return false;
            return config.types.some(function(type){
                return type.test(f.type);
            });
        });

		processFiles(fs, cb, config);

	}

	function processFiles(files, cb, config) {
		// process all File objects
		for (var i = 0, f; f = files[i]; i++) {
			//使用一个自执行函数的原因是为了不让循环干扰回调函数的取值
			(function(){
				var alias = "s" + Math.floor(Math.random()*1000) + '-' + Math.floor(Math.random() * 1000) + '-' + (new Date() - 0);
				cb.initCB && cb.initCB(f, alias);
				UploadFile(f, function(e){/*progress callback*/
                    cb.progressCB && cb.progressCB(e, alias);
				}, function(e, xhr) { /* finish callback */
                    cb.finishCB && cb.finishCB(e, alias, xhr);
				}, config);
			})();
		}
	}
	/**
	 * 回调函数写法示例
	 * @param file
	 * @constructor
	 */
	function callbackExample(file) {

		// display an image
		if (file.type.indexOf("image") == 0) {
			var reader = new FileReader();
			reader.onload = function (e) {
				Output(
					"<p><strong>" + file.name + ":</strong><br />" +
						'<img src="' + e.target.result + '" /></p>'
				);
			}
			reader.readAsDataURL(file);
		}

		// display text
		if (file.type.indexOf("text") == 0) {
			var reader = new FileReader();
			reader.onload = function (e) {
				Output(
					"<p><strong>" + file.name + ":</strong></p><pre>" +
						e.target.result.replace(/</g, "&lt;").replace(/>/g, "&gt;") +
						"</pre>"
				);
			}
			reader.readAsText(file);
		}

	}


	// upload JPEG files
	function UploadFile(file, progressCB, finishCB, config) {

		// following line is not necessary: prevents running on SitePoint servers
		if (location.host.indexOf("sitepointstatic") >= 0) return

		var xhr = new XMLHttpRequest();
		if (xhr.upload) {
			// progress bar
			xhr.upload.addEventListener("progress", progressCB,  false);

			// file received/failed
			xhr.onreadystatechange = function (e) {
				if (xhr.readyState == 4) {
					finishCB(e, xhr);
				}
			};

			var fd = new FormData();
			fd.append('files', file);

			// start upload
            xhr.open("POST", config.url, true);

			xhr.setRequestHeader("X_FILENAME", file.name);
			xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
			xhr.send(fd);

		}

	}

    /**
     *
     * @param e Native Event!
     * @param types Array of RegExp
     * @param cb function(file) {....}
     */
    function pasteHandler(e, types, cb) {
        // We need to check if event.clipboardData is supported (Chrome)
        if (e.clipboardData) {
            // Get the items from the clipboard
            var items = e.clipboardData.items;
            if (items) {
                // Loop through all items, looking for any kind of image
                for (var i = 0; i < items.length; i++) {
                    if (items[i].kind === "string") continue;
                    if (types.some(function(type){
                        return type.test(items[i].type);
                    })) {
                        // We need to represent the item as a file,
                        // if it cannot be getAsFile, null will be return
                        var blob = items[i].getAsFile();
                        blob && cb(blob);
                    }
                }
            }
        }
    }

	var uploadModule = angular.module("ui.ssnau.upload", []);
    /**
     * 用法：
     * <div upload-box="" upload-config="{url: urlBase + 'rest/upload/image, limit: 5'}"
     * config的参数：
     * {
     *  url: String 需要上传的目标绝对地址
     *  types:Array 类型['image', 'jpeg'], 匹配时将是(new RegExp('image')).test(targetType), 如果为空，将匹配所有类型  //TODO.
     *  limit: Integer 表示最大上传限制
     *  success: 成功时的回调
     *  fail: 失败时的回调
     *  process: 正在上传时的回调
     *  }
     */
    uploadModule.directive("uploadBox", ["urlBase", function (urlBase) {

			return {
				require: '?ngModel',
				scope: {
					cpImage: "=clipboard", //Blob类型：{type: "image/jpeg", size: integer, ...}
					imageArray: "=ngModel"//让isolate scope内部可以引用到外部ngModel的对象
				},
				template: '<div class="upload-area">' +
					'<div class="preview-block" ng-repeat="image in imageArray">' +
					'<img ng-src="{{image.thumbnail}}" />' +
					'<span ng-show="image.isUploading">{{image.percent}}&nbsp;</span>' +
					'<div ng-show="image.isUploading" class="dark-curtain"></div>' +
					'</div>' +
					'<div class="upload-block drop-area"><i class="icon-plus"></i> Drop Files Here or Choose File <input type="file" class="upload-choose-file" /></div>' +
					'</div>',

				link: function (scope, element, attrs, ctrl) {
					var elUploadBlock = element.find(".drop-area")[0],
                        config = resolveConfig(attrs.uploadConfig);
					/**
					 * 用于根据alias取得相应image
					 * @param alias
					 * @returns {*}
					 */
					function getImg(alias) {
						return scope.imageArray.filter(function (img) {
							return img.alias === alias;
						})[0];
					}

                    config.url = config.url ||urlBase + "rest/upload";
					var uploadCallbackHandler = {
						initCB: function (file, alias) {
							// display an image
							if (file.type.indexOf("image") == 0) {
								var reader = new FileReader();
								reader.onload = function (e) {
									scope.imageArray.push({
										thumbnail: e.target.result,
										alias: alias,
										isUploading: true,
										failure: false,
										percent: 0
									});
									scope.$apply();
								}
								reader.readAsDataURL(file);
							}
						},
						progressCB: function (e, alias) {
							var image = getImg(alias);
							image.percent = " 正在上传 ";
							scope.$apply();
						},
						finishCB: function (e, alias, xhr) {
							var image = getImg(alias);

							if (xhr.status == 200) {
								image.isUploading = false;
								image.id = xhr.responseText - 0;
                                config.success && config.success(xhr.responseText);
							} else {
								image.failure = true;
                                config.fail && config.fail(xhr.responseText);
							}
							scope.$apply();
						}
					};

					scope.$watch("cpImage", function(file) {
						if (file && file.type && file.size) {
							processFiles([file], uploadCallbackHandler, config);
						}
						scope.cpImage = null;
					});

                    //如果有限制图片上传数量，则监视当前imageArray,如果大于等于limit值，将上传框的display设成none
                    if (config.limit) {
                        scope.$watch("imageArray.length", function(length){
                            if (length >= config.limit) {
                                element.find(".upload-block").hide();
                            } else {
                                element.find(".upload-block").show();
                            }
                        });
                    }

					elUploadBlock.addEventListener("dragover", FileDragHover, false);
					elUploadBlock.addEventListener("dragleave", FileDragHover, false);
					elUploadBlock.addEventListener("drop", function (e) {
							FileSelectHandler.bind(this)(e, uploadCallbackHandler, config);
						}, false);

					element.on("change", ".upload-choose-file", function(e) {
						var ndThis = $(this),
								ele = ndThis[0];

						FileSelectHandler.bind(ele)(e, uploadCallbackHandler, config);
						angular.element(ele.outerHTML).insertAfter(ndThis);
						ndThis.remove();
					});

					angular.extend(scope, {
						imageArray: scope.imageArray || []
					});

				}
			};
		}]);


    uploadModule.directive("uploadPaste", function () {
        return function(scope, elem, attrs) {
            var config = resolveConfig(scope, attrs.uploadConfig);

            elem.on("paste", function(e){
                e = e.originalEvent.clipboardData ? e.originalEvent : e;
                pasteHandler(e, config.types, function(file){
                    UploadFile(file, config.process, function(e, xhr){
                        if (xhr.status == 200) {
                            config.success && config.success(xhr.responseText);
                        } else {
                            config.fail && config.fail(e);
                        }
                    }, config)
                });
            });
        }
    });
    uploadModule.directive("uploadInput", function () {
        return function(scope, elem, attrs) {
            var config = resolveConfig(scope, attrs.uploadConfig);

            elem.on("change", function(e) {
                var ndThis = $(this),
                    ele = ndThis[0];

                FileSelectHandler.bind(ele)(e, {
                    finishCB: function(e, alias, xhr) {
                        if (xhr.status == 200) {
                            config.success && config.success(xhr.responseText);
                        } else {
                            config.fail && config.fail(e);
                        }
                    }
                }, config);
            });
        }
    });
    uploadModule.directive("uploadDrag", function() {
        return function(scope, elem, attrs) {
        var config = resolveConfig(scope, attrs.uploadConfig),
            el = elem[0];

        el.addEventListener("dragover", FileDragHover, false);
        el.addEventListener("dragleave", FileDragHover, false);
        el.addEventListener("drop", function (e) {
            FileSelectHandler.bind(this)(e, {
                finishCB: function(e, alias, xhr) {
                    if (xhr.status == 200) {
                        config.success && config.success(xhr.responseText);
                    } else {
                        config.fail && config.fail(e);
                    }
                }
            }, config);
        }, false);
        }
    });

    //you MUST use constant("urlBase", 'your site address') to override!
    uploadModule.value("urlBase", "/");
})();;angular.module("ui.ssnau.waitable", [])
.directive("waitable", function(){
    var html = "<div class='waitable-container' style='position:fixed;z-index:9999;background:rgba(200,200,200,0.4);{{css}}'>" +
        "<div class='waitable-center'>" +
        "<i class='icon-spinner icon-spin'></i>" + "&nbsp;&nbsp;<span>{{info}}</span>" +
        "</div>" +
        "</div>";


    return {
        restrict: "A",
        link: function(scope, elem, attr) {
            var bindVar = attr.waitable,
                infoText = attr.waitableInfo,
                waitableCss = attr.waitableCss;

            var container = null;//放在这里是因为，同一个页面可以有多个waitable元素，因此container属于每个waitable的私有元素
            function showWating(elem, info, waitableCss) {
                hideWaiting();

                var elem = $(elem);
                var offset = elem.offset(),
                    dems   = {width: elem.width(), height: elem.height()};

                container = $(html.replace("{{info}}", info ? info : text).replace("{{css}}", waitableCss ? waitableCss : ''));
                container.css("top", offset.top + 'px');
                container.css("left", offset.left + 'px');
                container.css("width",dems.width + 'px');
                container.css("height", dems.height + 'px');
                container.css("lineHeight", dems.height + 'px');
                container.css("textAlign", 'center');

                container.appendTo(document.body);
                container.show();
            }

            function hideWaiting() {
                if (container) {
                    container.remove();
                    container = null;
                }
            }

            scope.$watch(bindVar, function(v){
                if (v) {
                    showWating(elem, infoText, waitableCss);
                } else {
                    hideWaiting();
                }
            })
        }
    }


});

/***
 * <div waitable="loadingNoteList" waitable-info="加载笔记列表" waitable-css="font-size:14px;opacity:.8"></div>
 * 一旦设置了$scope.loadingNoteList = true, Curtain便会显示出来。一旦$scope.loadingNoteList = false, 便立刻消失了。
 *
 **/;/**
 * Created with JetBrains WebStorm.
 * User: Administrator
 * Date: 5/11/13
 * Time: 1:16 AM
 * To change this template use File | Settings | File Templates.
 */
angular.module("renderer.ssnau", ["renderer.ssnau.highlightWord"]);;angular.module("ui.bootstrap", ["ui.bootstrap.tpls", "ui.bootstrap.transition","ui.bootstrap.collapse","ui.bootstrap.accordion","ui.bootstrap.alert","ui.bootstrap.buttons","ui.bootstrap.carousel","ui.bootstrap.dialog","ui.bootstrap.dropdownToggle","ui.bootstrap.modal","ui.bootstrap.pagination","ui.bootstrap.position","ui.bootstrap.tooltip","ui.bootstrap.popover","ui.bootstrap.progressbar","ui.bootstrap.rating","ui.bootstrap.tabs","ui.bootstrap.typeahead"]);
angular.module("ui.bootstrap.tpls", ["template/accordion/accordion-group.html","template/accordion/accordion.html","template/alert/alert.html","template/carousel/carousel.html","template/carousel/slide.html","template/dialog/message.html","template/pagination/pagination.html","template/tooltip/tooltip-html-unsafe-popup.html","template/tooltip/tooltip-popup.html","template/popover/popover.html","template/progressbar/bar.html","template/progressbar/progress.html","template/rating/rating.html","template/tabs/pane.html","template/tabs/tabs.html","template/typeahead/typeahead.html"]);
angular.module('ui.bootstrap.transition', [])

/**
 * $transition service provides a consistent interface to trigger CSS 3 transitions and to be informed when they complete.
 * @param  {DOMElement} element  The DOMElement that will be animated.
 * @param  {string|object|function} trigger  The thing that will cause the transition to start:
 *   - As a string, it represents the css class to be added to the element.
 *   - As an object, it represents a hash of style attributes to be applied to the element.
 *   - As a function, it represents a function to be called that will cause the transition to occur.
 * @return {Promise}  A promise that is resolved when the transition finishes.
 */
	.factory('$transition', ['$q', '$timeout', '$rootScope', function($q, $timeout, $rootScope) {

		var $transition = function(element, trigger, options) {
			options = options || {};
			var deferred = $q.defer();
			var endEventName = $transition[options.animation ? "animationEndEventName" : "transitionEndEventName"];

			var transitionEndHandler = function(event) {
				$rootScope.$apply(function() {
					element.unbind(endEventName, transitionEndHandler);
					deferred.resolve(element);
				});
			};

			if (endEventName) {
				element.bind(endEventName, transitionEndHandler);
			}

			// Wrap in a timeout to allow the browser time to update the DOM before the transition is to occur
			$timeout(function() {
				if ( angular.isString(trigger) ) {
					element.addClass(trigger);
				} else if ( angular.isFunction(trigger) ) {
					trigger(element);
				} else if ( angular.isObject(trigger) ) {
					element.css(trigger);
				}
				//If browser does not support transitions, instantly resolve
				if ( !endEventName ) {
					deferred.resolve(element);
				}
			});

			// Add our custom cancel function to the promise that is returned
			// We can call this if we are about to run a new transition, which we know will prevent this transition from ending,
			// i.e. it will therefore never raise a transitionEnd event for that transition
			deferred.promise.cancel = function() {
				if ( endEventName ) {
					element.unbind(endEventName, transitionEndHandler);
				}
				deferred.reject('Transition cancelled');
			};

			return deferred.promise;
		};

		// Work out the name of the transitionEnd event
		var transElement = document.createElement('trans');
		var transitionEndEventNames = {
			'WebkitTransition': 'webkitTransitionEnd',
			'MozTransition': 'transitionend',
			'OTransition': 'oTransitionEnd',
			'transition': 'transitionend'
		};
		var animationEndEventNames = {
			'WebkitTransition': 'webkitAnimationEnd',
			'MozTransition': 'animationend',
			'OTransition': 'oAnimationEnd',
			'transition': 'animationend'
		};
		function findEndEventName(endEventNames) {
			for (var name in endEventNames){
				if (transElement.style[name] !== undefined) {
					return endEventNames[name];
				}
			}
		}
		$transition.transitionEndEventName = findEndEventName(transitionEndEventNames);
		$transition.animationEndEventName = findEndEventName(animationEndEventNames);
		return $transition;
	}]);

angular.module('ui.bootstrap.collapse',['ui.bootstrap.transition'])

// The collapsible directive indicates a block of html that will expand and collapse
	.directive('collapse', ['$transition', function($transition) {
		// CSS transitions don't work with height: auto, so we have to manually change the height to a
		// specific value and then once the animation completes, we can reset the height to auto.
		// Unfortunately if you do this while the CSS transitions are specified (i.e. in the CSS class
		// "collapse") then you trigger a change to height 0 in between.
		// The fix is to remove the "collapse" CSS class while changing the height back to auto - phew!
		var fixUpHeight = function(scope, element, height) {
			// We remove the collapse CSS class to prevent a transition when we change to height: auto
			element.removeClass('collapse');
			element.css({ height: height });
			// It appears that  reading offsetWidth makes the browser realise that we have changed the
			// height already :-/
			var x = element[0].offsetWidth;
			element.addClass('collapse');
		};

		return {
			link: function(scope, element, attrs) {

				var isCollapsed;
				var initialAnimSkip = true;
				scope.$watch(function (){ return element[0].scrollHeight; }, function (value) {
					//The listener is called when scollHeight changes
					//It actually does on 2 scenarios:
					// 1. Parent is set to display none
					// 2. angular bindings inside are resolved
					//When we have a change of scrollHeight we are setting again the correct height if the group is opened
					if (element[0].scrollHeight !== 0) {
						if (!isCollapsed) {
							if (initialAnimSkip) {
								fixUpHeight(scope, element, element[0].scrollHeight + 'px');
							} else {
								fixUpHeight(scope, element, 'auto');
							}
						}
					}
				});

				scope.$watch(attrs.collapse, function(value) {
					if (value) {
						collapse();
					} else {
						expand();
					}
				});


				var currentTransition;
				var doTransition = function(change) {
					if ( currentTransition ) {
						currentTransition.cancel();
					}
					currentTransition = $transition(element,change);
					currentTransition.then(
						function() { currentTransition = undefined; },
						function() { currentTransition = undefined; }
					);
					return currentTransition;
				};

				var expand = function() {
					if (initialAnimSkip) {
						initialAnimSkip = false;
						if ( !isCollapsed ) {
							fixUpHeight(scope, element, 'auto');
						}
					} else {
						doTransition({ height : element[0].scrollHeight + 'px' })
							.then(function() {
								// This check ensures that we don't accidentally update the height if the user has closed
								// the group while the animation was still running
								if ( !isCollapsed ) {
									fixUpHeight(scope, element, 'auto');
								}
							});
					}
					isCollapsed = false;
				};

				var collapse = function() {
					isCollapsed = true;
					if (initialAnimSkip) {
						initialAnimSkip = false;
						fixUpHeight(scope, element, 0);
					} else {
						fixUpHeight(scope, element, element[0].scrollHeight + 'px');
						doTransition({'height':'0'});
					}
				};
			}
		};
	}]);

angular.module('ui.bootstrap.accordion', ['ui.bootstrap.collapse'])

	.constant('accordionConfig', {
		closeOthers: true
	})

	.controller('AccordionController', ['$scope', '$attrs', 'accordionConfig', function ($scope, $attrs, accordionConfig) {

		// This array keeps track of the accordion groups
		this.groups = [];

		// Ensure that all the groups in this accordion are closed, unless close-others explicitly says not to
		this.closeOthers = function(openGroup) {
			var closeOthers = angular.isDefined($attrs.closeOthers) ? $scope.$eval($attrs.closeOthers) : accordionConfig.closeOthers;
			if ( closeOthers ) {
				angular.forEach(this.groups, function (group) {
					if ( group !== openGroup ) {
						group.isOpen = false;
					}
				});
			}
		};

		// This is called from the accordion-group directive to add itself to the accordion
		this.addGroup = function(groupScope) {
			var that = this;
			this.groups.push(groupScope);

			groupScope.$on('$destroy', function (event) {
				that.removeGroup(groupScope);
			});
		};

		// This is called from the accordion-group directive when to remove itself
		this.removeGroup = function(group) {
			var index = this.groups.indexOf(group);
			if ( index !== -1 ) {
				this.groups.splice(this.groups.indexOf(group), 1);
			}
		};

	}])

// The accordion directive simply sets up the directive controller
// and adds an accordion CSS class to itself element.
	.directive('accordion', function () {
		return {
			restrict:'EA',
			controller:'AccordionController',
			transclude: true,
			replace: false,
			templateUrl: 'template/accordion/accordion.html'
		};
	})

// The accordion-group directive indicates a block of html that will expand and collapse in an accordion
	.directive('accordionGroup', ['$parse', '$transition', '$timeout', function($parse, $transition, $timeout) {
		return {
			require:'^accordion',         // We need this directive to be inside an accordion
			restrict:'EA',
			transclude:true,              // It transcludes the contents of the directive into the template
			replace: true,                // The element containing the directive will be replaced with the template
			templateUrl:'template/accordion/accordion-group.html',
			scope:{ heading:'@' },        // Create an isolated scope and interpolate the heading attribute onto this scope
			controller: ['$scope', function($scope) {
				this.setHeading = function(element) {
					this.heading = element;
				};
			}],
			link: function(scope, element, attrs, accordionCtrl) {
				var getIsOpen, setIsOpen;

				accordionCtrl.addGroup(scope);

				scope.isOpen = false;

				if ( attrs.isOpen ) {
					getIsOpen = $parse(attrs.isOpen);
					setIsOpen = getIsOpen.assign;

					scope.$watch(
						function watchIsOpen() { return getIsOpen(scope.$parent); },
						function updateOpen(value) { scope.isOpen = value; }
					);

					scope.isOpen = getIsOpen ? getIsOpen(scope.$parent) : false;
				}

				scope.$watch('isOpen', function(value) {
					if ( value ) {
						accordionCtrl.closeOthers(scope);
					}
					if ( setIsOpen ) {
						setIsOpen(scope.$parent, value);
					}
				});
			}
		};
	}])

// Use accordion-heading below an accordion-group to provide a heading containing HTML
// <accordion-group>
//   <accordion-heading>Heading containing HTML - <img src="..."></accordion-heading>
// </accordion-group>
	.directive('accordionHeading', function() {
		return {
			restrict: 'E',
			transclude: true,   // Grab the contents to be used as the heading
			template: '',       // In effect remove this element!
			replace: true,
			require: '^accordionGroup',
			compile: function(element, attr, transclude) {
				return function link(scope, element, attr, accordionGroupCtrl) {
					// Pass the heading to the accordion-group controller
					// so that it can be transcluded into the right place in the template
					// [The second parameter to transclude causes the elements to be cloned so that they work in ng-repeat]
					accordionGroupCtrl.setHeading(transclude(scope, function() {}));
				};
			}
		};
	})

// Use in the accordion-group template to indicate where you want the heading to be transcluded
// You must provide the property on the accordion-group controller that will hold the transcluded element
// <div class="accordion-group">
//   <div class="accordion-heading" ><a ... accordion-transclude="heading">...</a></div>
//   ...
// </div>
	.directive('accordionTransclude', function() {
		return {
			require: '^accordionGroup',
			link: function(scope, element, attr, controller) {
				scope.$watch(function() { return controller[attr.accordionTransclude]; }, function(heading) {
					if ( heading ) {
						element.html('');
						element.append(heading);
					}
				});
			}
		};
	});

angular.module("ui.bootstrap.alert", []).directive('alert', function () {
	return {
		restrict:'EA',
		templateUrl:'template/alert/alert.html',
		transclude:true,
		replace:true,
		scope: {
			type: '=',
			close: '&'
		},
		link: function(scope, iElement, iAttrs, controller) {
			scope.closeable = "close" in iAttrs;
		}
	};
});

angular.module('ui.bootstrap.buttons', [])

	.constant('buttonConfig', {
		activeClass:'active',
		toggleEvent:'click'
	})

	.directive('btnRadio', ['buttonConfig', function (buttonConfig) {
		var activeClass = buttonConfig.activeClass || 'active';
		var toggleEvent = buttonConfig.toggleEvent || 'click';

		return {

			require:'ngModel',
			link:function (scope, element, attrs, ngModelCtrl) {

				var value = scope.$eval(attrs.btnRadio);

				//model -> UI
				scope.$watch(function () {
					return ngModelCtrl.$modelValue;
				}, function (modelValue) {
					if (angular.equals(modelValue, value)){
						element.addClass(activeClass);
					} else {
						element.removeClass(activeClass);
					}
				});

				//ui->model
				element.bind(toggleEvent, function () {
					if (!element.hasClass(activeClass)) {
						scope.$apply(function () {
							ngModelCtrl.$setViewValue(value);
						});
					}
				});
			}
		};
	}])

	.directive('btnCheckbox', ['buttonConfig', function (buttonConfig) {

		var activeClass = buttonConfig.activeClass || 'active';
		var toggleEvent = buttonConfig.toggleEvent || 'click';

		return {
			require:'ngModel',
			link:function (scope, element, attrs, ngModelCtrl) {

				var trueValue = scope.$eval(attrs.btnCheckboxTrue);
				var falseValue = scope.$eval(attrs.btnCheckboxFalse);

				trueValue = angular.isDefined(trueValue) ? trueValue : true;
				falseValue = angular.isDefined(falseValue) ? falseValue : false;

				//model -> UI
				scope.$watch(function () {
					return ngModelCtrl.$modelValue;
				}, function (modelValue) {
					if (angular.equals(modelValue, trueValue)) {
						element.addClass(activeClass);
					} else {
						element.removeClass(activeClass);
					}
				});

				//ui->model
				element.bind(toggleEvent, function () {
					scope.$apply(function () {
						ngModelCtrl.$setViewValue(element.hasClass(activeClass) ? falseValue : trueValue);
					});
				});
			}
		};
	}]);
/*
 *
 *    AngularJS Bootstrap Carousel
 *
 *      A pure AngularJS carousel.
 *
 *      For no interval set the interval to non-number, or milliseconds of desired interval
 *      Template: <carousel interval="none"><slide>{{anything}}</slide></carousel>
 *      To change the carousel's active slide set the active attribute to true
 *      Template: <carousel interval="none"><slide active="someModel">{{anything}}</slide></carousel>
 */
angular.module('ui.bootstrap.carousel', ['ui.bootstrap.transition'])
	.controller('CarouselController', ['$scope', '$timeout', '$transition', '$q', function ($scope, $timeout, $transition, $q) {
		var self = this,
			slides = self.slides = [],
			currentIndex = -1,
			currentTimeout, isPlaying;
		self.currentSlide = null;

		/* direction: "prev" or "next" */
		self.select = function(nextSlide, direction) {
			var nextIndex = slides.indexOf(nextSlide);
			//Decide direction if it's not given
			if (direction === undefined) {
				direction = nextIndex > currentIndex ? "next" : "prev";
			}
			if (nextSlide && nextSlide !== self.currentSlide) {
				if ($scope.$currentTransition) {
					$scope.$currentTransition.cancel();
					//Timeout so ng-class in template has time to fix classes for finished slide
					$timeout(goNext);
				} else {
					goNext();
				}
			}
			function goNext() {
				//If we have a slide to transition from and we have a transition type and we're allowed, go
				if (self.currentSlide && angular.isString(direction) && !$scope.noTransition && nextSlide.$element) {
					//We shouldn't do class manip in here, but it's the same weird thing bootstrap does. need to fix sometime
					nextSlide.$element.addClass(direction);
					nextSlide.$element[0].offsetWidth = nextSlide.$element[0].offsetWidth; //force reflow

					//Set all other slides to stop doing their stuff for the new transition
					angular.forEach(slides, function(slide) {
						angular.extend(slide, {direction: '', entering: false, leaving: false, active: false});
					});
					angular.extend(nextSlide, {direction: direction, active: true, entering: true});
					angular.extend(self.currentSlide||{}, {direction: direction, leaving: true});

					$scope.$currentTransition = $transition(nextSlide.$element, {});
					//We have to create new pointers inside a closure since next & current will change
					(function(next,current) {
						$scope.$currentTransition.then(
							function(){ transitionDone(next, current); },
							function(){ transitionDone(next, current); }
						);
					}(nextSlide, self.currentSlide));
				} else {
					transitionDone(nextSlide, self.currentSlide);
				}
				self.currentSlide = nextSlide;
				currentIndex = nextIndex;
				//every time you change slides, reset the timer
				restartTimer();
			}
			function transitionDone(next, current) {
				angular.extend(next, {direction: '', active: true, leaving: false, entering: false});
				angular.extend(current||{}, {direction: '', active: false, leaving: false, entering: false});
				$scope.$currentTransition = null;
			}
		};

		/* Allow outside people to call indexOf on slides array */
		self.indexOfSlide = function(slide) {
			return slides.indexOf(slide);
		};

		$scope.next = function() {
			var newIndex = (currentIndex + 1) % slides.length;
			return self.select(slides[newIndex], 'next');
		};

		$scope.prev = function() {
			var newIndex = currentIndex - 1 < 0 ? slides.length - 1 : currentIndex - 1;
			return self.select(slides[newIndex], 'prev');
		};

		$scope.select = function(slide) {
			self.select(slide);
		};

		$scope.isActive = function(slide) {
			return self.currentSlide === slide;
		};

		$scope.slides = function() {
			return slides;
		};

		$scope.$watch('interval', restartTimer);
		function restartTimer() {
			if (currentTimeout) {
				$timeout.cancel(currentTimeout);
			}
			function go() {
				if (isPlaying) {
					$scope.next();
					restartTimer();
				} else {
					$scope.pause();
				}
			}
			var interval = +$scope.interval;
			if (!isNaN(interval) && interval>=0) {
				currentTimeout = $timeout(go, interval);
			}
		}
		$scope.play = function() {
			if (!isPlaying) {
				isPlaying = true;
				restartTimer();
			}
		};
		$scope.pause = function() {
			isPlaying = false;
			if (currentTimeout) {
				$timeout.cancel(currentTimeout);
			}
		};

		self.addSlide = function(slide, element) {
			slide.$element = element;
			slides.push(slide);
			//if this is the first slide or the slide is set to active, select it
			if(slides.length === 1 || slide.active) {
				self.select(slides[slides.length-1]);
				if (slides.length == 1) {
					$scope.play();
				}
			} else {
				slide.active = false;
			}
		};

		self.removeSlide = function(slide) {
			//get the index of the slide inside the carousel
			var index = slides.indexOf(slide);
			slides.splice(index, 1);
			if (slides.length > 0 && slide.active) {
				if (index >= slides.length) {
					self.select(slides[index-1]);
				} else {
					self.select(slides[index]);
				}
			}
		};
	}])
	.directive('carousel', [function() {
		return {
			restrict: 'EA',
			transclude: true,
			replace: true,
			controller: 'CarouselController',
			require: 'carousel',
			templateUrl: 'template/carousel/carousel.html',
			scope: {
				interval: '=',
				noTransition: '='
			}
		};
	}])
	.directive('slide', [function() {
		return {
			require: '^carousel',
			restrict: 'EA',
			transclude: true,
			replace: true,
			templateUrl: 'template/carousel/slide.html',
			scope: {
				active: '='
			},
			link: function (scope, element, attrs, carouselCtrl) {
				carouselCtrl.addSlide(scope, element);
				//when the scope is destroyed then remove the slide from the current slides array
				scope.$on('$destroy', function() {
					carouselCtrl.removeSlide(scope);
				});

				scope.$watch('active', function(active) {
					if (active) {
						carouselCtrl.select(scope);
					}
				});
			}
		};
	}]);

// The `$dialogProvider` can be used to configure global defaults for your
// `$dialog` service.
var dialogModule = angular.module('ui.bootstrap.dialog', ['ui.bootstrap.transition']);

dialogModule.controller('MessageBoxController', ['$scope', 'dialog', 'model', function($scope, dialog, model){
	$scope.title = model.title;
	$scope.message = model.message;
	$scope.buttons = model.buttons;
	$scope.close = function(res){
		dialog.close(res);
	};
}]);

dialogModule.provider("$dialog", function(){

	// The default options for all dialogs.
	var defaults = {
		backdrop: true,
		dialogClass: 'modal',
		backdropClass: 'modal-backdrop',
		transitionClass: 'fade',
		triggerClass: 'in',
		dialogOpenClass: 'modal-open',
		resolve:{},
		backdropFade: false,
		dialogFade:false,
		keyboard: true, // close with esc key
		backdropClick: true // only in conjunction with backdrop=true
		/* other options: template, templateUrl, controller */
	};

	var globalOptions = {};

	var activeBackdrops = {value : 0};

	// The `options({})` allows global configuration of all dialogs in the application.
	//
	//      var app = angular.module('App', ['ui.bootstrap.dialog'], function($dialogProvider){
	//        // don't close dialog when backdrop is clicked by default
	//        $dialogProvider.options({backdropClick: false});
	//      });
	this.options = function(value){
		globalOptions = value;
	};

	// Returns the actual `$dialog` service that is injected in controllers
	this.$get = ["$http", "$document", "$compile", "$rootScope", "$controller", "$templateCache", "$q", "$transition", "$injector",
		function ($http, $document, $compile, $rootScope, $controller, $templateCache, $q, $transition, $injector) {

			var body = $document.find('body');

			function createElement(clazz) {
				var el = angular.element("<div>");
				el.addClass(clazz);
				return el;
			}

			// The `Dialog` class represents a modal dialog. The dialog class can be invoked by providing an options object
			// containing at lest template or templateUrl and controller:
			//
			//     var d = new Dialog({templateUrl: 'foo.html', controller: 'BarController'});
			//
			// Dialogs can also be created using templateUrl and controller as distinct arguments:
			//
			//     var d = new Dialog('path/to/dialog.html', MyDialogController);
			function Dialog(opts) {

				var self = this, options = this.options = angular.extend({}, defaults, globalOptions, opts);
				this._open = false;

				this.backdropEl = createElement(options.backdropClass);
				if(options.backdropFade){
					this.backdropEl.addClass(options.transitionClass);
					this.backdropEl.removeClass(options.triggerClass);
				}

				this.modalEl = createElement(options.dialogClass);
				if(options.dialogFade){
					this.modalEl.addClass(options.transitionClass);
					this.modalEl.removeClass(options.triggerClass);
				}

				this.handledEscapeKey = function(e) {
					if (e.which === 27) {
						self.close();
						e.preventDefault();
						self.$scope.$apply();
					}
				};

				this.handleBackDropClick = function(e) {
					self.close();
					e.preventDefault();
					self.$scope.$apply();
				};

				this.handleLocationChange = function() {
					self.close();
				};
			}

			// The `isOpen()` method returns wether the dialog is currently visible.
			Dialog.prototype.isOpen = function(){
				return this._open;
			};

			// The `open(templateUrl, controller)` method opens the dialog.
			// Use the `templateUrl` and `controller` arguments if specifying them at dialog creation time is not desired.
			Dialog.prototype.open = function(templateUrl, controller){
				var self = this, options = this.options;

				if(templateUrl){
					options.templateUrl = templateUrl;
				}
				if(controller){
					options.controller = controller;
				}

				if(!(options.template || options.templateUrl)) {
					throw new Error('Dialog.open expected template or templateUrl, neither found. Use options or open method to specify them.');
				}

				this._loadResolves().then(function(locals) {
					var $scope = locals.$scope = self.$scope = locals.$scope ? locals.$scope : $rootScope.$new();

					self.modalEl.html(locals.$template);

					if (self.options.controller) {
						var ctrl = $controller(self.options.controller, locals);
						self.modalEl.children().data('ngControllerController', ctrl);
					}

					$compile(self.modalEl)($scope);
					self._addElementsToDom();
					body.addClass(self.options.dialogOpenClass);

					// trigger tranisitions
					setTimeout(function(){
						if(self.options.dialogFade){ self.modalEl.addClass(self.options.triggerClass); }
						if(self.options.backdropFade){ self.backdropEl.addClass(self.options.triggerClass); }
					});

					self._bindEvents();
				});

				this.deferred = $q.defer();
				return this.deferred.promise;
			};

			// closes the dialog and resolves the promise returned by the `open` method with the specified result.
			Dialog.prototype.close = function(result){
				var self = this;
				var fadingElements = this._getFadingElements();

				body.removeClass(self.options.dialogOpenClass);
				if(fadingElements.length > 0){
					for (var i = fadingElements.length - 1; i >= 0; i--) {
						$transition(fadingElements[i], removeTriggerClass).then(onCloseComplete);
					}
					return;
				}

				this._onCloseComplete(result);

				function removeTriggerClass(el){
					el.removeClass(self.options.triggerClass);
				}

				function onCloseComplete(){
					if(self._open){
						self._onCloseComplete(result);
					}
				}
			};

			Dialog.prototype._getFadingElements = function(){
				var elements = [];
				if(this.options.dialogFade){
					elements.push(this.modalEl);
				}
				if(this.options.backdropFade){
					elements.push(this.backdropEl);
				}

				return elements;
			};

			Dialog.prototype._bindEvents = function() {
				if(this.options.keyboard){ body.bind('keydown', this.handledEscapeKey); }
				if(this.options.backdrop && this.options.backdropClick){ this.backdropEl.bind('click', this.handleBackDropClick); }

				this.$scope.$on('$locationChangeSuccess', this.handleLocationChange);
			};

			Dialog.prototype._unbindEvents = function() {
				if(this.options.keyboard){ body.unbind('keydown', this.handledEscapeKey); }
				if(this.options.backdrop && this.options.backdropClick){ this.backdropEl.unbind('click', this.handleBackDropClick); }
			};

			Dialog.prototype._onCloseComplete = function(result) {
				this._removeElementsFromDom();
				this._unbindEvents();

				this.deferred.resolve(result);
			};

			Dialog.prototype._addElementsToDom = function(){
				body.append(this.modalEl);

				if(this.options.backdrop) {
					if (activeBackdrops.value === 0) {
						body.append(this.backdropEl);
					}
					activeBackdrops.value++;
				}

				this._open = true;
			};

			Dialog.prototype._removeElementsFromDom = function(){
				this.modalEl.remove();

				if(this.options.backdrop) {
					activeBackdrops.value--;
					if (activeBackdrops.value === 0) {
						this.backdropEl.remove();
					}
				}
				this._open = false;
			};

			// Loads all `options.resolve` members to be used as locals for the controller associated with the dialog.
			Dialog.prototype._loadResolves = function(){
				var values = [], keys = [], templatePromise, self = this;

				if (this.options.template) {
					templatePromise = $q.when(this.options.template);
				} else if (this.options.templateUrl) {
					templatePromise = $http.get(this.options.templateUrl, {cache:$templateCache})
						.then(function(response) { return response.data; });
				}

				angular.forEach(this.options.resolve || [], function(value, key) {
					keys.push(key);
					values.push(angular.isString(value) ? $injector.get(value) : $injector.invoke(value));
				});

				keys.push('$template');
				values.push(templatePromise);

				return $q.all(values).then(function(values) {
					var locals = {};
					angular.forEach(values, function(value, index) {
						locals[keys[index]] = value;
					});
					locals.dialog = self;
					return locals;
				});
			};

			// The actual `$dialog` service that is injected in controllers.
			return {
				// Creates a new `Dialog` with the specified options.
				dialog: function(opts){
					return new Dialog(opts);
				},
				// creates a new `Dialog` tied to the default message box template and controller.
				//
				// Arguments `title` and `message` are rendered in the modal header and body sections respectively.
				// The `buttons` array holds an object with the following members for each button to include in the
				// modal footer section:
				//
				// * `result`: the result to pass to the `close` method of the dialog when the button is clicked
				// * `label`: the label of the button
				// * `cssClass`: additional css class(es) to apply to the button for styling
				messageBox: function(title, message, buttons){
					return new Dialog({templateUrl: 'template/dialog/message.html', controller: 'MessageBoxController', resolve:
					{model: function() {
						return {
							title: title,
							message: message,
							buttons: buttons
						};
					}
					}});
				}
			};
		}];
});

/*
 * dropdownToggle - Provides dropdown menu functionality in place of bootstrap js
 * @restrict class or attribute
 * @example:
 <li class="dropdown">
 <a class="dropdown-toggle">My Dropdown Menu</a>
 <ul class="dropdown-menu">
 <li ng-repeat="choice in dropChoices">
 <a ng-href="{{choice.href}}">{{choice.text}}</a>
 </li>
 </ul>
 </li>
 */

angular.module('ui.bootstrap.dropdownToggle', []).directive('dropdownToggle',
	['$document', '$location', '$window', function ($document, $location, $window) {
		var openElement = null,
			closeMenu   = angular.noop;
		return {
			restrict: 'CA',
			link: function(scope, element, attrs) {
				scope.$watch('$location.path', function() { closeMenu(); });
				element.parent().bind('click', function() { closeMenu(); });
				element.bind('click', function(event) {
					event.preventDefault();
					event.stopPropagation();
					var elementWasOpen = (element === openElement);
					if (!!openElement) {
						closeMenu(); }
					if (!elementWasOpen){
						element.parent().addClass('open');
						openElement = element;
						closeMenu = function (event) {
							if (event) {
								event.preventDefault();
								event.stopPropagation();
							}
							$document.unbind('click', closeMenu);
							element.parent().removeClass('open');
							closeMenu   = angular.noop;
							openElement = null;
						};
						$document.bind('click', closeMenu);
					}
				});
			}
		};
	}]);
angular.module('ui.bootstrap.modal', ['ui.bootstrap.dialog'])
	.directive('modal', ['$parse', '$dialog', function($parse, $dialog) {
		return {
			restrict: 'EA',
			terminal: true,
			link: function(scope, elm, attrs) {
				var opts = angular.extend({}, scope.$eval(attrs.uiOptions || attrs.bsOptions || attrs.options));
				var shownExpr = attrs.modal || attrs.show;
				var setClosed;

				// Create a dialog with the template as the contents of the directive
				// Add the current scope as the resolve in order to make the directive scope as a dialog controller scope
				opts = angular.extend(opts, {
					template: elm.html(),
					resolve: { $scope: function() { return scope; } }
				});
				var dialog = $dialog.dialog(opts);

				elm.remove();

				if (attrs.close) {
					setClosed = function() {
						$parse(attrs.close)(scope);
					};
				} else {
					setClosed = function() {
						if (angular.isFunction($parse(shownExpr).assign)) {
							$parse(shownExpr).assign(scope, false);
						}
					};
				}

				scope.$watch(shownExpr, function(isShown, oldShown) {
					if (isShown) {
						dialog.open().then(function(){
							setClosed();
						});
					} else {
						//Make sure it is not opened
						if (dialog.isOpen()){
							dialog.close();
						}
					}
				});
			}
		};
	}]);
angular.module('ui.bootstrap.pagination', [])

	.constant('paginationConfig', {
		boundaryLinks: false,
		directionLinks: true,
		firstText: 'First',
		previousText: 'Previous',
		nextText: 'Next',
		lastText: 'Last'
	})

	.directive('pagination', ['paginationConfig', function(paginationConfig) {
		return {
			restrict: 'EA',
			scope: {
				numPages: '=',
				currentPage: '=',
				maxSize: '=',
				onSelectPage: '&'
			},
			templateUrl: 'template/pagination/pagination.html',
			replace: true,
			link: function(scope, element, attrs) {

				// Setup configuration parameters
				var boundaryLinks = angular.isDefined(attrs.boundaryLinks) ? scope.$eval(attrs.boundaryLinks) : paginationConfig.boundaryLinks;
				var directionLinks = angular.isDefined(attrs.directionLinks) ? scope.$eval(attrs.directionLinks) : paginationConfig.directionLinks;
				var firstText = angular.isDefined(attrs.firstText) ? attrs.firstText : paginationConfig.firstText;
				var previousText = angular.isDefined(attrs.previousText) ? attrs.previousText : paginationConfig.previousText;
				var nextText = angular.isDefined(attrs.nextText) ? attrs.nextText : paginationConfig.nextText;
				var lastText = angular.isDefined(attrs.lastText) ? attrs.lastText : paginationConfig.lastText;

				// Create page object used in template
				function makePage(number, text, isActive, isDisabled) {
					return {
						number: number,
						text: text,
						active: isActive,
						disabled: isDisabled
					};
				}

				scope.$watch('numPages + currentPage + maxSize', function() {
					scope.pages = [];

					// Default page limits
					var startPage = 1, endPage = scope.numPages;

					// recompute if maxSize
					if ( scope.maxSize && scope.maxSize < scope.numPages ) {
						startPage = Math.max(scope.currentPage - Math.floor(scope.maxSize/2), 1);
						endPage   = startPage + scope.maxSize - 1;

						// Adjust if limit is exceeded
						if (endPage > scope.numPages) {
							endPage   = scope.numPages;
							startPage = endPage - scope.maxSize + 1;
						}
					}

					// Add page number links
					for (var number = startPage; number <= endPage; number++) {
						var page = makePage(number, number, scope.isActive(number), false);
						scope.pages.push(page);
					}

					// Add previous & next links
					if (directionLinks) {
						var previousPage = makePage(scope.currentPage - 1, previousText, false, scope.noPrevious());
						scope.pages.unshift(previousPage);

						var nextPage = makePage(scope.currentPage + 1, nextText, false, scope.noNext());
						scope.pages.push(nextPage);
					}

					// Add first & last links
					if (boundaryLinks) {
						var firstPage = makePage(1, firstText, false, scope.noPrevious());
						scope.pages.unshift(firstPage);

						var lastPage = makePage(scope.numPages, lastText, false, scope.noNext());
						scope.pages.push(lastPage);
					}


					if ( scope.currentPage > scope.numPages ) {
						scope.selectPage(scope.numPages);
					}
				});
				scope.noPrevious = function() {
					return scope.currentPage === 1;
				};
				scope.noNext = function() {
					return scope.currentPage === scope.numPages;
				};
				scope.isActive = function(page) {
					return scope.currentPage === page;
				};

				scope.selectPage = function(page) {
					if ( ! scope.isActive(page) && page > 0 && page <= scope.numPages) {
						scope.currentPage = page;
						scope.onSelectPage({ page: page });
					}
				};
			}
		};
	}]);
angular.module('ui.bootstrap.position', [])

/**
 * A set of utility methods that can be use to retrieve position of DOM elements.
 * It is meant to be used where we need to absolute-position DOM elements in
 * relation to other, existing elements (this is the case for tooltips, popovers,
 * typeahead suggestions etc.).
 */
	.factory('$position', ['$document', '$window', function ($document, $window) {

		function getStyle(el, cssprop) {
			if (el.currentStyle) { //IE
				return el.currentStyle[cssprop];
			} else if ($window.getComputedStyle) {
				return $window.getComputedStyle(el)[cssprop];
			}
			// finally try and get inline style
			return el.style[cssprop];
		}

		/**
		 * Checks if a given element is statically positioned
		 * @param element - raw DOM element
		 */
		function isStaticPositioned(element) {
			return (getStyle(element, "position") || 'static' ) === 'static';
		}

		/**
		 * returns the closest, non-statically positioned parentOffset of a given element
		 * @param element
		 */
		var parentOffsetEl = function (element) {
			var docDomEl = $document[0];
			var offsetParent = element.offsetParent || docDomEl;
			while (offsetParent && offsetParent !== docDomEl && isStaticPositioned(offsetParent) ) {
				offsetParent = offsetParent.offsetParent;
			}
			return offsetParent || docDomEl;
		};

		return {
			/**
			 * Provides read-only equivalent of jQuery's position function:
			 * http://api.jquery.com/position/
			 */
			position: function (element) {
				var elBCR = this.offset(element);
				var offsetParentBCR = { top: 0, left: 0 };
				var offsetParentEl = parentOffsetEl(element[0]);
				if (offsetParentEl != $document[0]) {
					offsetParentBCR = this.offset(angular.element(offsetParentEl));
					offsetParentBCR.top += offsetParentEl.clientTop;
					offsetParentBCR.left += offsetParentEl.clientLeft;
				}

				return {
					width: element.prop('offsetWidth'),
					height: element.prop('offsetHeight'),
					top: elBCR.top - offsetParentBCR.top,
					left: elBCR.left - offsetParentBCR.left
				};
			},

			/**
			 * Provides read-only equivalent of jQuery's offset function:
			 * http://api.jquery.com/offset/
			 */
			offset: function (element) {
				var boundingClientRect = element[0].getBoundingClientRect();
				return {
					width: element.prop('offsetWidth'),
					height: element.prop('offsetHeight'),
					top: boundingClientRect.top + ($window.pageYOffset || $document[0].body.scrollTop),
					left: boundingClientRect.left + ($window.pageXOffset || $document[0].body.scrollLeft)
				};
			}
		};
	}]);

/**
 * The following features are still outstanding: animation as a
 * function, placement as a function, inside, support for more triggers than
 * just mouse enter/leave, html tooltips, and selector delegation.
 */
angular.module( 'ui.bootstrap.tooltip', [ 'ui.bootstrap.position' ] )

/**
 * The $tooltip service creates tooltip- and popover-like directives as well as
 * houses global options for them.
 */
	.provider( '$tooltip', function () {
		// The default options tooltip and popover.
		var defaultOptions = {
			placement: 'top',
			animation: true,
			popupDelay: 0
		};

		// Default hide triggers for each show trigger
		var triggerMap = {
			'mouseenter': 'mouseleave',
			'click': 'click',
			'focus': 'blur'
		};

		// The options specified to the provider globally.
		var globalOptions = {};

		/**
		 * `options({})` allows global configuration of all tooltips in the
		 * application.
		 *
		 *   var app = angular.module( 'App', ['ui.bootstrap.tooltip'], function( $tooltipProvider ) {
   *     // place tooltips left instead of top by default
   *     $tooltipProvider.options( { placement: 'left' } );
   *   });
		 */
		this.options = function( value ) {
			angular.extend( globalOptions, value );
		};

		/**
		 * This is a helper function for translating camel-case to snake-case.
		 */
		function snake_case(name){
			var regexp = /[A-Z]/g;
			var separator = '-';
			return name.replace(regexp, function(letter, pos) {
				return (pos ? separator : '') + letter.toLowerCase();
			});
		}

		/**
		 * Returns the actual instance of the $tooltip service.
		 * TODO support multiple triggers
		 */
		this.$get = [ '$window', '$compile', '$timeout', '$parse', '$document', '$position', function ( $window, $compile, $timeout, $parse, $document, $position ) {
			return function $tooltip ( type, prefix, defaultTriggerShow ) {
				var options = angular.extend( {}, defaultOptions, globalOptions );

				/**
				 * Returns an object of show and hide triggers.
				 *
				 * If a trigger is supplied,
				 * it is used to show the tooltip; otherwise, it will use the `trigger`
				 * option passed to the `$tooltipProvider.options` method; else it will
				 * default to the trigger supplied to this directive factory.
				 *
				 * The hide trigger is based on the show trigger. If the `trigger` option
				 * was passed to the `$tooltipProvider.options` method, it will use the
				 * mapped trigger from `triggerMap` or the passed trigger if the map is
				 * undefined; otherwise, it uses the `triggerMap` value of the show
				 * trigger; else it will just use the show trigger.
				 */
				function setTriggers ( trigger ) {
					var show, hide;

					show = trigger || options.trigger || defaultTriggerShow;
					if ( angular.isDefined ( options.trigger ) ) {
						hide = triggerMap[options.trigger] || show;
					} else {
						hide = triggerMap[show] || show;
					}

					return {
						show: show,
						hide: hide
					};
				}

				var directiveName = snake_case( type );
				var triggers = setTriggers( undefined );

				var template =
					'<'+ directiveName +'-popup '+
						'title="{{tt_title}}" '+
						'content="{{tt_content}}" '+
						'placement="{{tt_placement}}" '+
						'animation="tt_animation()" '+
						'is-open="tt_isOpen"'+
						'>'+
						'</'+ directiveName +'-popup>';

				return {
					restrict: 'EA',
					scope: true,
					link: function link ( scope, element, attrs ) {
						var tooltip = $compile( template )( scope );
						var transitionTimeout;
						var popupTimeout;
						var $body;

						// By default, the tooltip is not open.
						// TODO add ability to start tooltip opened
						scope.tt_isOpen = false;

						function toggleTooltipBind () {
							if ( ! scope.tt_isOpen ) {
								showTooltipBind();
							} else {
								hideTooltipBind();
							}
						}

						// Show the tooltip with delay if specified, otherwise show it immediately
						function showTooltipBind() {
							if ( scope.tt_popupDelay ) {
								popupTimeout = $timeout( show, scope.tt_popupDelay );
							} else {
								scope.$apply( show );
							}
						}

						function hideTooltipBind () {
							scope.$apply(function () {
								hide();
							});
						}

						// Show the tooltip popup element.
						function show() {
							var position,
								ttWidth,
								ttHeight,
								ttPosition;

							// Don't show empty tooltips.
							if ( ! scope.tt_content ) {
								return;
							}

							// If there is a pending remove transition, we must cancel it, lest the
							// tooltip be mysteriously removed.
							if ( transitionTimeout ) {
								$timeout.cancel( transitionTimeout );
							}

							// Set the initial positioning.
							tooltip.css({ top: 0, left: 0, display: 'block' });

							// Now we add it to the DOM because need some info about it. But it's not
							// visible yet anyway.
							if ( options.appendToBody ) {
								$body = $body || $document.find( 'body' );
								$body.append( tooltip );
							} else {
								element.after( tooltip );
							}

							// Get the position of the directive element.
							position = $position.position( element );

							// Get the height and width of the tooltip so we can center it.
							ttWidth = tooltip.prop( 'offsetWidth' );
							ttHeight = tooltip.prop( 'offsetHeight' );

							// Calculate the tooltip's top and left coordinates to center it with
							// this directive.
							switch ( scope.tt_placement ) {
								case 'right':
									ttPosition = {
										top: (position.top + position.height / 2 - ttHeight / 2) + 'px',
										left: (position.left + position.width) + 'px'
									};
									break;
								case 'bottom':
									ttPosition = {
										top: (position.top + position.height) + 'px',
										left: (position.left + position.width / 2 - ttWidth / 2) + 'px'
									};
									break;
								case 'left':
									ttPosition = {
										top: (position.top + position.height / 2 - ttHeight / 2) + 'px',
										left: (position.left - ttWidth) + 'px'
									};
									break;
								default:
									ttPosition = {
										top: (position.top - ttHeight) + 'px',
										left: (position.left + position.width / 2 - ttWidth / 2) + 'px'
									};
									break;
							}

							// Now set the calculated positioning.
							tooltip.css( ttPosition );

							// And show the tooltip.
							scope.tt_isOpen = true;
						}

						// Hide the tooltip popup element.
						function hide() {
							// First things first: we don't show it anymore.
							scope.tt_isOpen = false;

							//if tooltip is going to be shown after delay, we must cancel this
							$timeout.cancel( popupTimeout );

							// And now we remove it from the DOM. However, if we have animation, we
							// need to wait for it to expire beforehand.
							// FIXME: this is a placeholder for a port of the transitions library.
							if ( angular.isDefined( scope.tt_animation ) && scope.tt_animation() ) {
								transitionTimeout = $timeout( function () { tooltip.remove(); }, 500 );
							} else {
								tooltip.remove();
							}
						}

						/**
						 * Observe the relevant attributes.
						 */
						attrs.$observe( type, function ( val ) {
							scope.tt_content = val;
						});

						attrs.$observe( prefix+'Title', function ( val ) {
							scope.tt_title = val;
						});

						attrs.$observe( prefix+'Placement', function ( val ) {
							scope.tt_placement = angular.isDefined( val ) ? val : options.placement;
						});

						attrs.$observe( prefix+'Animation', function ( val ) {
							scope.tt_animation = angular.isDefined( val ) ? $parse( val ) : function(){ return options.animation; };
						});

						attrs.$observe( prefix+'PopupDelay', function ( val ) {
							var delay = parseInt( val, 10 );
							scope.tt_popupDelay = ! isNaN(delay) ? delay : options.popupDelay;
						});

						attrs.$observe( prefix+'Trigger', function ( val ) {
							element.unbind( triggers.show );
							element.unbind( triggers.hide );

							triggers = setTriggers( val );

							if ( triggers.show === triggers.hide ) {
								element.bind( triggers.show, toggleTooltipBind );
							} else {
								element.bind( triggers.show, showTooltipBind );
								element.bind( triggers.hide, hideTooltipBind );
							}
						});
					}
				};
			};
		}];
	})

	.directive( 'tooltipPopup', function () {
		return {
			restrict: 'E',
			replace: true,
			scope: { content: '@', placement: '@', animation: '&', isOpen: '&' },
			templateUrl: 'template/tooltip/tooltip-popup.html'
		};
	})

	.directive( 'tooltip', [ '$tooltip', function ( $tooltip ) {
		return $tooltip( 'tooltip', 'tooltip', 'mouseenter' );
	}])

	.directive( 'tooltipHtmlUnsafePopup', function () {
		return {
			restrict: 'E',
			replace: true,
			scope: { content: '@', placement: '@', animation: '&', isOpen: '&' },
			templateUrl: 'template/tooltip/tooltip-html-unsafe-popup.html'
		};
	})

	.directive( 'tooltipHtmlUnsafe', [ '$tooltip', function ( $tooltip ) {
		return $tooltip( 'tooltipHtmlUnsafe', 'tooltip', 'mouseenter' );
	}])

;


/**
 * The following features are still outstanding: popup delay, animation as a
 * function, placement as a function, inside, support for more triggers than
 * just mouse enter/leave, html popovers, and selector delegatation.
 */
angular.module( 'ui.bootstrap.popover', [ 'ui.bootstrap.tooltip' ] )
	.directive( 'popoverPopup', function () {
		return {
			restrict: 'EA',
			replace: true,
			scope: { title: '@', content: '@', placement: '@', animation: '&', isOpen: '&' },
			templateUrl: 'template/popover/popover.html'
		};
	})
	.directive( 'popover', [ '$compile', '$timeout', '$parse', '$window', '$tooltip', function ( $compile, $timeout, $parse, $window, $tooltip ) {
		return $tooltip( 'popover', 'popover', 'click' );
	}]);


angular.module('ui.bootstrap.progressbar', ['ui.bootstrap.transition'])

	.constant('progressConfig', {
		animate: true,
		autoType: false,
		stackedTypes: ['success', 'info', 'warning', 'danger']
	})

	.controller('ProgressBarController', ['$scope', '$attrs', 'progressConfig', function($scope, $attrs, progressConfig) {

		// Whether bar transitions should be animated
		var animate = angular.isDefined($attrs.animate) ? $scope.$eval($attrs.animate) : progressConfig.animate;
		var autoType = angular.isDefined($attrs.autoType) ? $scope.$eval($attrs.autoType) : progressConfig.autoType;
		var stackedTypes = angular.isDefined($attrs.stackedTypes) ? $scope.$eval('[' + $attrs.stackedTypes + ']') : progressConfig.stackedTypes;

		// Create bar object
		this.makeBar = function(newBar, oldBar, index) {
			var newValue = (angular.isObject(newBar)) ? newBar.value : (newBar || 0);
			var oldValue =  (angular.isObject(oldBar)) ? oldBar.value : (oldBar || 0);
			var type = (angular.isObject(newBar) && angular.isDefined(newBar.type)) ? newBar.type : (autoType) ? getStackedType(index || 0) : null;

			return {
				from: oldValue,
				to: newValue,
				type: type,
				animate: animate
			};
		};

		function getStackedType(index) {
			return stackedTypes[index];
		}

		this.addBar = function(bar) {
			$scope.bars.push(bar);
			$scope.totalPercent += bar.to;
		};

		this.clearBars = function() {
			$scope.bars = [];
			$scope.totalPercent = 0;
		};
		this.clearBars();
	}])

	.directive('progress', function() {
		return {
			restrict: 'EA',
			replace: true,
			controller: 'ProgressBarController',
			scope: {
				value: '=',
				onFull: '&',
				onEmpty: '&'
			},
			templateUrl: 'template/progressbar/progress.html',
			link: function(scope, element, attrs, controller) {
				scope.$watch('value', function(newValue, oldValue) {
					controller.clearBars();

					if (angular.isArray(newValue)) {
						// Stacked progress bar
						for (var i=0, n=newValue.length; i < n; i++) {
							controller.addBar(controller.makeBar(newValue[i], oldValue[i], i));
						}
					} else {
						// Simple bar
						controller.addBar(controller.makeBar(newValue, oldValue));
					}
				}, true);

				// Total percent listeners
				scope.$watch('totalPercent', function(value) {
					if (value >= 100) {
						scope.onFull();
					} else if (value <= 0) {
						scope.onEmpty();
					}
				}, true);
			}
		};
	})

	.directive('progressbar', ['$transition', function($transition) {
		return {
			restrict: 'EA',
			replace: true,
			scope: {
				width: '=',
				old: '=',
				type: '=',
				animate: '='
			},
			templateUrl: 'template/progressbar/bar.html',
			link: function(scope, element) {
				scope.$watch('width', function(value) {
					if (scope.animate) {
						element.css('width', scope.old + '%');
						$transition(element, {width: value + '%'});
					} else {
						element.css('width', value + '%');
					}
				});
			}
		};
	}]);
angular.module('ui.bootstrap.rating', [])

	.constant('ratingConfig', {
		max: 5
	})

	.directive('rating', ['ratingConfig', '$parse', function(ratingConfig, $parse) {
		return {
			restrict: 'EA',
			scope: {
				value: '='
			},
			templateUrl: 'template/rating/rating.html',
			replace: true,
			link: function(scope, element, attrs) {

				var maxRange = angular.isDefined(attrs.max) ? scope.$eval(attrs.max) : ratingConfig.max;

				scope.range = [];
				for (var i = 1; i <= maxRange; i++) {
					scope.range.push(i);
				}

				scope.rate = function(value) {
					if ( ! scope.readonly ) {
						scope.value = value;
					}
				};

				scope.enter = function(value) {
					if ( ! scope.readonly ) {
						scope.val = value;
					}
				};

				scope.reset = function() {
					scope.val = angular.copy(scope.value);
				};
				scope.reset();

				scope.$watch('value', function(value) {
					scope.val = value;
				});

				scope.readonly = false;
				if (attrs.readonly) {
					scope.$parent.$watch($parse(attrs.readonly), function(value) {
						scope.readonly = !!value;
					});
				}
			}
		};
	}]);
angular.module('ui.bootstrap.tabs', [])
	.controller('TabsController', ['$scope', '$element', function($scope, $element) {
		var panes = $scope.panes = [];

		this.select = $scope.select = function selectPane(pane) {
			angular.forEach(panes, function(pane) {
				pane.selected = false;
			});
			pane.selected = true;
		};

		this.addPane = function addPane(pane) {
			if (!panes.length) {
				$scope.select(pane);
			}
			panes.push(pane);
		};

		this.removePane = function removePane(pane) {
			var index = panes.indexOf(pane);
			panes.splice(index, 1);
			//Select a new pane if removed pane was selected
			if (pane.selected && panes.length > 0) {
				$scope.select(panes[index < panes.length ? index : index-1]);
			}
		};
	}])
	.directive('tabs', function() {
		return {
			restrict: 'EA',
			transclude: true,
			scope: {},
			controller: 'TabsController',
			templateUrl: 'template/tabs/tabs.html',
			replace: true
		};
	})
	.directive('pane', ['$parse', function($parse) {
		return {
			require: '^tabs',
			restrict: 'EA',
			transclude: true,
			scope:{
				heading:'@'
			},
			link: function(scope, element, attrs, tabsCtrl) {
				var getSelected, setSelected;
				scope.selected = false;
				if (attrs.active) {
					getSelected = $parse(attrs.active);
					setSelected = getSelected.assign;
					scope.$watch(
						function watchSelected() {return getSelected(scope.$parent);},
						function updateSelected(value) {scope.selected = value;}
					);
					scope.selected = getSelected ? getSelected(scope.$parent) : false;
				}
				scope.$watch('selected', function(selected) {
					if(selected) {
						tabsCtrl.select(scope);
					}
					if(setSelected) {
						setSelected(scope.$parent, selected);
					}
				});

				tabsCtrl.addPane(scope);
				scope.$on('$destroy', function() {
					tabsCtrl.removePane(scope);
				});
			},
			templateUrl: 'template/tabs/pane.html',
			replace: true
		};
	}]);

angular.module('ui.bootstrap.typeahead', ['ui.bootstrap.position'])

/**
 * A helper service that can parse typeahead's syntax (string provided by users)
 * Extracted to a separate service for ease of unit testing
 */
	.factory('typeaheadParser', ['$parse', function ($parse) {

		//                      00000111000000000000022200000000000000003333333333333330000000000044000
		var TYPEAHEAD_REGEXP = /^\s*(.*?)(?:\s+as\s+(.*?))?\s+for\s+(?:([\$\w][\$\w\d]*))\s+in\s+(.*)$/;

		return {
			parse:function (input) {

				var match = input.match(TYPEAHEAD_REGEXP), modelMapper, viewMapper, source;
				if (!match) {
					throw new Error(
						"Expected typeahead specification in form of '_modelValue_ (as _label_)? for _item_ in _collection_'" +
							" but got '" + input + "'.");
				}

				return {
					itemName:match[3],
					source:$parse(match[4]),
					viewMapper:$parse(match[2] || match[1]),
					modelMapper:$parse(match[1])
				};
			}
		};
	}])

	.directive('typeahead', ['$compile', '$parse', '$q', '$document', '$position', 'typeaheadParser', function ($compile, $parse, $q, $document, $position, typeaheadParser) {

		var HOT_KEYS = [9, 13, 27, 38, 40];

		return {
			require:'ngModel',
			link:function (originalScope, element, attrs, modelCtrl) {

				var selected;

				//minimal no of characters that needs to be entered before typeahead kicks-in
				var minSearch = originalScope.$eval(attrs.typeaheadMinLength) || 1;

				//expressions used by typeahead
				var parserResult = typeaheadParser.parse(attrs.typeahead);

				//should it restrict model values to the ones selected from the popup only?
				var isEditable = originalScope.$eval(attrs.typeaheadEditable) !== false;

				var isLoadingSetter = $parse(attrs.typeaheadLoading).assign || angular.noop;

				//pop-up element used to display matches
				var popUpEl = angular.element(
					"<typeahead-popup " +
						"matches='matches' " +
						"active='activeIdx' " +
						"select='select(activeIdx)' "+
						"query='query' "+
						"position='position'>"+
						"</typeahead-popup>");

				//create a child scope for the typeahead directive so we are not polluting original scope
				//with typeahead-specific data (matches, query etc.)
				var scope = originalScope.$new();
				originalScope.$on('$destroy', function(){
					scope.$destroy();
				});

				var resetMatches = function() {
					scope.matches = [];
					scope.activeIdx = -1;
				};

				var getMatchesAsync = function(inputValue) {

					var locals = {$viewValue: inputValue};
					isLoadingSetter(originalScope, true);
					$q.when(parserResult.source(scope, locals)).then(function(matches) {

						//it might happen that several async queries were in progress if a user were typing fast
						//but we are interested only in responses that correspond to the current view value
						if (inputValue === modelCtrl.$viewValue) {
							if (matches.length > 0) {

								scope.activeIdx = 0;
								scope.matches.length = 0;

								//transform labels
								for(var i=0; i<matches.length; i++) {
									locals[parserResult.itemName] = matches[i];
									scope.matches.push({
										label: parserResult.viewMapper(scope, locals),
										model: matches[i]
									});
								}

								scope.query = inputValue;
								//position pop-up with matches - we need to re-calculate its position each time we are opening a window
								//with matches as a pop-up might be absolute-positioned and position of an input might have changed on a page
								//due to other elements being rendered
								scope.position = $position.position(element);
								scope.position.top = scope.position.top + element.prop('offsetHeight');

							} else {
								resetMatches();
							}
							isLoadingSetter(originalScope, false);
						}
					}, function(){
						resetMatches();
						isLoadingSetter(originalScope, false);
					});
				};

				resetMatches();

				//we need to propagate user's query so we can higlight matches
				scope.query = undefined;

				//plug into $parsers pipeline to open a typeahead on view changes initiated from DOM
				//$parsers kick-in on all the changes coming from the view as well as manually triggered by $setViewValue
				modelCtrl.$parsers.push(function (inputValue) {

					resetMatches();
					if (selected) {
						return inputValue;
					} else {
						if (inputValue && inputValue.length >= minSearch) {
							getMatchesAsync(inputValue);
						}
					}

					return isEditable ? inputValue : undefined;
				});

				modelCtrl.$render = function () {
					var locals = {};
					locals[parserResult.itemName] = selected || modelCtrl.$viewValue;
					element.val(parserResult.viewMapper(scope, locals) || modelCtrl.$viewValue);
					selected = undefined;
				};

				scope.select = function (activeIdx) {
					//called from within the $digest() cycle
					var locals = {};
					locals[parserResult.itemName] = selected = scope.matches[activeIdx].model;

					modelCtrl.$setViewValue(parserResult.modelMapper(scope, locals));
					modelCtrl.$render();
				};

				//bind keyboard events: arrows up(38) / down(40), enter(13) and tab(9), esc(27)
				element.bind('keydown', function (evt) {

					//typeahead is open and an "interesting" key was pressed
					if (scope.matches.length === 0 || HOT_KEYS.indexOf(evt.which) === -1) {
						return;
					}

					evt.preventDefault();

					if (evt.which === 40) {
						scope.activeIdx = (scope.activeIdx + 1) % scope.matches.length;
						scope.$digest();

					} else if (evt.which === 38) {
						scope.activeIdx = (scope.activeIdx ? scope.activeIdx : scope.matches.length) - 1;
						scope.$digest();

					} else if (evt.which === 13 || evt.which === 9) {
						scope.$apply(function () {
							scope.select(scope.activeIdx);
						});

					} else if (evt.which === 27) {
						evt.stopPropagation();

						resetMatches();
						scope.$digest();
					}
				});

				$document.bind('click', function(){
					resetMatches();
					scope.$digest();
				});

				element.after($compile(popUpEl)(scope));
			}
		};

	}])

	.directive('typeaheadPopup', function () {
		return {
			restrict:'E',
			scope:{
				matches:'=',
				query:'=',
				active:'=',
				position:'=',
				select:'&'
			},
			replace:true,
			templateUrl:'template/typeahead/typeahead.html',
			link:function (scope, element, attrs) {

				scope.isOpen = function () {
					return scope.matches.length > 0;
				};

				scope.isActive = function (matchIdx) {
					return scope.active == matchIdx;
				};

				scope.selectActive = function (matchIdx) {
					scope.active = matchIdx;
				};

				scope.selectMatch = function (activeIdx) {
					scope.select({activeIdx:activeIdx});
				};
			}
		};
	})

	.filter('typeaheadHighlight', function() {

		function escapeRegexp(queryToEscape) {
			return queryToEscape.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
		}

		return function(matchItem, query) {
			return query ? matchItem.replace(new RegExp(escapeRegexp(query), 'gi'), '<strong>$&</strong>') : query;
		};
	});
angular.module("template/accordion/accordion-group.html", []).run(["$templateCache", function($templateCache) {
	$templateCache.put("template/accordion/accordion-group.html",
		"<div class=\"accordion-group\">\n" +
			"  <div class=\"accordion-heading\" ><a class=\"accordion-toggle\" ng-click=\"isOpen = !isOpen\" accordion-transclude=\"heading\">{{heading}}</a></div>\n" +
			"  <div class=\"accordion-body\" collapse=\"!isOpen\">\n" +
			"    <div class=\"accordion-inner\" ng-transclude></div>  </div>\n" +
			"</div>");
}]);

angular.module("template/accordion/accordion.html", []).run(["$templateCache", function($templateCache) {
	$templateCache.put("template/accordion/accordion.html",
		"<div class=\"accordion\" ng-transclude></div>");
}]);

angular.module("template/alert/alert.html", []).run(["$templateCache", function($templateCache) {
	$templateCache.put("template/alert/alert.html",
		"<div class='alert' ng-class='type && \"alert-\" + type'>\n" +
			"    <button ng-show='closeable' type='button' class='close' ng-click='close()'>&times;</button>\n" +
			"    <div ng-transclude></div>\n" +
			"</div>\n" +
			"");
}]);

angular.module("template/carousel/carousel.html", []).run(["$templateCache", function($templateCache) {
	$templateCache.put("template/carousel/carousel.html",
		"<div ng-mouseenter=\"pause()\" ng-mouseleave=\"play()\" class=\"carousel\">\n" +
			"    <ol class=\"carousel-indicators\" ng-show=\"slides().length > 1\">\n" +
			"        <li ng-repeat=\"slide in slides()\" ng-class=\"{active: isActive(slide)}\" ng-click=\"select(slide)\"></li>\n" +
			"    </ol>\n" +
			"    <div class=\"carousel-inner\" ng-transclude></div>\n" +
			"    <a ng-click=\"prev()\" class=\"carousel-control left\" ng-show=\"slides().length > 1\">&lsaquo;</a>\n" +
			"    <a ng-click=\"next()\" class=\"carousel-control right\" ng-show=\"slides().length > 1\">&rsaquo;</a>\n" +
			"</div>\n" +
			"");
}]);

angular.module("template/carousel/slide.html", []).run(["$templateCache", function($templateCache) {
	$templateCache.put("template/carousel/slide.html",
		"<div ng-class=\"{\n" +
			"    'active': leaving || (active && !entering),\n" +
			"    'prev': (next || active) && direction=='prev',\n" +
			"    'next': (next || active) && direction=='next',\n" +
			"    'right': direction=='prev',\n" +
			"    'left': direction=='next'\n" +
			"  }\" class=\"item\" ng-transclude></div>\n" +
			"");
}]);

angular.module("template/dialog/message.html", []).run(["$templateCache", function($templateCache) {
	$templateCache.put("template/dialog/message.html",
		"<div class=\"modal-header\">\n" +
			"	<h1>{{ title }}</h1>\n" +
			"</div>\n" +
			"<div class=\"modal-body\">\n" +
			"	<p>{{ message }}</p>\n" +
			"</div>\n" +
			"<div class=\"modal-footer\">\n" +
			"	<button ng-repeat=\"btn in buttons\" ng-click=\"close(btn.result)\" class=btn ng-class=\"btn.cssClass\">{{ btn.label }}</button>\n" +
			"</div>\n" +
			"");
}]);

angular.module("template/pagination/pagination.html", []).run(["$templateCache", function($templateCache) {
	$templateCache.put("template/pagination/pagination.html",
		"<div class=\"pagination\"><ul>\n" +
			"  <li ng-repeat=\"page in pages\" ng-class=\"{active: page.active, disabled: page.disabled}\"><a ng-click=\"selectPage(page.number)\">{{page.text}}</a></li>\n" +
			"  </ul>\n" +
			"</div>\n" +
			"");
}]);

angular.module("template/tooltip/tooltip-html-unsafe-popup.html", []).run(["$templateCache", function($templateCache) {
	$templateCache.put("template/tooltip/tooltip-html-unsafe-popup.html",
		"<div class=\"tooltip {{placement}}\" ng-class=\"{ in: isOpen(), fade: animation() }\">\n" +
			"  <div class=\"tooltip-arrow\"></div>\n" +
			"  <div class=\"tooltip-inner\" ng-bind-html-unsafe=\"content\"></div>\n" +
			"</div>\n" +
			"");
}]);

angular.module("template/tooltip/tooltip-popup.html", []).run(["$templateCache", function($templateCache) {
	$templateCache.put("template/tooltip/tooltip-popup.html",
		"<div class=\"tooltip {{placement}}\" ng-class=\"{ in: isOpen(), fade: animation() }\">\n" +
			"  <div class=\"tooltip-arrow\"></div>\n" +
			"  <div class=\"tooltip-inner\" ng-bind=\"content\"></div>\n" +
			"</div>\n" +
			"");
}]);

angular.module("template/popover/popover.html", []).run(["$templateCache", function($templateCache) {
	$templateCache.put("template/popover/popover.html",
		"<div class=\"popover {{placement}}\" ng-class=\"{ in: isOpen(), fade: animation() }\">\n" +
			"  <div class=\"arrow\"></div>\n" +
			"\n" +
			"  <div class=\"popover-inner\">\n" +
			"      <h3 class=\"popover-title\" ng-bind=\"title\" ng-show=\"title\"></h3>\n" +
			"      <div class=\"popover-content\" ng-bind=\"content\"></div>\n" +
			"  </div>\n" +
			"</div>\n" +
			"");
}]);

angular.module("template/progressbar/bar.html", []).run(["$templateCache", function($templateCache) {
	$templateCache.put("template/progressbar/bar.html",
		"<div class=\"bar\" ng-class='type && \"bar-\" + type'></div>");
}]);

angular.module("template/progressbar/progress.html", []).run(["$templateCache", function($templateCache) {
	$templateCache.put("template/progressbar/progress.html",
		"<div class=\"progress\"><progressbar ng-repeat=\"bar in bars\" width=\"bar.to\" old=\"bar.from\" animate=\"bar.animate\" type=\"bar.type\"></progressbar></div>");
}]);

angular.module("template/rating/rating.html", []).run(["$templateCache", function($templateCache) {
	$templateCache.put("template/rating/rating.html",
		"<span ng-mouseleave=\"reset()\">\n" +
			"	<i ng-repeat=\"number in range\" ng-mouseenter=\"enter(number)\" ng-click=\"rate(number)\" ng-class=\"{'icon-star': number <= val, 'icon-star-empty': number > val}\"></i>\n" +
			"</span>\n" +
			"");
}]);

angular.module("template/tabs/pane.html", []).run(["$templateCache", function($templateCache) {
	$templateCache.put("template/tabs/pane.html",
		"<div class=\"tab-pane\" ng-class=\"{active: selected}\" ng-show=\"selected\" ng-transclude></div>\n" +
			"");
}]);

angular.module("template/tabs/tabs.html", []).run(["$templateCache", function($templateCache) {
	$templateCache.put("template/tabs/tabs.html",
		"<div class=\"tabbable\">\n" +
			"  <ul class=\"nav nav-tabs\">\n" +
			"    <li ng-repeat=\"pane in panes\" ng-class=\"{active:pane.selected}\">\n" +
			"      <a ng-click=\"select(pane)\">{{pane.heading}}</a>\n" +
			"    </li>\n" +
			"  </ul>\n" +
			"  <div class=\"tab-content\" ng-transclude></div>\n" +
			"</div>\n" +
			"");
}]);

angular.module("template/typeahead/match.html", []).run(["$templateCache", function($templateCache){
	$templateCache.put("template/typeahead/match.html",
		"<a tabindex=\"-1\" ng-bind-html-unsafe=\"match.label | typeaheadHighlight:query\"></a>");
}]);

angular.module("template/typeahead/typeahead.html", []).run(["$templateCache", function($templateCache) {
	$templateCache.put("template/typeahead/typeahead.html",
		"<ul class=\"typeahead dropdown-menu\" ng-style=\"{display: isOpen()&&'block' || 'none', top: position.top+'px', left: position.left+'px'}\">\n" +
			"    <li ng-repeat=\"match in matches\" ng-class=\"{active: isActive($index) }\" ng-mouseenter=\"selectActive($index)\">\n" +
			"        <a tabindex=\"-1\" ng-click=\"selectMatch($index)\" ng-bind-html-unsafe=\"match.label | typeaheadHighlight:query\"></a>\n" +
			"    </li>\n" +
			"</ul>");
}]);;/**
 * Created with JetBrains WebStorm.
 * User: Administrator
 * Date: 5/10/13
 * Time: 10:14 PM
 * To change this template use File | Settings | File Templates.
 */
angular.module('ui.ssnau', ["ui.ssnau.tagInput", "ui.ssnau.powerButton", "ui.ssnau.buttonInput"]);;/**
 * General-purpose Event binding. Bind any event not natively supported by Angular
 * Pass an object with keynames for events to ui-event
 * Allows $event object and $params object to be passed
 *
 * @example <input ui-event="{ focus : 'counter++', blur : 'someCallback()' }">
 * @example <input ui-event="{ myCustomEvent : 'myEventHandler($event, $params)'}">
 *
 * @param ui-event {string|object literal} The event to bind to as a string or a hash of events with their callbacks
 */
angular.module('ui.event', []).directive('uiEvent', ['$parse',
    function ($parse) {
        return function ($scope, elm, attrs) {
            var events = $scope.$eval(attrs.uiEvent);
            angular.forEach(events, function (uiEvent, eventName) {
                var fn = $parse(uiEvent);
                elm.bind(eventName, function (evt) {
                    var params = Array.prototype.slice.call(arguments);
                    //Take out first paramater (event object);
                    params = params.splice(1);
                    fn($scope, {$event: evt, $params: params});
                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                });
            });
        };
    }]);
;/*"use strict";
angular.module("resource.entry", ["resource"])

.factory("Entry", ["Resource", function(resourceFactory){
  var Entry = resourceFactory("entry"); //Resource is a factory function, we do not need a `new` operator

  // get 10 articles from home page
  Entry.home = function() {
    return Entry.set('limit', 10).query({});
  };

  return Entry;
}])

.filter("testfilter", ["Resource", function(resourceFactory){
  return function(input) {
    return 'hello';
  };
}]);*/


"use strict";
angular.module("resource.entry", ["appResource"])

.factory("Entry", ["ResourceFactory", "urlBase", function(ResourceFactory, urlBase){
  var Entry = ResourceFactory(urlBase + "rest/entry/:id", {"id":"@id"}); //$resource is a factory function, we do not need a `new` operator

  return Entry;
}]);;"use strict";
angular.module("resource.note", ["appResource"])

.factory("Note", ["ResourceFactory", "urlBase", function(ResourceFactory, urlBase){
    var Note = ResourceFactory(
        urlBase + "rest/note/:id",
        {"id":"@id"},
        {
            search: {
                method : "GET",
                isArray: true,
                params: {"action": "search"}
            }
        }
    ); //$resource is a factory function, we do not need a `new` operator

    return Note;
}])
.factory("NoteCollection", ["CollectionFacotry", "Note", function(CollectionFacotry, Note) {
    return CollectionFacotry(Note);
}])
;;"use strict";
angular.module("resource.notebook", ["appResource"])

.factory("NoteBook", ["ResourceFactory", "urlBase", function(ResourceFactory, urlBase){
    var NoteBook = ResourceFactory(urlBase + "rest/notebook/:id", {"id":"@id"}); //$resource is a factory function, we do not need a `new` operator

    return NoteBook;
}])
.factory("NoteBookCollection", ["CollectionFacotry", "NoteBook", function(CollectionFacotry, NoteBook) {
    return CollectionFacotry(NoteBook);
}])
;;"use strict";
/**
 * Resource Static Method:all, query, getById, getByIds, set
 * Resource Instance Mthod: $id, $save, $update, $updateC, $set, $remove, $saveOrUpdate
 */
angular.module('resource', []).factory('Resource', ["$http", "$q", function($http, $q) {
  function ResourceFactory(name) {
    var url = "res/" + name;
    var defaultParams = {}, _defaultParams = {headers:"application/json"};
    var Resource = function(data) {
      angular.extend(this, data);
    };
    var PromiseToQuery = function(){};

    var thenFactoryMethod = function(httpPromise, successcb, errorcb, isArray) {
      var scb = successcb || angular.noop;
      var ecb = errorcb || angular.noop;
      var i;

      return httpPromise.then(function(response) {
        var result;
        if (isArray) { // only Resource.query set isArray = true
          result = [];
          for (i = 0; i < response.data.length; i++) {
            result.push(new Resource(response.data[i]));
          }
        } else {
          //MongoLab has rather peculiar way of reporting not-found items, I would expect 404 HTTP response status...
          //if (response.data === " null ") {
          if (!response) {
            return $q.reject({
              code: 'resource.notfound',
              collection: name
            });
          } else {
            result = new Resource(response.data);
          }
        }
        scb(result, response.status, response.headers, response.config);
        return result;
      }, function(response) {
        ecb(undefined, response.status, response.headers, response.config);
        return undefined;
      });
    };

    /*
      Resource method: instance do not has the following method.
      and we won't mess the instance with the Resource itself up.
      */
    Resource.all = function(cb, errorcb) {
      return Resource.query({}, cb, errorcb);
    };
    /**
     * query for resources, the paramet will append with url
     * such as: http://localhost:8080/note/doo/res/entry?l=10&q=%7B%7D
     * ${url} => http://localhost:8080/note/doo/res/entry
     * ${params} => {l:10, q:{}}
     * @param  {[type]} queryJson   condition to query
     * @param  {[type]} successcb   function(result, response.status, response.headers, response.config)
     * @param  {[type]} errorcb     function(result, response.status, response.headers, response.config)
     * @param  {[type]} extraParams most of the time, we do not set this param and use the proxy class PromiseToQuery to handle it
     * @return {[type]}             [description]
     */
    Resource.query = function(queryJson, successcb, errorcb, extraParams) {
      for(var key in queryJson) {
    	  if (/^(limit|skip|count|field|single|sortby)$/.test(key)) throw Exception("queryJson cannot contain meta info");
      }
      var httpPromise = $http.get(url, {
        params: angular.extend({}, defaultParams, queryJson, extraParams)
      });
      return thenFactoryMethod(httpPromise, successcb, errorcb, true);
    };

    /**
     * query for resource with a specific id.
     * such as: http://localhost:8080/note/doo/res/entry/1065322
     */
    Resource.getById = function(id, successcb, errorcb) {
      var httpPromise = $http.get(url + '/' + id, {
        params: defaultParams
      });
      return thenFactoryMethod(httpPromise, successcb, errorcb);
    };

    /**
     * [Static Method]
     * Set the params for query, using the temp PromiseToQuery class;
     * usually resouce.setParam("limit", 2).setParam("field", {author: 1}).query({}, sucFn, errFn)
     * @param name
     * @param value
     * @returns {Object} the Resource itself for chaining
     */
    Resource.set = function(name, value) {
      var ptq = new PromiseToQuery();
      ptq.set(name, value);
      return ptq; //so that we can chain, resource.setParam("limit", 2).setParam("field", {author: 1}).query({})
    };

    function PromiseToQuery(){}
    PromiseToQuery.prototype.set = function(name, value) {
    	/*
      var map = {
            "limit": "l",
            "skip": "sk",
            "count": "c",
            "field": "f",
            "single": "fo",
            "sortby": "s"
          },
          key = map[name];
*/
    	var key = name;
      this.params = this.params || {};
      if (key) {
        this.params[key] = value;
      } else {
        throw new Error("No key found in PromiseToQuery");
      }
      return this; // so that we can chain
    };

    PromiseToQuery.prototype.query = function(queryJSON, sFn, eFn) {
      return Resource.query(queryJSON, sFn, eFn, this.params);
    };

    //instance methods
    Resource.prototype.$id = function() {
      if (this.id) {
        return this.id;
      }
    };

    /**
     * save use post to save data in case the data is too large for GET.
     * @param successcb
     * @param errorcb
     * @returns
     */
    Resource.prototype.$save = function(successcb, errorcb) {
      var httpPromise = $http.post(url, this, {
        params: defaultParams
      });
      return thenFactoryMethod(httpPromise, successcb, errorcb);
    };

    Resource.prototype.$update = function(successcb, errorcb) {
      var httpPromise = $http.put(url + "/" + this.$id(), angular.extend({}, this, {
        _id: undefined
      }), {
        params: defaultParams
      });
      return thenFactoryMethod(httpPromise, successcb, errorcb);
    };

	  /**
	   * http://docs.mongodb.org/manual/core/update/
	   * send Action as `inc`, `set` to update the Resource.
	   * @param action Sample: {"_name": "set", "body": { 'name.middle': 'Warner' }}, _name and body are required!!
	   * @param successcb
	   * @param errorcb
	   * @returns {*}
	   */
    Resource.prototype.$updateC = function(action, successcb, errorcb) {
      var httpPromise = $http.put(url + "/" + this.$id() + "/" + action._name, /* url */
      action.body || {}, /* request body */
	      {
        params: angular.extend({}, defaultParams)
      } /* empty by default*/ );
      var me = this;

      return thenFactoryMethod(httpPromise, function(res, status, headers, config) {
        angular.extend(me, res instanceof Resource ? res : {}); //update me
        successcb && successcb.apply(this, arguments);
      }, errorcb);
    };

	  /**
	   * send $set operation to server
	   * @param obj sould be {key1: value1, key2: value2}
	   * @returns {*}
     * @param successcb
		 * @param errorcb
	   */
	  Resource.prototype.$set = function(obj, successcb, errorcb) {
		  return this.$updateC({
			  "_name": "set",
			  "body": obj
		  }, successcb, errorcb);
	  }

    Resource.prototype.$remove = function(successcb, errorcb) {
      var httpPromise = $http['delete'](url + "/" + this.$id(), {
        params: defaultParams
      });
      return thenFactoryMethod(httpPromise, successcb, errorcb);
    };

    Resource.prototype.$saveOrUpdate = function(savecb, updatecb, errorSavecb, errorUpdatecb) {
      return this.$id() ? this.$update(updatecb, errorUpdatecb) : this.$save(savecb, errorSavecb);
    };

    return Resource;
  }

  return ResourceFactory;
}]);;angular.module('appResource', ['ngResource']).
/**
 * 其实这就是ngResource的一个代理，增强了ResourceFacotry生成的Resource的功能
 */
    factory('ResourceFactory', ["$resource", function($resource){
        return function(url, paramDefaults, actions) {
            var Resource = $resource(url, paramDefaults, actions);

            /**
             * 对于$set方法，仅当设置到服务成功才更新本地变量.
             * 为params增加一个_keys属性，表示仅设置params中提到的key
             */
            Resource.prototype['$set'] = function(params, success, error) {
                var me = this;
                var new_obj = angular.extend({}, this, params);
                new_obj.__keys = _.keys(params);
                var result = Resource.save(null, new_obj, function(data){
                    angular.extend(me, data); //only after success, we apply the new value to the origin object
                    success && success.apply(this, arguments);
                }, error);
                return result.$promise || result;
            };

            Resource.prototype['detach'] = function(list) {
                for(var i = 0; i < list.length; i++) {
                    if (list[i]['id'] === this.id) {
                        list.splice(i, 1);
                        return;
                    }
                }
            }

            Resource.setupPage = function(config){
                var config = angular.extend({
                    "page": 0,
                    "page.size": 10,
                    "page.sort": "id",
                    "page.sort.dir": "asc"
                }, config);

                return {
                    query: function (params, success, error) {
                        var cfg = {};
                        //如果params不是函数，则认为它是配置
                        if (!angular.isFunction(params)) {
                            angular.extend(cfg, config, params);
                        } else {
                            //否则认为params=>success, success则为error
                            angular.extend(cfg, config);
                            var temp = success;
                            success = params;
                            error = temp;
                        }

                        return Resource.query(cfg, success, error);

                    }
                }
            }/* End of Resource.setupPage */

            return Resource;
        }

    }])
/**
 * 用于生成Resource的集合类的工厂类
 * 用法：
 * var ProductCollection = CollectionFactory(Product);
 * var products = new ProductCollection([product1, product2, product3])
 */
.factory("CollectionFacotry",function(){
    return function(resourceClass) {
        return function(list){
            var alist = [];
            angular.forEach(list, function(entry) {
                alist.push(new resourceClass(entry));
            });
            //将某个property为value值的元素从数组中清掉
            alist.without = function(property, value) {
                for(var i = 0; i < alist.length; i++) {
                    if (alist[i][property] === value) {
                        alist.splice(i, 1);
                        return;
                    }
                }
            }
            return alist;
        }
    }
})
// you MUST use constant("urlBase", 'your site address') to override!
.value("urlBase", "/");;'use strict';

/**
 * @ngdoc overview
 * @name ngResource
 * @description
 */

/**
 * @ngdoc object
 * @name ngResource.$resource
 * @requires $http
 *
 * @description
 * view: http://docs.angularjs.org/api/ngResource.$resource
 */
angular.module('ngResource', ['ng']).
  factory('$resource', ['$http', '$parse', '$q', function($http, $parse, $q) {
    var DEFAULT_ACTIONS = {
      'get':    {method:'GET'},
      'save':   {method:'POST'},
      'query':  {method:'GET', isArray:true},
      'remove': {method:'DELETE'},
      'delete': {method:'DELETE'}
    };
    var noop = angular.noop,
        forEach = angular.forEach,
        extend = angular.extend,
        copy = angular.copy,
        isFunction = angular.isFunction,
        getter = function(obj, path) {
          return $parse(path)(obj);
        };

    /**
     * We need our custom method because encodeURIComponent is too aggressive and doesn't follow
     * http://www.ietf.org/rfc/rfc3986.txt with regards to the character set (pchar) allowed in path
     * segments:
     *    segment       = *pchar
     *    pchar         = unreserved / pct-encoded / sub-delims / ":" / "@"
     *    pct-encoded   = "%" HEXDIG HEXDIG
     *    unreserved    = ALPHA / DIGIT / "-" / "." / "_" / "~"
     *    sub-delims    = "!" / "$" / "&" / "'" / "(" / ")"
     *                     / "*" / "+" / "," / ";" / "="
     */
    function encodeUriSegment(val) {
      return encodeUriQuery(val, true).
        replace(/%26/gi, '&').
        replace(/%3D/gi, '=').
        replace(/%2B/gi, '+');
    }


    /**
     * This method is intended for encoding *key* or *value* parts of query component. We need a custom
     * method because encodeURIComponent is too aggressive and encodes stuff that doesn't have to be
     * encoded per http://tools.ietf.org/html/rfc3986:
     *    query       = *( pchar / "/" / "?" )
     *    pchar         = unreserved / pct-encoded / sub-delims / ":" / "@"
     *    unreserved    = ALPHA / DIGIT / "-" / "." / "_" / "~"
     *    pct-encoded   = "%" HEXDIG HEXDIG
     *    sub-delims    = "!" / "$" / "&" / "'" / "(" / ")"
     *                     / "*" / "+" / "," / ";" / "="
     */
    function encodeUriQuery(val, pctEncodeSpaces) {
      return encodeURIComponent(val).
        replace(/%40/gi, '@').
        replace(/%3A/gi, ':').
        replace(/%24/g, '$').
        replace(/%2C/gi, ',').
        replace(/%20/g, (pctEncodeSpaces ? '%20' : '+'));
    }

    function Route(template, defaults) {
      this.template = template;
      this.defaults = defaults || {};
      this.urlParams = {};
    }

    Route.prototype = {
      setUrlParams: function(config, params, actionUrl) {
        var self = this,
            url = actionUrl || self.template,
            val,
            encodedVal;

        var urlParams = self.urlParams = {};
        forEach(url.split(/\W/), function(param){
          if (!(new RegExp("^\\d+$").test(param)) && param && (new RegExp("(^|[^\\\\]):" + param + "(\\W|$)").test(url))) {
              urlParams[param] = true;
          }
        });
        url = url.replace(/\\:/g, ':');

        params = params || {};
        forEach(self.urlParams, function(_, urlParam){
          val = params.hasOwnProperty(urlParam) ? params[urlParam] : self.defaults[urlParam];
          if (angular.isDefined(val) && val !== null) {
            encodedVal = encodeUriSegment(val);
            url = url.replace(new RegExp(":" + urlParam + "(\\W|$)", "g"), encodedVal + "$1");
          } else {
            url = url.replace(new RegExp("(\/?):" + urlParam + "(\\W|$)", "g"), function(match,
                leadingSlashes, tail) {
              if (tail.charAt(0) == '/') {
                return tail;
              } else {
                return leadingSlashes + tail;
              }
            });
          }
        });

        // strip trailing slashes and set the url
        url = url.replace(/\/+$/, '');
        // then replace collapse `/.` if found in the last URL path segment before the query
        // E.g. `http://url.com/id./format?q=x` becomes `http://url.com/id.format?q=x`
        url = url.replace(/\/\.(?=\w+($|\?))/, '.');
        // replace escaped `/\.` with `/.`
        config.url = url.replace(/\/\\\./, '/.');


        // set params - delegate param encoding to $http
        forEach(params, function(value, key){
          if (!self.urlParams[key]) {
            config.params = config.params || {};
            config.params[key] = value;
          }
        });
      }
    };


    function ResourceFactory(url, paramDefaults, actions) {
      var route = new Route(url);

      /*
       * 通常actions参数为空，则此处actions即为
       * {
	      'get':    {method:'GET'},
	      'save':   {method:'POST'},
	      'query':  {method:'GET', isArray:true},
	      'remove': {method:'DELETE'},
	      'delete': {method:'DELETE'}
    	 }
       */
      actions = extend({}, DEFAULT_ACTIONS, actions);

      /**
       * 将param中的东西规范化，如{a : "@id"} => {a : data.id}
       * @param  {[type]} data         当前发出请求的对象
       * @param  {[type]} actionParams action的params，如{method: "POST", isArray: true, others: ....}
       * @return {[type]}              规范化后的JSON对象
       */
      function extractParams(data, actionParams){
        var ids = {};
        actionParams = extend({}, paramDefaults, actionParams);//注：会混合自来paramDefaults的东西，即new Resource(url, {id:'@id', ... }, {post:{..}, get:{..}})中的第二个参数
        forEach(actionParams, function(value, key){
          if (isFunction(value)) { value = value(); }
          ids[key] = value && value.charAt && value.charAt(0) == '@' ? getter(data, value.substr(1)) : value;//如果value以@为前缀，表示动态取其宿主对象这个属性的值
        });
        return ids;
      }

      function defaultResponseInterceptor(response) {
        return response.resource;
      }

      function Resource(value){
        copy(value || {}, this);
      }

      forEach(actions, function(action, name) {
        var hasBody = /^(POST|PUT|PATCH)$/i.test(action.method);

        //即相当于Resource.get = function(){... return promise}
        Resource[name] = function(a1, a2, a3, a4) {//这个函数执行完，最后返回一个promise
          var params = {}, data, success, error;

          switch(arguments.length) {
          case 4:
            error = a4;
            success = a3;
            //fallthrough
          case 3:
          case 2:
            if (isFunction(a2)) {
              if (isFunction(a1)) {
                success = a1;
                error = a2;
                break;
              }

              success = a2;
              error = a3;
              //fallthrough
            } else {
              params = a1;
              data = a2;
              success = a3;
              break;
            }
          case 1:
            if (isFunction(a1)) success = a1;
            else if (hasBody) data = a1;
            else params = a1;
            break;
          case 0: break;
          default:
            throw ngResourceMinErr('badargs',
              "Expected up to 4 arguments [params, data, success, error], got {0} arguments", arguments.length);
          }

          var isInstanceCall = data instanceof Resource;
          var value = isInstanceCall ? data : (action.isArray ? [] : new Resource(data));
          var httpConfig = {};
          var responseInterceptor = action.interceptor && action.interceptor.response || defaultResponseInterceptor;
          var responseErrorInterceptor = action.interceptor && action.interceptor.responseError || undefined;

          forEach(action, function(value, key) {
            if (key != 'params' && key != 'isArray' && key != 'interceptor') {
              httpConfig[key] = copy(value);
            }
          });

          httpConfig.data = data;
          //总共三个参数
          // action.params即{method:"POST", isArray: true}这样的东西
          route.setUrlParams(httpConfig, /*如method=GET*/ 
        		  			 extend({}, extractParams(data, action.params || {}), params), //params指的是entity.$save(params, success, failure)中传入的params
        		  			 action.url);

          /** 凡是符合这个表达式的，都是success: 200 <= status && status < 300;
           * 如200, 201, 202等，反之401， 403， 500等都由error处理
           * response的格式为: 
           * {
           *  config: {对应的request的config}
           *  data:{服务器传回的内容}
           *  headers: function (name){ ... }，使用方式如: headers("content-type") --> "application/json;charset=UTF-8"
           *  status: 401
           * }
           **/
          var promise = $http(httpConfig).then(function(response) {
            var data = response.data,
                promise = value.$promise;

            if (data) {
              if ( angular.isArray(data) != !!action.isArray ) {
                throw ngResourceMinErr('badcfg', 'Error in resource configuration. Expected response' +
                  ' to contain an {0} but got an {1}', 
                  action.isArray?'array':'object', angular.isArray(data)?'array':'object');
              }
              if (action.isArray) {
                value.length = 0;
                forEach(data, function(item) {
                  value.push(new Resource(item));
                });
              } else {
                copy(data, value);
                value.$promise = promise;
              }
            }

            value.$resolved = true;

            (success||noop)(value, response.headers);

            response.resource = value;

            return response;
          }, function(response) {
            value.$resolved = true;

            (error||noop)(response);

            return $q.reject(response);
          }).then(responseInterceptor, responseErrorInterceptor);


          if (!isInstanceCall) {
            // we are creating instance / collection
            // - set the initial promise
            // - return the instance / collection
            value.$promise = promise;
            value.$resolved = false;

            return value;
          }

          // instance call
          return promise;
        };


        Resource.prototype['$' + name] = function(params, success, error) {
          if (isFunction(params)) {
            error = success; success = params; params = {};
          }
          var result = Resource[name](params, this, success, error);
          return result.$promise || result;
        };
      });

      Resource.bind = function(additionalParamDefaults){
        return ResourceFactory(url, extend({}, paramDefaults, additionalParamDefaults), actions);
      };

      return Resource;
    }

    return ResourceFactory;
  }]);
;angular.module("ssnau.service", [])
.factory("ThemeManager", function(){
    var prototype = {
        fetch: function (name) {
            if (this.loaded.indexOf(name) != -1) return;

            var themes = this.themes;
            themes.forEach(function(theme) {
                if (theme.name === name) {
                    _fetch(theme.path + "/" + name);
                }
            });
        }
    };

    var constructor =  function ThemeManager() {
        angular.extend(this, {
            base: "/",  /* base url when fetching a theme */
            loaded: [], /* array of theme name */
            loading: [], /* array of theme name */
            current: null
        });
    };

    angular.extend(constructor.prototype, prototype);

    return constructor;
});;angular.module("class.ssnau", [])
.factory("Class", function() {
        var initializing = false,
            fnTest = /xyz/.test(function(){xyz;}) ? /\b_super\b/ : /.*/,
            Class = function(){}; // The base Class implementation (does nothing)

        // Create a new Class that inherits from this class
        Class.extend = function(prop) {
            var _super = this.prototype;

            // Instantiate a base class (but only create the instance,
            // don't run the init constructor)
            initializing = true;
            var prototype = new this();
            initializing = false;

            // Copy the properties over onto the new prototype
            for (var name in prop) {
                // Check if we're overwriting an existing function
                prototype[name] = typeof prop[name] == "function" &&
                    typeof _super[name] == "function" && fnTest.test(prop[name]) ?
                    (function(name, fn){
                        return function() {
                            var tmp = this._super;

                            // Add a new ._super() method that is the same method
                            // but on the super-class
                            this._super = _super[name];

                            // The method only need to be bound temporarily, so we
                            // remove it when we're done executing
                            var ret = fn.apply(this, arguments);
                            this._super = tmp;

                            return ret;
                        };
                    })(name, prop[name]) :
                    prop[name];
            }

            // The dummy class constructor
            function Class() {
                // All construction is actually done in the init method
                if ( !initializing && this.init )
                    this.init.apply(this, arguments);
            }

            // Populate our constructed prototype object
            Class.prototype = prototype;

            // Enforce the constructor to be what we expect
            Class.prototype.constructor = Class;

            // And make this class extendable
            Class.extend = arguments.callee;

            return Class;
        };

        return Class;
});
;(function(){
    var module = angular.module("interceptor.ssnau", []);
    module.factory('LoginHttpInterceptor', function($q, dependency1, dependency2) {
        return {
            // optional method
            'request': function(config) {
                // do something on success
                return config || $q.when(config);
            },

            // optional method
            'requestError': function(rejection) {
                // do something on error
                if (canRecover(rejection)) {
                    return responseOrNewPromise
                }
                return $q.reject(rejection);
            },

            // optional method
            'response': function(response) {
                // do something on success
                return response || $q.when(response);
            },

            // optional method
            'responseError': function(rejection) {
                // do something on error
                if (canRecover(rejection)) {
                    return responseOrNewPromise
                }
                return $q.reject(rejection);
            }
        };
    });

})();

;angular.module("message.service", ['ui.bootstrap'])
.service('MessageBox', ['$dialog', function($dialog){
        var defaultConfig = {
            backdrop: true,
            keyboard: true,
            backdropClick: true,
            dialogClass:"modal auto-modal",
            template: "<div class=\"modal-header auto-modal\">\n" +
                "	<h5>{{ title }}</h5>\n" +
                "</div>\n" +
                "<div class=\"modal-body\">\n" +
                "	<p>{{ message }}</p>\n" +
                "</div>\n" +
                "<div class=\"modal-footer\">\n" +
                "	<button ng-repeat=\"btn in buttons\" ng-click=\"close(btn.result)\" class=btn ng-class=\"btn.cssClass\">{{ btn.label }}</button>\n" +
                "</div>\n" +
                "",
            controller: ['$scope', 'dialog', 'model', function($scope, dialog, model){
                $scope.title = model.title;
                $scope.message = model.message;
                $scope.buttons = model.buttons;
                $scope.close = function(res){
                    model.close(res);
                    dialog.close(res);
                };
            }],
            buttons: [
                {
                    label: '确定'
                }
            ]
        };
        return function(config) {
            var finalConfig = angular.extend({}, defaultConfig, config);
            finalConfig.resolve =  {
                model: function() {
                    return {
                        title: finalConfig.title,
                        message: finalConfig.message,
                        buttons: finalConfig.buttons,
                        close: finalConfig.close || angular.noop
                    };
                }
            };


            return $dialog.dialog(finalConfig);
        }

    }]);;(function () {
    var utilModule = angular.module("util.ssnau", []);

    // Create an AngularJS service called debounce
    utilModule.factory('debounce', ['$timeout', '$q', function ($timeout, $q) {
        // The service is actually this function, which we call with the func
        // that should be debounced and how long to wait in between calls
        return function debounce(func, wait, immediate) {
            var timeout;
            // Create a deferred object that will be resolved when we need to
            // actually call the func
            var deferred = $q.defer();
            return function () {
                var context = this, args = arguments;
                var later = function () {
                    timeout = null;
                    if (!immediate) {
                        deferred.resolve(func.apply(context, args));
                        deferred = $q.defer();
                    }
                };
                var callNow = immediate && !timeout;
                if (timeout) {
                    $timeout.cancel(timeout);
                }
                timeout = $timeout(later, wait);
                if (callNow) {
                    deferred.resolve(func.apply(context, args));
                    deferred = $q.defer();
                }
                return deferred.promise;
            };
        };
    }]);
})();
;(function() {
    /* Start angularLocalStorage */

    var angularLocalStorage = angular.module('LocalStorageModule', []);

// You should set a prefix to avoid overwriting any local storage variables from the rest of your app
// e.g. angularLocalStorage.constant('prefix', 'youAppName');
    angularLocalStorage.value('prefix', 'ls');
// Cookie options (usually in case of fallback)
// expiry = Number of days before cookies expire // 0 = Does not expire
// path = The web path the cookie represents
    angularLocalStorage.constant('cookie', { expiry:30, path: '/'});
    angularLocalStorage.constant('notify', { setItem: true, removeItem: false} );

    angularLocalStorage.service('localStorageService', [
        '$rootScope',
        'prefix',
        'cookie',
        'notify',
        function($rootScope, prefix, cookie, notify) {

            // If there is a prefix set in the config lets use that with an appended period for readability
            //var prefix = angularLocalStorage.constant;
            if (prefix.substr(-1)!=='.') {
                prefix = !!prefix ? prefix + '.' : '';
            }

            // Checks the browser to see if local storage is supported
            var browserSupportsLocalStorage = function () {
                try {
                    return ('localStorage' in window && window['localStorage'] !== null);
                } catch (e) {
                    $rootScope.$broadcast('LocalStorageModule.notification.error',e.message);
                    return false;
                }
            };

            // Directly adds a value to local storage
            // If local storage is not available in the browser use cookies
            // Example use: localStorageService.add('library','angular');
            var addToLocalStorage = function (key, value) {

                // If this browser does not support local storage use cookies
                if (!browserSupportsLocalStorage()) {
                    $rootScope.$broadcast('LocalStorageModule.notification.warning','LOCAL_STORAGE_NOT_SUPPORTED');
                    if (notify.setItem) {
                        $rootScope.$broadcast('LocalStorageModule.notification.setitem', {key: key, newvalue: value, storageType: 'cookie'});
                    }
                    return addToCookies(key, value);
                }

                // Let's convert undefined values to null to get the value consistent
                if (typeof value == "undefined") {
                    value = null;
                }

                try {
                    if (angular.isObject(value) || angular.isArray(value)) {
                        value = angular.toJson(value);
                    }
                    localStorage.setItem(prefix+key, value);
                    if (notify.setItem) {
                        $rootScope.$broadcast('LocalStorageModule.notification.setitem', {key: key, newvalue: value, storageType: 'localStorage'});
                    }
                } catch (e) {
                    $rootScope.$broadcast('LocalStorageModule.notification.error',e.message);
                    return addToCookies(key, value);
                }
                return true;
            };

            // Directly get a value from local storage
            // Example use: localStorageService.get('library'); // returns 'angular'
            var getFromLocalStorage = function (key) {
                if (!browserSupportsLocalStorage()) {
                    $rootScope.$broadcast('LocalStorageModule.notification.warning','LOCAL_STORAGE_NOT_SUPPORTED');
                    return getFromCookies(key);
                }

                var item = localStorage.getItem(prefix+key);
                // angular.toJson will convert null to 'null', so a proper conversion is needed
                // FIXME not a perfect solution, since a valid 'null' string can't be stored
                if (!item || item === 'null') return null;

                if (item.charAt(0) === "{" || item.charAt(0) === "[") {
                    return angular.fromJson(item);
                }
                return item;
            };

            // Remove an item from local storage
            // Example use: localStorageService.remove('library'); // removes the key/value pair of library='angular'
            var removeFromLocalStorage = function (key) {
                if (!browserSupportsLocalStorage()) {
                    $rootScope.$broadcast('LocalStorageModule.notification.warning','LOCAL_STORAGE_NOT_SUPPORTED');
                    if (notify.removeItem) {
                        $rootScope.$broadcast('LocalStorageModule.notification.removeitem', {key: key, storageType: 'cookie'});
                    }
                    return removeFromCookies(key);
                }

                try {
                    localStorage.removeItem(prefix+key);
                    if (notify.removeItem) {
                        $rootScope.$broadcast('LocalStorageModule.notification.removeitem', {key: key, storageType: 'localStorage'});
                    }
                } catch (e) {
                    $rootScope.$broadcast('LocalStorageModule.notification.error',e.message);
                    return removeFromCookies(key);
                }
                return true;
            };

            // Return array of keys for local storage
            // Example use: var keys = localStorageService.keys()
            var getKeysForLocalStorage = function () {

                if (!browserSupportsLocalStorage()) {
                    $rootScope.$broadcast('LocalStorageModule.notification.warning','LOCAL_STORAGE_NOT_SUPPORTED');
                    return false;
                }

                var prefixLength = prefix.length;
                var keys = [];
                for (var key in localStorage) {
                    // Only return keys that are for this app
                    if (key.substr(0,prefixLength) === prefix) {
                        try {
                            keys.push(key.substr(prefixLength));
                        } catch (e) {
                            $rootScope.$broadcast('LocalStorageModule.notification.error',e.Description);
                            return [];
                        }
                    }
                }
                return keys;
            };

            // Remove all data for this app from local storage
            // Example use: localStorageService.clearAll();
            // Should be used mostly for development purposes
            var clearAllFromLocalStorage = function () {

                if (!browserSupportsLocalStorage()) {
                    $rootScope.$broadcast('LocalStorageModule.notification.warning','LOCAL_STORAGE_NOT_SUPPORTED');
                    return clearAllFromCookies();
                }

                var prefixLength = prefix.length;

                for (var key in localStorage) {
                    // Only remove items that are for this app
                    if (key.substr(0,prefixLength) === prefix) {
                        try {
                            removeFromLocalStorage(key.substr(prefixLength));
                        } catch (e) {
                            $rootScope.$broadcast('LocalStorageModule.notification.error',e.message);
                            return clearAllFromCookies();
                        }
                    }
                }
                return true;
            };

            // Checks the browser to see if cookies are supported
            var browserSupportsCookies = function() {
                try {
                    return navigator.cookieEnabled ||
                        ("cookie" in document && (document.cookie.length > 0 ||
                            (document.cookie = "test").indexOf.call(document.cookie, "test") > -1));
                } catch (e) {
                    $rootScope.$broadcast('LocalStorageModule.notification.error',e.message);
                    return false;
                }
            };

            // Directly adds a value to cookies
            // Typically used as a fallback is local storage is not available in the browser
            // Example use: localStorageService.cookie.add('library','angular');
            var addToCookies = function (key, value) {

                if (typeof value == "undefined") {
                    return false;
                }

                if (!browserSupportsCookies()) {
                    $rootScope.$broadcast('LocalStorageModule.notification.error','COOKIES_NOT_SUPPORTED');
                    return false;
                }

                try {
                    var expiry = '', expiryDate = new Date();
                    if (value === null) {
                        // Mark that the cookie has expired one day ago
                        expiryDate.setTime(expiryDate.getTime() + (-1 * 24*60*60*1000));
                        expiry = "; expires="+expiryDate.toGMTString();

                        value = '';
                    } else if (cookie.expiry !== 0) {
                        expiryDate.setTime(expiryDate.getTime() + (cookie.expiry*24*60*60*1000));
                        expiry = "; expires="+expiryDate.toGMTString();
                    }
                    if (!!key) {
                        document.cookie = prefix + key + "=" + encodeURIComponent(value) + expiry + "; path="+cookie.path;
                    }
                } catch (e) {
                    $rootScope.$broadcast('LocalStorageModule.notification.error',e.message);
                    return false;
                }
                return true;
            };

            // Directly get a value from a cookie
            // Example use: localStorageService.cookie.get('library'); // returns 'angular'
            var getFromCookies = function (key) {
                if (!browserSupportsCookies()) {
                    $rootScope.$broadcast('LocalStorageModule.notification.error','COOKIES_NOT_SUPPORTED');
                    return false;
                }

                var cookies = document.cookie.split(';');
                for(var i=0;i < cookies.length;i++) {
                    var thisCookie = cookies[i];
                    while (thisCookie.charAt(0)==' ') {
                        thisCookie = thisCookie.substring(1,thisCookie.length);
                    }
                    if (thisCookie.indexOf(prefix+key+'=') === 0) {
                        return decodeURIComponent(thisCookie.substring(prefix.length+key.length+1,thisCookie.length));
                    }
                }
                return null;
            };

            var removeFromCookies = function (key) {
                addToCookies(key,null);
            };

            var clearAllFromCookies = function () {
                var thisCookie = null, thisKey = null;
                var prefixLength = prefix.length;
                var cookies = document.cookie.split(';');
                for(var i=0;i < cookies.length;i++) {
                    thisCookie = cookies[i];
                    while (thisCookie.charAt(0)==' ') {
                        thisCookie = thisCookie.substring(1,thisCookie.length);
                    }
                    key = thisCookie.substring(prefixLength,thisCookie.indexOf('='));
                    removeFromCookies(key);
                }
            };

            return {
                isSupported: browserSupportsLocalStorage,
                set: addToLocalStorage,
                add: addToLocalStorage, //DEPRECATED
                get: getFromLocalStorage,
                keys: getKeysForLocalStorage,
                remove: removeFromLocalStorage,
                clearAll: clearAllFromLocalStorage,
                cookie: {
                    set: addToCookies,
                    add: addToCookies, //DEPRECATED
                    get: getFromCookies,
                    remove: removeFromCookies,
                    clearAll: clearAllFromCookies
                }
            };

        }]);
}).call(this);
;angular.module("ui.ssnau.modal", ["ui.bootstrap"])
.factory("modal", ["$dialog", function($dialog){
    return {
        modal: function(config) {

        }
    }
}]);