"use strict";
angular.module("resource.notebook", ["bsResource"])

.factory("NoteBook", ["ResourceFactory", function(ResourceFactory){
    var NoteBook = ResourceFactory("note_book", {"id":"@id"}); //$resource is a factory function, we do not need a `new` operator

    return NoteBook;
}]);