angular.module('appResource', ['ngResource']).
/**
 * 其实这就是ngResource的一个代理，增强了ResourceFacotry生成的Resource的功能
 */
    factory('ResourceFactory', ["$resource", function($resource){
        return function(url, paramDefaults, actions) {
            var Resource = $resource(url, paramDefaults, actions);

            /**
             * 对于$set方法，仅当设置到服务成功才更新本地变量.
             * 为params增加一个_keys属性，表示仅设置params中提到的key
             */
            Resource.prototype['$set'] = function(params, success, error) {
                var me = this;
                var new_obj = angular.extend({}, this, params);
                new_obj.__keys = _.keys(params);
                var result = Resource.save(null, new_obj, function(data){
                    angular.extend(me, data); //only after success, we apply the new value to the origin object
                    success && success.apply(this, arguments);
                }, error);
                return result.$promise || result;
            };

            Resource.prototype['detach'] = function(list) {
                for(var i = 0; i < list.length; i++) {
                    if (list[i]['id'] === this.id) {
                        list.splice(i, 1);
                        return;
                    }
                }
            }

            Resource.setupPage = function(config){
                var config = angular.extend({
                    "page": 0,
                    "page.size": 10,
                    "page.sort": "id",
                    "page.sort.dir": "asc"
                }, config);

                return {
                    query: function (params, success, error) {
                        var cfg = {};
                        //如果params不是函数，则认为它是配置
                        if (!angular.isFunction(params)) {
                            angular.extend(cfg, config, params);
                        } else {
                            //否则认为params=>success, success则为error
                            angular.extend(cfg, config);
                            var temp = success;
                            success = params;
                            error = temp;
                        }

                        return Resource.query(cfg, success, error);

                    }
                }
            }/* End of Resource.setupPage */

            return Resource;
        }

    }])
/**
 * 用于生成Resource的集合类的工厂类
 * 用法：
 * var ProductCollection = CollectionFactory(Product);
 * var products = new ProductCollection([product1, product2, product3])
 */
.factory("CollectionFacotry",function(){
    return function(resourceClass) {
        return function(list){
            var alist = [];
            angular.forEach(list, function(entry) {
                alist.push(new resourceClass(entry));
            });
            //将某个property为value值的元素从数组中清掉
            alist.without = function(property, value) {
                for(var i = 0; i < alist.length; i++) {
                    if (alist[i][property] === value) {
                        alist.splice(i, 1);
                        return;
                    }
                }
            }
            return alist;
        }
    }
})
// you MUST use constant("urlBase", 'your site address') to override!
.value("urlBase", "/");