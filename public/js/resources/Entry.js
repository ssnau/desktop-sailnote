/*"use strict";
angular.module("resource.entry", ["resource"])

.factory("Entry", ["Resource", function(resourceFactory){
  var Entry = resourceFactory("entry"); //Resource is a factory function, we do not need a `new` operator

  // get 10 articles from home page
  Entry.home = function() {
    return Entry.set('limit', 10).query({});
  };

  return Entry;
}])

.filter("testfilter", ["Resource", function(resourceFactory){
  return function(input) {
    return 'hello';
  };
}]);*/


"use strict";
angular.module("resource.entry", ["appResource"])

.factory("Entry", ["ResourceFactory", "urlBase", function(ResourceFactory, urlBase){
  var Entry = ResourceFactory(urlBase + "rest/entry/:id", {"id":"@id"}); //$resource is a factory function, we do not need a `new` operator

  return Entry;
}]);