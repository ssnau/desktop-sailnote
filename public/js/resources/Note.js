"use strict";
angular.module("resource.note", ["bsResource"])

.factory("Note", ["ResourceFactory", function(ResourceFactory){
    var Note = ResourceFactory(
        "note",
        {"id":"@id"},
        {
            search: {
                method : "GET",
                isArray: true,
                params: {"action": "search"}
            }
        }
    ); //$resource is a factory function, we do not need a `new` operator

    return Note;
}]);