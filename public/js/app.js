angular.module("app", ["app.config", "ui.ssnau.resize", "ui.ssnau.waitable", "ui.ssnau.flexBox",
    "ui.ssnau.dockableFlexBox", 'ui.ssnau.upload', "LocalStorageModule", "ui.ssnau.dnd",
    "ui.ssnau.contextmenu", "ui.bootstrap", "simple.ssnau", "ui.ssnau.draggable", "message.service",
    "resource.notebook", "resource.note", "ui.ssnau.keyboard", "ui.event", "util.ssnau"]);